package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 4/17/2018.
 */
class TasksDataManager()
{
    companion object {
        public var tasksStatus: ArrayList<TasksStatusAdapter>? = null
        public var tasksPriorities: ArrayList<TaskPriorityAdapter>? = null
        public var assignedToMe: ArrayList<TaskAssignedToMeAdapter>? = null
        public var assignedByMe: ArrayList<TaskAssignedByMeAdapter>? = null


        init {
            tasksStatus = ArrayList()
            tasksPriorities = ArrayList()
            assignedByMe = ArrayList()
            assignedToMe = ArrayList()
        }

        fun insertTasksStatus(statusID: Any?,status: Any?)
        {

            tasksStatus?.add(TasksStatusAdapter(statusID,status))
        }

        fun insertTaskPriority(priorityID: Any?,priorityName: Any?)
        {

            tasksPriorities?.add(TaskPriorityAdapter(priorityID,priorityName))
        }

        fun insertAssignedToMe(TaskID: Any?,Title: Any?,StatusID: Any?,Priority: Any?, UserID: Any?,
                               ProjectID: Any?,StartDate: Any?, EndDate: Any?,Attachment: Any?,
                                TaskDesc: Any?,TaskInchargeID: Any?, Flag: Any?, SendDate: Any?,
                                ModuleID: Any?, ReferenceID: Any?,PendingDays: Any?, PendingDate: Any?,
                                CompletedDate: Any?,CompanyID: Any?,CustomerId: Any?, UserName: Any?,
                              Status: Any?, TaskPriority: Any?,TaskInchargeName: Any?,Name: Any?, ProjectName: Any?,
                                DaysLeft: Any?, DocumentURL: Any?)
        {
            assignedToMe?.add(TaskAssignedToMeAdapter(TaskID,Title,StatusID,Priority, UserID,
            ProjectID,StartDate, EndDate,Attachment,
            TaskDesc,TaskInchargeID, Flag, SendDate,
            ModuleID, ReferenceID,PendingDays, PendingDate,
            CompletedDate,CompanyID,CustomerId, UserName,
            Status, TaskPriority,TaskInchargeName,Name,ProjectName,
            DaysLeft, DocumentURL))
        }


        fun insertAssignedByMe(TaskID: Any?,Title: Any?,StatusID: Any?,Priority: Any?, UserID: Any?,
                               ProjectID: Any?,StartDate: Any?, EndDate: Any?,Attachment: Any?,
                               TaskDesc: Any?,TaskInchargeID: Any?, Flag: Any?, SendDate: Any?,
                               ModuleID: Any?, ReferenceID: Any?,PendingDays: Any?, PendingDate: Any?,
                               CompletedDate: Any?,CompanyID: Any?,CustomerId: Any?, UserName: Any?,
                               Status: Any?, TaskPriority: Any?,TaskInchargeName: Any?,Name: Any?,ProjectName: Any?,
                               DaysLeft: Any?, DocumentURL: Any?)
        {
            assignedByMe?.add(TaskAssignedByMeAdapter(TaskID,Title,StatusID,Priority, UserID,
                    ProjectID,StartDate, EndDate,Attachment,
                    TaskDesc,TaskInchargeID, Flag, SendDate,
                    ModuleID, ReferenceID,PendingDays, PendingDate,
                    CompletedDate,CompanyID,CustomerId, UserName,
                    Status, TaskPriority,TaskInchargeName,Name,ProjectName,
                    DaysLeft, DocumentURL))
        }
    }
}