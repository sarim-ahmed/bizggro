package com.averox.bizggro.Adapters

/**
 * Created by Sarim on 12/12/2017.
 */
class ProjectsAdapter
{

    private var projectID: Int? = null
    private var projectName: String? = null

    constructor(projectID: Int?,projectName: String?)
    {
        this.projectID = projectID
        this.projectName = projectName
    }

    fun getProjectID(): Int?
    {
        return projectID
    }
    fun getProjectName(): String?
    {
        return projectName
    }
}