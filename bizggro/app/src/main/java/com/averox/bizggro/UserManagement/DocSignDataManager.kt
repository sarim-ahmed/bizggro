package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.LeadsDetailAdapter
import com.averox.bizggro.Adapters.LeadsStatusAdapter
import com.averox.bizggro.Adapters.SignatureRequestAdapter

/**
 * Created by Sarim on 6/8/2018.
 */
class DocSignDataManager
{
    companion object {

        public var signatureRequestList: ArrayList<SignatureRequestAdapter>? = null



        init {
            signatureRequestList = ArrayList<SignatureRequestAdapter>()
        }

        fun insertSignatureRequests(useremail: Any?,id: Any?,docId: Any?,docTitle: Any?,
                                    originalDocId: Any?, previousDocId: Any?,dateSigned: Any?,
                                    documentUrl: Any?) {
            signatureRequestList?.add(SignatureRequestAdapter(useremail,id,docId,docTitle,
            originalDocId, previousDocId,dateSigned,
            documentUrl))
        }


    }
}