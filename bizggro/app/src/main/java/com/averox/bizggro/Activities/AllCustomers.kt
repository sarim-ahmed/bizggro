package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.CustomersListAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class AllCustomers : AppCompatActivity(),CompoundButton.OnCheckedChangeListener,View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var customers_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    //private var tooglebutton_filters: ToggleButton? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_projects: Spinner?= null
    private var spinner_categories: Spinner? = null
    private var spinner_type: Spinner? = null
    private var spinner_date: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var categoryNameArray: ArrayList<String>? = null
    private var typeArray: ArrayList<String>? = null
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var description: Any = ""
    private var projectID: Any = ""
    private var categoryID: Any = ""
    private var typeID: Any = ""
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var applyDateFilter: Any = ""
    private var input_description: EditText? = null
    private var currentDate: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_customers)


        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        customers_view = findViewById(R.id.customers_view)

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE

        //tooglebutton_filters = findViewById(R.id.togglebutton_filters)
        //tooglebutton_filters?.setOnCheckedChangeListener(this)
        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_categories = findViewById(R.id.spinner_categories)
        spinner_projects = findViewById(R.id.spinner_projects)
        spinner_type = findViewById(R.id.spinner_type)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)

        input_description = findViewById(R.id.input_description)


        projectNameArray = ArrayList()
        categoryNameArray = ArrayList()
        typeArray = ArrayList()

        input_fromDate = findViewById(R.id.input_fromdate)
        input_toDate = findViewById(R.id.input_todate)
        input_fromDate?.setOnClickListener(this)
        input_toDate?.setOnClickListener(this)

        loadSpinnerValues()
        setCurrentDate()

        input_toDate?.setText(resources.getString(R.string.toDate))
        input_fromDate?.setText(resources.getString(R.string.fromDate))



        getCustomers(loginManager?.getCompanyID(),"","1918/01/01",currentDate,"","")


        spinner_date?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position.equals(0))
                {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = ""
                }
                else if(position.equals(1))
                {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1

                }
            }
        }


        spinner_type?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    typeID = ""
                }
                else
                {
                    typeID = UserDataManager.customerTypeList?.get(position-1)?.typeid!!.toInt()

                }

            }

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

        try {
            UserDataManager?.allTransactionsList?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    fun loadSpinnerValues()
    {

       /* projectNameArray?.add("All Projects")
        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, projectNameArray)

        spinner_projects?.adapter = spinnerProjectAdapter
        for(i in 0..UserDataManager?.allProjectsList!!.size - 1)
        {
            if(UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false)
            {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }


        spinner_projects?.adapter = spinnerProjectAdapter*/


       /* categoryNameArray?.add("All Categories")
        var spinnerCategoryAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,categoryNameArray)
        spinner_categories?.adapter = spinnerCategoryAdapter

        for(i in 0..UserDataManager?.companyCategoriesList!!.size - 1)
        {
            if(UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()?.equals(null) == false)
            {
                categoryNameArray?.add(UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()!!)

            }

        }*/


       // spinner_categories?.adapter = spinnerCategoryAdapter




        typeArray?.add("Select All")

        var spinnerTypeAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,typeArray)

        spinner_type?.adapter = spinnerTypeAdapter

        for(i in 0..UserDataManager?.customerTypeList!!.size - 1)
        {

            typeArray?.add(UserDataManager?.customerTypeList?.get(i)?.type!!)


        }

        spinnerTypeAdapter?.notifyDataSetChanged()



        var spinnerDateAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, resources.getStringArray(R.array.array_datefilter))

        spinner_date?.adapter = spinnerDateAdapter


    }

    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

       /*input_fromDate?.setText(formattedDate)
               input_toDate?.setText(formattedDate)*/

        currentDate = formattedDate
    }

    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth )
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_search->
            {

                description = Uri.encode(input_description?.text.toString().trim(),"UTF-8")
                if(description.equals(null))
                {
                    description = ""
                }

                if(applyDateFilter.equals(1))
                {
                    fromDate= Uri.encode(input_fromDate?.text.toString(),"UTF-8")

                    toDate = Uri.encode(input_toDate?.text.toString(),"UTF-8")

                }

                if(input_toDate?.text!!.equals(resources.getString(R.string.toDate)) || input_fromDate?.text!!.equals(resources.getString(R.string.fromDate)))
                {

                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()

                }

                    getCustomers(loginManager?.getCompanyID(),typeID,fromDate,toDate,applyDateFilter,description)




                //getCustomers(loginManager?.getCompanyID(),projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,description)

            }
            R.id.button_reset->
            {
                input_description?.setText("")
                spinner_projects?.setSelection(0)
                spinner_categories?.setSelection(0)
                spinner_type?.setSelection(0)
                spinner_date?.setSelection(0)

                getCustomers(loginManager?.getCompanyID(),"","1918/01/01",currentDate,"","")
            }
            R.id.input_fromdate->
            {
                setUpFromDate()
            }
            R.id.input_todate->
            {
                setUpToDate()
            }


        }
    }

    fun getCustomers(companyid: Int?, TypeID: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,keyword: Any?)
    {



        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        /*UserDataManager.allTransactionsList?.clear()
        var adapter = CustomAdapter(this,UserDataManager.allTransactionsList!!)
        transactions_view?.layoutManager = LinearLayoutManager(this)
        transactions_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE*/

      /*  UserDataManager.customersListList?.clear()
        var adapter = CustomAdapter(this,UserDataManager.customersListList!!)
        transactions_view?.layoutManager = LinearLayoutManager(this)
        transactions_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE
*/

        UserDataManager.customersList?.clear()
        var adapter = CustomAdapter(this,UserDataManager.customersList!!)
        customers_view?.layoutManager = LinearLayoutManager(this)
        customers_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE

        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.TypeId,typeID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))

        UrlBuilder?.setUrl(Module.customers, RequestCode.getcustomers,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->


            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))

            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))
            }
            else
            {

                Log.d("response","in else ")

                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationdate = result.getString(JsonKeys.variables.CREATIONDATE)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var state = result.getString(JsonKeys.variables.KEY_STATE)
                        var countryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var details = result.getString(JsonKeys.variables.DETAILS)
                        var companyId = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var dateupdated = result.getString(JsonKeys.variables.DATEUPDATED)
                        var createdby = result.getString(JsonKeys.variables.CREATEDBY)
                        var updatedby = result.getString(JsonKeys.variables.UPDATEDBY)
                        var contacttypeid = result.getInt(JsonKeys.variables.CONTACTTYPEID)
                        var companyname = result.getString(JsonKeys.variables.KEY_COMPANYNAME)
                        var categoryid = result.getInt(JsonKeys.variables.CategoryId)
                        var zipcode = result.getString(JsonKeys.variables.KEY_ZIPCODE)
                        var ishidden = result.getBoolean(JsonKeys.variables.ISHIDDEN)
                        var customerid = result.getInt(JsonKeys.variables.CUSTOMERID)
                        var covertedbyid = result.getInt(JsonKeys.variables.CONVERTEDBY)
                        var dateconverted = result.getString(JsonKeys.variables.DATECONVERTED)
                        var typeid = result.getInt(JsonKeys.variables.TYPEID)
                        var creditwothinessid = result.getInt(JsonKeys.variables.CREDITWORTHINESSID)
                        var contactpersonid = result.getInt(JsonKeys.variables.CONTACTPERSONID)
                        var paymenttermsid = result.getInt(JsonKeys.variables.PAYMENTTERMSID)
                        var billingstreet = result.getString(JsonKeys.variables.BILLINGSTREET)
                        var billingcity = result.getString(JsonKeys.variables.BILLINGCITY)
                        var billingstate = result.getString(JsonKeys.variables.BILLINGSTATE)
                        var billingzipcode = result.getString(JsonKeys.variables.BILLINGZIPCODE)
                        var billingcountryid = result.getInt(JsonKeys.variables.BILLINGCOUNTRYID)
                        var shippingstreet = result.getString(JsonKeys.variables.SHIPPINGSTREET)
                        var shippingcity = result.getString(JsonKeys.variables.SHIPPINGCITY)
                        var shippingstate = result.getString(JsonKeys.variables.SHIPPINGSTATE)
                        var shippingzipcode = result.getString(JsonKeys.variables.SHIPPINGZIPCODE)
                        var shippingcountryid = result.getInt(JsonKeys.variables.SHIPPINGCOUNTRYID)
                        var type = result.getString(JsonKeys.variables.TYPE)
                        var convertedbyuser = result.getString(JsonKeys.variables.CONVERTEDBYUSER)


                        UserDataManager.insertCustomers(contactId, firstname,lastname, email,phone,mobile, address, creationdate,  city, state, countryID, details,companyId,
                                dateupdated, createdby, updatedby, contacttypeid,companyname, categoryid, zipcode, ishidden
                                , customerid, covertedbyid,dateconverted,typeid,creditwothinessid,contactpersonid
                                ,paymenttermsid,  billingstreet, billingcity, billingstate, billingzipcode, billingcountryid
                                ,shippingstreet, shippingcity, shippingstate, shippingzipcode,
                                shippingcountryid, type,convertedbyuser)

                       /* UserDataManager.insertAllTransactions(date,description,category,amount)


                        var adapter = CustomAdapter(this,UserDataManager.allTransactionsList!!)
                        transactions_view?.layoutManager = LinearLayoutManager(this)
                        transactions_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE
*/


                        var adapter = CustomAdapter(this,UserDataManager.customersList!!)
                        customers_view?.layoutManager = LinearLayoutManager(this)
                        customers_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: "+e.message.toString())
                } finally {
                }


            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE

                    alert(resources.getString(R.string.networkProblem))


                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }


    internal inner class CustomAdapter(private val context: Context, arraylist: ArrayList<CustomersListAdapter>) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomersListAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customers_list_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/
            var sr_number = position+1

            holder.sr_view?.setText(""+sr_number)
            holder.name_view?.setText(""+list_items.firstname +" "+ list_items.lastname)
            holder.email_view?.setText(""+list_items.email)
            holder.mobile_view?.setText(""+list_items.mobile)
            holder.type_view?.setText(""+list_items.type)
            holder.companyname_view?.setText(""+list_items.companyname)
            holder.convertedby_view?.setText(""+list_items.convertedbyuser)

          /*  holder.sr_view?.setText(""+sr_number)
            holder.date_view?.setText(""+list_items.getDate())
            holder.description_view?.setText(""+list_items.getDescription())
            holder.category_view?.setText(""+list_items.getCategory())
            holder.amount_view?.setText(""+list_items.getAmount())
*/
            Log.d("holder",""+list_items.firstname)
            Log.d("holder",""+list_items.mobile)

            Log.d("holder",""+list_items.companyname)

            Log.d("holder",""+list_items.convertedbyuser)
        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var name_view: TextView? = null
            var companyname_view: TextView? = null
            var mobile_view: TextView? = null
            var email_view: TextView? = null
            var type_view: TextView? = null
            var convertedby_view: TextView? = null

            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                sr_view = itemView.findViewById(R.id.sr_view)
                name_view = itemView.findViewById(R.id.name_view)
                companyname_view =  itemView.findViewById(R.id.companyname_view)
                mobile_view = itemView.findViewById(R.id.mobile_view)
                type_view = itemView.findViewById(R.id.type_view)
                email_view = itemView.findViewById(R.id.email_view)
                convertedby_view = itemView.findViewById(R.id.convertedby_view)

            }

            override fun onClick(v: View) {

                val itemPosition = customers_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                //toast("CUSTOMER_ID"+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)

                var intent = Intent(this@AllCustomers,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",UserDataManager?.customersList?.get(itemPosition!!)?.customerid as Int)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)


            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when(buttonView?.id)
        {
           /* R.id.togglebutton_filters ->
            {
                if(isChecked)
                {
                    layout_filters?.visibility = View.VISIBLE
                }
                else if(!isChecked)
                {
                    layout_filters?.visibility = View.GONE

                }
            }*/
        }
    }

}
