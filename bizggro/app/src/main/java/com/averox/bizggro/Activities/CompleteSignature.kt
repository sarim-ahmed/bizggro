package com.averox.bizggro.Activities

import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.webkit.WebView
import com.averox.bizggro.R
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import org.jetbrains.anko.find
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.TextView
import com.averox.bizggro.UserManagement.DocSignDataManager
import com.averox.bizggro.UserManagement.LoginManager
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*
import android.text.format.Formatter.formatIpAddress
import java.net.SocketException
import android.text.format.Formatter.formatIpAddress
import android.view.View
import android.widget.Button
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import org.apache.http.NameValuePair
import org.apache.http.conn.util.InetAddressUtils
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject


class CompleteSignature : AppCompatActivity(),View.OnClickListener{



    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var title: String? = null
    private var tasks_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var item_position: Int? = null
    private var progressDialog: ProgressDialog? = null


/*    val SAMPLE_FILE = "http://files.bizggro.com/4/SignedDocuments/1494_1494.pdf"

    private var pdfView: PDFView? = null
    private var pageNumber: Int? = 0
    private var pdfFileName: String? = null*/

    private var pdfView: WebView? = null
    private var pdfUrl: String? = null

    private var ipAddress: String? = null
    private var doc: String? = null
    private var dateSigned: String? = null
    private var btn_sign: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_signature)

        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")

        toolbar_title = findViewById(R.id.toolbar_title)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_sign = findViewById(R.id.btn_sign)
        btn_sign?.setOnClickListener(this)
        btn_sign?.isClickable = false

        var intent = intent
        var extras = intent.extras

        item_position = extras?.getInt("ITEM_POSITION")

        pdfUrl = DocSignDataManager?.signatureRequestList?.get(item_position!!)?.documentUrl.toString()
        doc = ""+DocSignDataManager?.signatureRequestList?.get(item_position!!)?.originalDocId+"_"+DocSignDataManager?.signatureRequestList?.get(item_position!!)?.docId+".pdf"
        dateSigned = DocSignDataManager?.signatureRequestList?.get(item_position!!)?.dateSigned.toString()



        toolbar_title?.setText(title)

        tasks_view = findViewById(R.id.tasks_view)

        loginManager = LoginManager(Constants.context!!)

        //pdfView = findViewById(R.id.pdfViewdisplayFromAsset(SAMPLE_FILE);

        pdfView = findViewById(R.id.pdfView)

        pdfView?.getSettings()?.setJavaScriptEnabled(true)

        loadPDF(pdfUrl)

        Log.d("response","ip address: "+getIPAddress())

        try {
            ipAddress = getIPAddress()
        } catch (e: Exception) {
        } finally {
        }



    }




    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {

        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }

    fun loadPDF(pdfUrl: String?)
    {


        pdfView?.loadUrl("http://docs.google.com/gview?embedded=true&url=" +pdfUrl)

        pdfView?.setWebViewClient(object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                // do your stuff hereprogressbar.setVisibility(View.GONE)
                btn_sign?.isClickable = true
            }
        })
    }

/*
    private fun displayFromAsset(assetFileName: String) {
        pdfFileName = assetFileName

        pdfView?.fromAsset(SAMPLE_FILE)
                ?.defaultPage(pageNumber!!)
                ?.enableSwipe(true)

                ?.swipeHorizontal(false)
                ?.onPageChange(OnPageChangeListener { page, pageCount ->  })
                ?.enableAnnotationRendering(true)
                ?.onLoad(OnLoadCompleteListener {  })
                ?.scrollHandle(DefaultScrollHandle(this))
                ?.load()
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
            Log.d("","")
    }

    override fun loadComplete(nbPages: Int) {
        Log.d("","")
    }
*/
    fun getIPAddress(): String?
    {
        try {
            //Enumerate all the network interfaces

            var en: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()

            while (en!!.hasMoreElements()) {

                var intf = en.nextElement()
                var enumIpAddr: Enumeration<InetAddress> = intf.getInetAddresses()

                for (en in NetworkInterface.getNetworkInterfaces()) {

                    while (enumIpAddr!!.hasMoreElements()) {
                        val inetAddress = enumIpAddr.nextElement()

                        for (enumIpAddr in intf.getInetAddresses()) {


                            if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress())) {
                                return inetAddress.getHostAddress().toString();
                            }
                        }
                    }

                }

            }
        }
        catch (e: SocketException)
        {
            e.printStackTrace();
        }
        return null;
    }


    fun completeSignature(ipAddress: String?,doc: String?,contactId: Int?, dateSigned: String?)
    {

        progressDialog = ProgressDialog.show(this@CompleteSignature, "Signing in", "Please wait....", true)
        progressDialog?.setCancelable(false)
            // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
            var requestParameters: LinkedList<NameValuePair> = LinkedList()
            requestParameters.add(BasicNameValuePair(Parameters.IP,ipAddress.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.Doc,doc.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.Email,loginManager?.getEmail().toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ContactId,contactId.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.UserId,loginManager?.getUserID().toString()))
            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
            requestParameters.add(BasicNameValuePair(Parameters.DateSigned,dateSigned.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.SigningPersonName,loginManager?.getFullName().toString()))

            //requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))

            /*       requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.CategoryID,categoryID.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.TransactionTypeID,transactionTypeID.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
                   requestParameters.add(BasicNameValuePair(Parameters.Description,description.toString()))
           */

            UrlBuilder?.setUrl(Module.documentSign, RequestCode.completesignatures,requestParameters)

            Log.d("response", UrlBuilder.getUrl())
            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    loader?.visibility = View.GONE

                }
                else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    loader?.visibility = View.GONE
                    alert(resources.getString(R.string.NoDataFound))


                }
                else
                {
                    val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                    try {
                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)



                        }

                    } catch (e: Exception) {

                        Log.d("response","exception: " +e.message.toString())
                    }

                    finally
                    {

                    }

                }

            },
                    Response.ErrorListener {
                        error ->
                        progressDialog?.dismiss()
                        Log.d("response","error: " +error.message.toString())
                        alert(resources.getString(R.string.networkProblem))
                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)

    }

    override fun onClick(v: View?) {

        when(v?.id)
        {
            R.id.btn_sign->
            {

                completeSignature(ipAddress,doc,0,dateSigned)

            }
        }

    }
}
