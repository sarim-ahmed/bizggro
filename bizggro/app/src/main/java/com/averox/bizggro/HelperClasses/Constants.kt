package com.averox.bizggro.HelperClasses

import android.content.Context

/**
 * Created by Sarim on 11/28/2017.
 */
public object Constants
{

    var context: Context? = null
    const val URL_BASE = ""
    const val URL_BASE_USERS_MODULE = URL_BASE+""
    const val URL_BASE_BOOKKEEPING_MODULE =  URL_BASE+""
    const val URL_BASE_PROJECTS_MODULE =  URL_BASE+""
    const val URL_BASE_TASKS_MODULE =  URL_BASE+""
    const val URL_ADDTRANSACTION =  URL_BASE+""
    const val URL_UPDATETRANSACTION =  URL_BASE+""
    const val URL_TRANSACTIONTYPES =  URL_BASE+""
    const val NoDataFound = ""
    const val URL_GETCOUNTRIES_LIST = ""
    const val URL_GETCONTACTCATEGORIES = ""

    const val URL_ADDTASK =  URL_BASE+""
    const val URL_UPDATETASK =  URL_BASE+""




}