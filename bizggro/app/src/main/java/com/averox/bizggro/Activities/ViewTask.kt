package com.averox.bizggro.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.ProgressBar
import android.widget.TextView
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.TasksDataManager
import org.jetbrains.anko.find
import org.w3c.dom.Text

class ViewTask : AppCompatActivity() {

    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var title: String? = null
    private var users_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var item_position: Int? = null

    private var assignedBy_nameView: TextView? = null
    private var assignedBy_emailView: TextView? = null
    private var subject_view: TextView? = null
    private var priority_view: TextView? = null
    private var project_view: TextView? = null
    private var status_view: TextView? = null
    private var startDate_view: TextView? = null
    private var expected_endDate_view: TextView? = null
    private var description_view: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_task)


        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")

        toolbar_title = findViewById(R.id.toolbar_title)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        var intent = intent
        var extras = intent.extras

        title = extras?.getString("TITLE")
        item_position = extras?.getInt("ITEM_POSITION")

        assignedBy_nameView = findViewById(R.id.textview_name)
        assignedBy_emailView = findViewById(R.id.textview_email)
        subject_view = findViewById(R.id.textview_subject)
        priority_view = findViewById(R.id.textview_priority)
        project_view = findViewById(R.id.textview_project)
        status_view = findViewById(R.id.textview_status)
        startDate_view = findViewById(R.id.textview_startDate)
        expected_endDate_view = findViewById(R.id.textview_expectedEndDate)
        description_view = findViewById(R.id.textview_description)

        if(title!!.equals(resources.getString(R.string.assignedToMe)))
        {
            loadAssignedToMeData()
        }
            else if(title!!.equals(resources.getString(R.string.assignedByMe)))
        {
            loadAssignedByMeData()
        }






    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {

        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }

    fun loadAssignedToMeData()
    {
        assignedBy_nameView?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.UserName?.toString())
        //assignedBy_emailView?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.Name?.toString())
        subject_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.Title?.toString())
        priority_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.Priority?.toString())
        project_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.ProjectName?.toString())
        status_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.Status?.toString())
        startDate_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.StartDate?.toString())
        expected_endDate_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.EndDate?.toString())
        description_view?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.TaskDesc?.toString())


    }
    fun loadAssignedByMeData()
    {

        assignedBy_nameView?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.UserName?.toString())
        //assignedBy_emailView?.setText(TasksDataManager?.assignedToMe?.get(item_position!!)?.Name?.toString())
        subject_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.Title?.toString())
        priority_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.Priority?.toString())
        project_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.ProjectName?.toString())
        status_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.Status?.toString())
        startDate_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.StartDate?.toString())
        expected_endDate_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.EndDate?.toString())
        description_view?.setText(TasksDataManager?.assignedByMe?.get(item_position!!)?.TaskDesc?.toString())

    }
}
