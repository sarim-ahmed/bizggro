package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 2/16/2018.
 */
class LeadsDataManager
{
    companion object {
        public var leadsStatusList: ArrayList<LeadsStatusAdapter>? = null
        public var leadsList: ArrayList<LeadsDetailAdapter>? = null



        init {
            leadsStatusList = ArrayList<LeadsStatusAdapter>()
            leadsList = ArrayList<LeadsDetailAdapter>()
        }

        fun insertLeadsStatus(statusId: Any?, status: Any?) {
            leadsStatusList?.add(LeadsStatusAdapter(statusId,status))
        }

        fun insertLeads(contactId: Any?,firstname: Any?,lastname: Any?, email: Any?,
                         phone: Any?, mobile: Any?, address: Any?,creationDate: Any?,
                         city: Any?,state: Any?,countryId: Any?,details: Any?,
                        companyId: Any?,dateUpdated: Any?,createdById: Any?,
                        updatedBy: Any?,contactTypeId: Any?, companyName: Any?, categoryId: Any?,
                        zipCode: Any?, isHidden: Any?,leadID: Any?, leadTitle: Any?,
                        rating: Any?,leadIncharge: Any?, convertedDate: Any?,statusId:Any?,ownerId: Any?,
                         convertedByID: Any?, modifiedDate: Any?,annualRevenue: Any?,noOfEmployees: Any?,
                        leadSource: Any?,status: Any?, convertedByUserName: Any?,
                        category: Any?,
                        countryName: Any?, sourceName: Any?,inchargeName: Any?,ownerUserName: Any?)
        {
            leadsList?.add(LeadsDetailAdapter(contactId,firstname,lastname,email,
            phone, mobile,address,creationDate, city,state, countryId,details,
                    companyId,dateUpdated, createdById,
            updatedBy, contactTypeId,companyName,categoryId,
            zipCode, isHidden,leadID,leadTitle,
             rating,leadIncharge,convertedDate,statusId,ownerId,
             convertedByID,modifiedDate,annualRevenue,noOfEmployees,
            leadSource,status,convertedByUserName,
                    category, countryName, sourceName,inchargeName,ownerUserName))
        }

    }

}