package com.averox.bizggro.Activities

import android.app.Dialog
import android.app.FragmentManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.averox.bizggro.Fragments.MainDrawerMenu
import com.averox.bizggro.R
import android.widget.ExpandableListView
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import org.json.JSONObject
import android.graphics.Color.parseColor
import android.os.Build
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.text.Layout
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import com.android.volley.DefaultRetryPolicy
import com.averox.bizggro.Adapters.CustomersTypeAdapter
import com.averox.bizggro.Adapters.IncomeExpenseChartDataAdapter
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.UserManagement.*
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import kotlinx.android.synthetic.main.activity_all_transactions.*
import kotlinx.android.synthetic.main.activity_main.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.*
import java.text.SimpleDateFormat

import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(),View.OnClickListener,OnChartValueSelectedListener {
    override fun onNothingSelected() {
       Log.d("response","")
    }

    override fun onValueSelected(e: Entry?, dataSetIndex: Int, h: Highlight?) {
        pieChart?.setCenterText(e?.data.toString())



    }

    private var drawerLayout: DrawerLayout? = null
    private var drawer_listView: ListView? = null
    private var drawerToggle: ActionBarDrawerToggle? = null
    private var homeToolbar: Toolbar? = null

    private var progressDialog: ProgressDialog? = null

    private var pieChart: PieChart? = null

    private var fragment_drawer: MainDrawerMenu? = null
    private lateinit var fragManager: FragmentManager

    private var drawer_content: FrameLayout? = null


    private var loginManager: LoginManager? = null
    private var yvalues: ArrayList<Entry>? = null

    private var currentMonth: String = ""
    private var currentDate: String = ""

    private var button_newTransaction: RoundedImageView? = null
    private var button_newContact: RoundedImageView? = null

    private var customersCount: Int = 0
    private var contactsCount: Int = 0
    private var leadsCount: Int = 0
    private var opportunitiesCount: Int = 0


    private var textview_customersCount: TextView? = null
    private var textview_contactsCount: TextView? = null
    private var textview_leadsCount: TextView? = null
    private var textview_opportunitiesCount: TextView? = null

    var monthName = arrayOf("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        super.setTitle("")


        /*homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)*/
        //homeToolbar!!.setNavigationIcon(R.drawable.)


        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        loginManager = LoginManager(this@MainActivity)
        homeToolbar!!.setNavigationIcon(R.mipmap.ic_navigation)

        //checkLoginCredentials()

        //viewPopUp()

        drawerLayout = findViewById(R.id.drawer_layout)
        //drawer_listView = findViewById(R.id.drawer_list)

        drawer_content = findViewById(R.id.drawer_content)

        pieChart = findViewById(R.id.pieChart)

        button_newContact = findViewById(R.id.button_newContact)
        button_newContact?.setOnClickListener(this)
        button_newTransaction = findViewById(R.id.button_newTransaction)
        button_newTransaction?.setOnClickListener(this)

        textview_contactsCount = findViewById(R.id.textview_contactsCount)
        textview_customersCount = findViewById(R.id.textview_customersCount)
        textview_leadsCount = findViewById(R.id.textview_leadsCount)
        textview_opportunitiesCount = findViewById(R.id.textview_OpportunitiesCount)


        setCurrentMonth()





      /*  mChart?.setUsePercentValues(true);
        mChart?.getDescription().setEnabled(false);
        mChart?.setExtraOffsets(5, 10, 5, 5);

        mChart?.setDragDecelerationFrictionCoef(0.95f);

        mChart?.setCenterTextTypeface(mTfLight);
        mChart?.setCenterText(generateCenterSpannableText());

        mChart?.setDrawHoleEnabled(true);
        mChart?.setHoleColor(Color.WHITE);

        mChart?.setTransparentCircleColor(Color.WHITE);
        mChart?.setTransparentCircleAlpha(110);

        mChart?.setHoleRadius(58f);
        mChart?.setTransparentCircleRadius(61f);

        mChart?.setDrawCenterText(true);

        mChart?.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart?.setRotationEnabled(true);
        mChart?.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart?.setOnChartValueSelectedListener(this);

        setData(4, 100);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);*/


        /* fragManager = fragmentManager
         fragment_drawer = MainDrawerMenu()
         var transaction = fragManager.beginTransaction()
         transaction.replace(R.id.drawer_content,fragment_drawer,"")
         transaction.commit()
 */

        drawerToggle = object : ActionBarDrawerToggle(this, drawerLayout, homeToolbar, R.string.drawer_open, R.string.drawer_close) {


            override fun onDrawerClosed(drawerView: View?) {
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerOpened(drawerView: View?) {
                super.onDrawerOpened(drawerView)
            }
        }

        drawerLayout!!.setDrawerListener(drawerToggle)


        fetchAllProjects()

        Log.d("response","on create method")

       /* var loadData = LoadData(this)
        loadData.execute()*/

    }

      /*fun setData( count: Int , range: Float) {

        var mult = range;

        var entries =  ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (i in 0..count) {
            entries.add(PieEntry((0f), (((Math.random() * mult) + mult / 5).toString()),
                    mParties[i % mParties.length],
                    getResources().getDrawable(R.drawable.star)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        var data = PieData(dataDir);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(mTfLight);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
}*/

    override fun onBackPressed() {
        super.onBackPressed()

        try {
            UserDataManager?.companyCategoriesList?.clear()
            UserDataManager?.allInvoicesList?.clear()
            UserDataManager?.allPOsList?.clear()
            UserDataManager?.transactionTypeList?.clear()
            UserDataManager?.incomeExpenseDataList?.clear()
            UserDataManager?.transactionTypeList?.clear()
            UserDataManager?.customerTypeList ?.clear()
            ContactsDataManager?.countriesList?.clear()
            ContactsDataManager?.categoriesList?.clear()
            LeadsDataManager?.leadsStatusList?.clear()
            OpportunitiesDataManager?.typesList?.clear()
            OpportunitiesDataManager?.stagesList?.clear()
            CustomersDataManager?.campaingsList?.clear()
            CustomersDataManager?.contractsList?.clear()
            TasksDataManager?.tasksStatus?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onClick(v: View?) {
when(v?.id)
{
    R.id.button_signout ->
    {
        try {
            loginManager?.logoutUser()
            overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)
            finishAffinity()
        } catch (e: Exception) {
        } finally {
        }
    }

    R.id.button_newTransaction->
    {
        var intent =  Intent(this@MainActivity, NewTransaction::class.java)
        intent.putExtra("TITLE",resources.getString(R.string.string_title_newtransaction))

        startActivity(intent)
        overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)
    }

    R.id.button_newContact->
    {
        var intent =  Intent(this@MainActivity,NewContact::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)
    }
}
    }


    /*internal inner class DrawerAdapter(private val context: Context) : BaseAdapter() {


        var drawer_enteries: Array<String>
        var images = intArrayOf(R.drawable.ic_colored_facebook, R.drawable.ic_colored_twitter, R.drawable.ic_colored_linkedin, R.drawable.ic_colored_instagram, R.drawable.ic_colored_pinterest, R.drawable.ic_colored_googleplus)

        init {
            drawer_enteries = context.resources.getStringArray(R.array.drawer_enteries)
        }

        override fun getCount(): Int {
            return drawer_enteries.size
        }

        override fun getItem(position: Int): Any {
            return drawer_enteries[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            var row: View? = null
            if (convertView == null) {
                *//*val inflater = context.getSystemService() as LayoutInflater
               row = inflater.inflate(R.layout.navigation_drawer_items, parent, false)*//*
                LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_drawer_items, parent, false);

            } else {
                row = convertView
            }
            val drawer_item_text = row!!.findViewById(R.id.drawer_item_text) as TextView
            val drawer_item_icon = row.findViewById(R.id.drawer_item_icon) as ImageView
            drawer_item_text.text = drawer_enteries[position]
            try {
                drawer_item_icon.setImageResource(images[position])
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("icon", e.message)
            }

            return row
        }
    }*/


    fun  fetchAllProjects()
    {

        try {

            progressDialog = ProgressDialog.show(this, "Accessing Data", "Please wait....", true)
            progressDialog?.setCancelable(false)

            UrlBuilder.setAllProjectsUrl(loginManager?.getCompanyID())
            var request = object : JsonObjectRequest(Method.GET,UrlBuilder.getAllProjectsUrl(),null, Response.Listener<JSONObject> {
                response ->

                Log.d("response",response.toString())
                Log.d("response","in projects response")

                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(R.string.NoDataFound))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    Log.d("response","in projects if")

                }
                else
                {
                    try {
                        Log.d("response","in projects else")

                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var projectID = result.getInt(JsonKeys.variables.KEY_PROJECTID)
                            var projectName = result.getString(JsonKeys.variables.KEY_PROJECTNAME)


                            UserDataManager.insertAllProjects(projectID,projectName)




                        }
                    } catch (e: Exception) {
                    } finally {
                    }

                }
                fetchCompanyCategories()

                getContactsCount()
                getCustomersCount()
                getLeadsCount()
                getOpportunitiesCount()


            },Response.ErrorListener {
                error ->

                progressDialog?.dismiss()

                fetchCompanyCategories()

                getContactsCount()
                getCustomersCount()
                getLeadsCount()
                getOpportunitiesCount()
            })
            {}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            fetchCompanyCategories()

            getContactsCount()
            getCustomersCount()
            getLeadsCount()
            getOpportunitiesCount()

        } finally {
        }
    }
    fun fetchCompanyCategories()
    {

        try {

            UrlBuilder.setCompanyCategoriesUrl(loginManager?.getCompanyID())
            var request = object : JsonObjectRequest(Method.GET,UrlBuilder.getCompanyCategoriesUrl(),null, Response.Listener<JSONObject> {
                response ->
                Log.d("response","in company response")

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(R.string.NoDataFound))
                {

                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    Log.d("response","in company if")

                }
                else
                {
                    Log.d("response","in company else")

                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var categoryID = result.getInt(JsonKeys.variables.KEY_CATEGORYID)
                            var categoryName = result.getString(JsonKeys.variables.KEY_CATEGORYNAME)
                            var irs_name = result.getString(JsonKeys.variables.KEY_IRSNAME)
                            var irs_category_id =  result.getInt(JsonKeys.variables.KEY_IRSCATEGORYID)


                            UserDataManager.insertCompanyCategories(categoryName,categoryID,irs_name,irs_category_id)




                        }
                    } catch (e: Exception) {
                    } finally {
                    }
                }
                fetchAllInvoices()



            },Response.ErrorListener {
                error ->

                 progressDialog?.dismiss()
                fetchAllInvoices()

            })
            {}

            ApplicationController.instance?.addToRequestQueue(request)

        } catch (e: Exception) {
            fetchAllInvoices()

        } finally {
        }
    }

    fun fetchAllInvoices()
    {



        try {

            UrlBuilder.setAllInvoicesUrl(loginManager?.getCompanyID())
            var request = object : JsonObjectRequest(Method.GET,UrlBuilder.getAllInvoicesUrl(),null, Response.Listener<JSONObject> {
                response ->
                Log.d("response","in company response")

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(R.string.NoDataFound))
                {

                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    Log.d("response","in invoices if")

                }

                else

                {

                    Log.d("response","in invoices else")

                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for (i in 0..data.length() - 1) {
                            var result = data.getJSONObject(i)

                            var invoiceID = result.getInt(JsonKeys.variables.KEY_INVOICEID)
                            var invoiceNumber = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                            var project_name = result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                            var po_number = result.getString(JsonKeys.variables.KEY_PONUMBER)
                            var due_date = result.getString(JsonKeys.variables.KEY_DUEDATE)
                            var status = result.getString(JsonKeys.variables.KEY_TRANSACTIONSTATUS)
                            var statusID = result.getInt(JsonKeys.variables.KEY_STATUSID)

                            var attachment = result.getString(JsonKeys.variables.KEY_ATTACHMENT)

                            UserDataManager.insertAllInvoices(invoiceID, invoiceNumber, project_name, po_number, due_date, status, statusID, attachment)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }

                }




               // fetchAllPOs()
                fetchTransactionTypes()

            },Response.ErrorListener {
                error ->

                 progressDialog?.dismiss()
                fetchTransactionTypes()

            })
            {}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            fetchTransactionTypes()

        } finally {
        }
    }
    fun fetchAllPOs()
    {

        try {
            UrlBuilder.setAllPOsUrl(loginManager?.getCompanyID())
            Log.d("response",""+UrlBuilder.getAllPOsUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getAllPOsUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var invoice_number = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                            var project_name = result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                            var po_number = result.getString(JsonKeys.variables.KEY_PONUMBER)
                            var duedate = result.getString(JsonKeys.variables.KEY_DUEDATE)
                            var status = result.getString(JsonKeys.variables.KEY_TRANSACTIONSTATUS)
                            var attachment = result.getString(JsonKeys.variables.KEY_ATTACHMENT)

                            UserDataManager.insertAllPOs(invoice_number,project_name,po_number,duedate,status,attachment)





                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
            //fetchTransactionTypes()
            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()

        } finally {
        }

    }

    fun fetchTransactionTypes()
    {

        try {



            var request = object : JsonObjectRequest(Method.GET, Constants.URL_TRANSACTIONTYPES,null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var transactionID = result.getInt(JsonKeys.variables.KEY_ID)
                            var transactionType = result.getString(JsonKeys.variables.KEY_TYPE)

                            UserDataManager.insertTransactionTypes(transactionID,transactionType)





                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
                fetchCustomerTypes()

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchCustomerTypes()
                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchCustomerTypes()

        } finally {
        }

    }

    fun fetchCustomerTypes()
    {
        try {

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.customers, RequestCode.getcustomertypes,requestParameters)

            Log.d("response", UrlBuilder.getUrl())



            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var typeid = result.getInt(JsonKeys.variables.TYPEID)
                            var type = result.getString(JsonKeys.variables.TYPE)
                            var companyid = result.getInt(JsonKeys.variables.CompanyId)
                            var addedby = result.getInt(JsonKeys.variables.KEY_ADDEDBY)
                            var dateadded = result.getString(JsonKeys.variables.KEY_DATEADDED)
                            var updatedby = result.getString(JsonKeys.variables.UPDATEDBY)
                            var dateupdated = result.getString(JsonKeys.variables.DATEUPDATED)
                            var username = result.getString(JsonKeys.variables.Username)

                            UserDataManager.insertCustomerTypes(typeid, type, companyid, addedby,
                                    dateadded, updatedby,dateupdated,
                                    username)





                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
                    fetchCountriesList()
            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchCountriesList()
                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchCountriesList()

        } finally {
        }

    }

    fun fetchCountriesList()
    {
        try {


            var request = object : JsonObjectRequest(Method.GET, Constants.URL_GETCOUNTRIES_LIST,null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var countryid = result.getInt(JsonKeys.variables.CountryId)
                            var name = result.getString(JsonKeys.variables.Name)

                            ContactsDataManager.insertCountries(countryid,name)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
                fetchContactCategories()

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchContactCategories()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchContactCategories()

        } finally {
        }
    }



    fun fetchContactCategories()
    {
        try {
            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.contacts, RequestCode.getcategories,requestParameters)
            Log.d("response",""+UrlBuilder.getUrl());



            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var categoryid = result.getInt(JsonKeys.variables.CategoryId)
                            var categoryName = result.getString(JsonKeys.variables.Category)

                            ContactsDataManager.insertCategories(categoryid,categoryName)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
                fetchLeadsStatus()

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchLeadsStatus()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchLeadsStatus()

        } finally {
        }
    }

    fun fetchLeadsStatus()
    {
        try {
            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.leads, RequestCode.getstatus,requestParameters)

            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var statusId = result.getInt(JsonKeys.variables.StatusId)
                            var status = result.getString(JsonKeys.variables.Status)

                            LeadsDataManager?.insertLeadsStatus(statusId,status)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
fetchOpportunityStages()

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchOpportunityStages()



                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchOpportunityStages()



        } finally {
        }
    }

    fun fetchOpportunityStages()
    {
        try {
            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.opportunity, RequestCode.getstages,requestParameters)

            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                }
                else
                {
                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var stageId = result.getInt(JsonKeys.variables.StageId)
                            var stage = result.getString(JsonKeys.variables.Stage)

                            OpportunitiesDataManager?.insertStages(stageId,stage)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
            fetchOpportunityTypes()
            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchOpportunityTypes()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchOpportunityTypes()

        } finally {
        }
    }

    fun fetchOpportunityTypes()
    {
        try {
            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.opportunity, RequestCode.gettypes,requestParameters)

            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                }
                else
                {
                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var typeId = result.getInt(JsonKeys.variables.TypeId)
                            var type = result.getString(JsonKeys.variables.Type)

                            OpportunitiesDataManager.insertTypes(typeId,type)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
               fetchIncomeExpense()


            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()
                        fetchIncomeExpense()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
            fetchIncomeExpense()


        } finally {
        }
    }

    fun fetchIncomeExpense()
    {
        try {
            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.bookkeeping, RequestCode.getincomeexpensechartdata,requestParameters)

            Log.d("response", UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    UserDataManager.insertIncomeExpenseData("Income",0)
                    UserDataManager.insertIncomeExpenseData("Expense",0)


                    setPieChartData(UserDataManager?.incomeExpenseDataList!!)


                }
                else
                {
                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var name = result.getString(JsonKeys.variables.Name)
                            var amount = result.getInt(JsonKeys.variables.Amount)

                            UserDataManager.insertIncomeExpenseData(name,amount)

                            Log.d("response","data loop: "+UserDataManager?.incomeExpenseDataList?.get(i)?.amount)


                        }
                    } catch (e: Exception) {
                    } finally {
                    }

                    setPieChartData(UserDataManager?.incomeExpenseDataList!!)


                }

                fetchTasksStatus()




            },
                    Response.ErrorListener {
                        error ->

                        fetchTasksStatus()
                        Log.d("response",""+error.message)


                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            fetchTasksStatus()

        } finally {
        }
    }

    /*  internal class LoadData(context: Context): AsyncTask<Void,Void,Void>()
      {

          var loginManager = LoginManager(Constants.context!!)

          private var context: Context? = null

        init {
            this.context = context
        }


          private var progressDialog: ProgressDialog? = null


          override fun onPreExecute() {

              Log.d("response","in prexecute")
              progressDialog = ProgressDialog.show(context, "Accessing Data", "Please wait....", true)
              progressDialog?.setCancelable(false)

          }
          override fun doInBackground(vararg params: Void?): Void? {
              fetchAllProjects()
              fetchCompanyCategories()

              return null
          }

          override fun onPostExecute(result: Void?) {

             progressDialog?.dismiss()

          }


      }*/

    fun setCurrentMonth() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy")
        val formattedDate = df.format(c.time)

        val month = monthName[c.get(Calendar.MONTH)]
        Log.d("response",month)



        val cd = SimpleDateFormat("yyyy/MM/dd")
        val fd= cd.format(c.time)

        currentDate = fd

        currentMonth = month+","+formattedDate
    }


    fun setPieChartData(arraylist: ArrayList<IncomeExpenseChartDataAdapter>)
    {
        try {
            Log.d("response","in set Data")

            pieChart?.setUsePercentValues(true);

            var yvalues = mutableListOf<Entry>()

            var count = 0

            Log.d("response","arraylist size"+ arraylist!!.size)
            val xVals = ArrayList<String>()

            for(i in 0..arraylist!!.size - 1)
            {
                var amount =   arraylist.get(i).amount?.toDouble()?.toFloat()
                yvalues?.add(Entry(amount!!,count,arraylist?.get(i).amount))
                count++
                xVals.add(arraylist?.get(i).name.toString())

            }

            var dataSet = PieDataSet(yvalues,null)



            var data = PieData(xVals,dataSet)

            data.setValueFormatter(PercentFormatter());
            data.setValueTextColor(resources.getColor(R.color.smokeWhite))

            dataSet.valueTextSize = 12f



            dataSet.sliceSpace = 3f
            dataSet.setSelectionShift(5f);

            dataSet.setColors(mutableListOf(resources.getColor(R.color.expense_color),resources.getColor(R.color.income_color)))
            pieChart?.setData(data);
            pieChart?.isSelected = true

            pieChart?.setUsePercentValues(true)
            pieChart?.valuesToHighlight()
            //pieChart?.setDrawHoleEnabled(false)
            pieChart?.setDescription(currentMonth)

            pieChart?.animate()

            pieChart?.animateXY(1400, 1400)
            pieChart?.setOnChartValueSelectedListener(this)
            pieChart?.highlightValue(0,0)
            pieChart?.setCenterText(yvalues.get(0).data.toString())

        } catch (e: Exception) {
            Log.d("response",e.message.toString())
        } finally {
        }

    }

    fun getCustomersCount()
    {
        var fromDate = "1918/01/01"


            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,currentDate.toString()))
            UrlBuilder?.setUrl(Module.customers, RequestCode.getcustomers,requestParameters)

            Log.d("response", UrlBuilder.getUrl())
            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->






                try {

                    val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)


                    if(data.length() == 0)
                    {
                        textview_customersCount?.setText(""+0)

                    }
                    else {
                        customersCount = data.length().toInt()

                        textview_customersCount?.setText(""+customersCount)
                    }



                    } catch (e: Exception) {

                        Log.d("response","exception: "+e.message.toString())
                    } finally {
                    }




            },
                    Response.ErrorListener {
                        error ->


                        Log.d("response",""+error.message)

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)


    }

    fun getContactsCount()
    {
        var fromDate = "1918/01/01"


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,currentDate.toString()))
        UrlBuilder?.setUrl(Module.contacts, RequestCode.getcontacts,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->





            try {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                if(data.length() == 0)
                {
                    textview_contactsCount?.setText(""+0)

                }
                else
                {
                    contactsCount = data.length()

                    textview_contactsCount?.setText(""+contactsCount)
                }




            } catch (e: Exception) {

                Log.d("response","exception: "+e.message.toString())
            } finally {
            }




        },
                Response.ErrorListener {
                    error ->

                    Log.d("response",""+error.message)


                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)


    }

    fun getLeadsCount()
    {

        var fromDate = "1918/01/01"

        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,currentDate.toString()))
        UrlBuilder?.setUrl(Module.leads, RequestCode.getleads,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->





            try {

                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)




                if(data.length() == 0)
                {
                    textview_leadsCount?.setText(" "+0)

                }
                else
                {
                    leadsCount = data.length()
                    textview_leadsCount?.setText(""+leadsCount)

                }


            } catch (e: Exception) {

                Log.d("response","exception: "+e.message.toString())
                getOpportunitiesCount()

            } finally {
            }

            getOpportunitiesCount()



        },
                Response.ErrorListener {
                    error ->
                    Log.d("response",""+error.message)

                getOpportunitiesCount()

                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)


    }

    fun getOpportunitiesCount()
    {
        var fromDate = "1918/01/01"


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,currentDate.toString()))

        UrlBuilder?.setUrl(Module.opportunity, RequestCode.getopportunities,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->





            try {

                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                if(data.length() == 0)
                {
                    textview_opportunitiesCount?.setText(""+0)

                }
                else
                {
                    opportunitiesCount = data.length()

                    textview_opportunitiesCount?.setText(""+opportunitiesCount)
                }




            } catch (e: Exception) {

                Log.d("response","exception: "+e.message.toString())
            } finally {
            }




        },
                Response.ErrorListener {
                    error ->

                    Log.d("response",""+error.message)


                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)


    }

    fun fetchTasksStatus()
    {

        try {
            TasksDataManager?.tasksStatus?.clear()

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.tasks, RequestCode.gettaskstatus,requestParameters)

            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response","tasks Status: "+response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)

                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var statusId = result.getInt(JsonKeys.variables.StatusID)
                            var status = result.getString(JsonKeys.variables.Status)

                            TasksDataManager?.insertTasksStatus(statusId,status)

                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }

               fetchUserGroups()
                //progressDialog?.dismiss()

            },
                    Response.ErrorListener {
                        error ->
                        Log.d("response",""+error.message)

                       fetchUserGroups()
                        //progressDialog?.dismiss()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            fetchUserGroups()
            //progressDialog?.dismiss()



        } finally {
        }
    }


    fun fetchUserGroups()
    {

        try {
            //TasksDataManager?.tasksStatus?.clear()

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
            requestParameters.add(BasicNameValuePair(Parameters.UserID,loginManager?.getUserID().toString()))


            UrlBuilder?.setUrl(Module.users, RequestCode.getusergroups,requestParameters)

            Log.d("response","USER GROUPS URL: "+UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response","USER GROUPS: "+response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    progressDialog?.dismiss()

                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var groupId = result.getInt(JsonKeys.variables.GroupID)
                            var groupName = result.getString(JsonKeys.variables.GroupName)
                            var companyID = result.getInt(JsonKeys.variables.CompanyID)
                            var dateCreated = result.getString(JsonKeys.variables.DateCreated)
                            var dateUpdated = result.getString(JsonKeys.variables.DateUpdated)
                            var createdBy = result.getInt(JsonKeys.variables.CreatedBy)

                            UserDataManager?.insertUserGroups(groupId,groupName,companyID,dateCreated,dateUpdated,createdBy)

                        }

                    } catch (e: Exception) {
                    } finally {
                    }
fetchTasksPriorities()

                }

            },
                    Response.ErrorListener {
                        error ->

                        fetchTasksPriorities()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            fetchTasksPriorities()



        } finally {
        }
    }


    fun fetchTasksPriorities()
    {

        try {
            //TasksDataManager?.tasksStatus?.clear()

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.tasks, RequestCode.gettaskpriorities,requestParameters)

            Log.d("response","USER GROUPS URL: "+UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response","USER GROUPS: "+response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    fetchUsersInfo()
                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var priorityId = result.getInt(JsonKeys.variables.PriorityID)
                            var priorityName = result.getString(JsonKeys.variables.Priority)


                            TasksDataManager?.insertTaskPriority(priorityId,priorityName)

                        }

                    } catch (e: Exception) {
                    } finally {
                    }
                    fetchUsersInfo()

                }

            },
                    Response.ErrorListener {
                        error ->

                        fetchUsersInfo()
                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            fetchUsersInfo()


        } finally {
        }
    }

    fun fetchUsersInfo()
    {

        try {
            //TasksDataManager?.tasksStatus?.clear()

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.UserID,loginManager?.getUserID().toString()))
            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.users, RequestCode.getusers,requestParameters)

            Log.d("response","USERS  URL: "+UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response","USERS : "+response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    progressDialog?.dismiss()

                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var userId = result.getInt(JsonKeys.variables.KEY_USERID)
                            var firstName = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                            var lastName = result.getString(JsonKeys.variables.KEY_LASTNAME)
                            var fullName = firstName +" "+ lastName


                            UserDataManager?.insertUsersInfo(userId,firstName,lastName,fullName)

                        }

                    } catch (e: Exception) {
                    } finally {
                    }
                    progressDialog?.dismiss()


                }

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception)
        {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()
        }
        finally {
        }


    }

    fun checkLoginCredentials()
    {

                var email = loginManager?.getEmail()
                var password = loginManager?.getPassword()


                UrlBuilder.setLoginUrl(email!!, password!!)
                Log.d("response", UrlBuilder.getLoginUrl())


                var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getLoginUrl(), null, Response.Listener<JSONObject> {

                    response ->

                    try {
                        //Log.d("response", response.getJSONObject("data").toString())

                        //alert(response.getString("ID")).show()

                        if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                        {
                           // progressDialog?.dismiss()

                            //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                            var alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)


                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {
                                    try {
                                        UserDataManager?.companyCategoriesList?.clear()
                                        UserDataManager?.allInvoicesList?.clear()
                                        UserDataManager?.allPOsList?.clear()
                                        UserDataManager?.transactionTypeList?.clear()
                                        UserDataManager?.incomeExpenseDataList?.clear()
                                        UserDataManager?.transactionTypeList?.clear()
                                        UserDataManager?.customerTypeList ?.clear()
                                        ContactsDataManager?.countriesList?.clear()
                                        ContactsDataManager?.categoriesList?.clear()
                                        LeadsDataManager?.leadsStatusList?.clear()
                                        OpportunitiesDataManager?.typesList?.clear()
                                        OpportunitiesDataManager?.stagesList?.clear()
                                        CustomersDataManager?.campaingsList?.clear()
                                        CustomersDataManager?.contractsList?.clear()
                                        TasksDataManager?.tasksStatus?.clear()

                                    } catch (e: Exception) {
                                    } finally {
                                    }

                                    loginManager?.logoutUser()
                                    overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)
                                    finishAffinity()

                                }
                            }, 2 * 1000)    //wait for 5 seconds



                        }
                       else
                        {

                        }

                        // alert(result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME)).show()
                        // Toast.makeText(this,result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME), Toast.LENGTH_LONG).show()

                    } catch(e: Exception) {
                        //progressDialog?.dismiss()

                        var alert = AlertManager("Network Problem!",3000,this)
                        Log.d("response","in volley exception ")


                        Log.d("response",""+e.message)
                        Log.d("response", "error "+e.printStackTrace().toString())

                    }


                },
                        Response.ErrorListener {

                            error ->

                            //progressDialog?.dismiss()
                            try {
                                //progressDialog?.dismiss()
                            } catch (e: Exception) {
                            } finally {
                            }

                            Log.d("response","in volley error ")

                            var alert = AlertManager("Network Problem!",3000,this)

                            Log.d("response", "error "+error.message)
                            //Toast.makeText(this, "error " +error.message, Toast.LENGTH_LONG).show()


                        }
                ) {}

                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                ApplicationController.instance?.addToRequestQueue(request)

                // progressDialog?.dismiss()





    }

    fun viewPopUp()
    {
        try {

            val alertDialog = AlertDialog.Builder(this@MainActivity).create()



            var message = TextView(this);
            message?.setPadding(40,0,20,0)
            val s = SpannableString("https://bizggro.com/privacy-policy/")

            var msg = "We have updated our privacy policy \nYou can view by following link:\n"
            Linkify.addLinks(s,Linkify.WEB_URLS)

            //alertDialog?.setMessage(s)

            message?.text = s
            message.setMovementMethod(LinkMovementMethod.getInstance());

            alertDialog?.setMessage(msg)
            alertDialog?.setView(message)
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK") { dialog, which ->




            }


            alertDialog.show()




        } catch (e: Exception) {
        } finally {
        }
    }
}
