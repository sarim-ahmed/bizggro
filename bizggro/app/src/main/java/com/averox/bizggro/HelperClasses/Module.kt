package com.averox.bizggro.HelperClasses

/**
 * Created by Sarim on 12/26/2017.
 */
object Module {

    const val users = "users.aspx"
    const val bookkeeping = "bookkeeping.aspx"
    const val customers = "customers.aspx"
    const val contacts = "contacts.aspx"
    const val leads = "leads.aspx"
    const val opportunity= "opportunity.aspx"
    const val tasks = "tasks.aspx"
    const val documentSign = "documentsign.aspx"

}