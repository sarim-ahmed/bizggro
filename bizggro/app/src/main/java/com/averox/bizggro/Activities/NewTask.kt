package com.averox.bizggro.Activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.provider.MediaStore
import android.support.v7.widget.Toolbar
import android.util.Base64
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.TasksDataManager
import com.averox.bizggro.UserManagement.UserDataManager
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NewTask : AppCompatActivity(),View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var loginManager: LoginManager? = null
    private var input_startDate: TextView? = null
    private var input_startTime: TextView? = null
    private var input_endDate: TextView? = null
    private var input_endTime: TextView? = null
    private var spinner_project: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var spinner_status: Spinner? = null
    private var statusNameArray: ArrayList<String>? = null
    private var spinner_group: Spinner? = null
    private var groupNameArray: ArrayList<String>? = null
    private var spinner_priority: Spinner? = null
    private var priorityNameArray: ArrayList<String>? = null
    private var spinner_incharge: Spinner? = null
    private var inchargeNameArray: ArrayList<String>? = null
    private var imageview_attached_doc: ImageView? = null
    private var ic_attach_task_doc: ImageButton? = null
    private var attachment: Boolean = false
    private var base64String: String = ""
    private lateinit var marshMallowPermission: MarshMallowPermission


    private var imageFileuri: Uri? = null
    private var bitmap_object: Bitmap? = null
    private var imageFile: File? = null
    private var RESULT_LOAD_CAMERA = 0
    private var RESULT_LOAD_IMAGE = 1
    private var filename: String? = null
    private var bitmap: Bitmap? = null
    private var attachment_view: ImageView? = null
    private var fileSize: Long = 0
    private var fileName: String = ""

    private var progressDialog: ProgressDialog? = null

    private var projectID: Any? = null
    private var statusID: Any? = null
    private var priorityID: Any? = null
    private var groupID: Any? = null
    private var inchargeID: Any? = null
    private var ic_add_to: ImageView? = null
    private var ic_add_cc: ImageView? = null

    private var startDate = ""
    private var startTime = ""
    private var endDate = ""
    private var endTime = ""

    private var selectedNames: String = ""


    private var details: String = ""
    private var subject: String = ""

    private var textview_To: TextView? = null
    private var textview_Cc: TextView? = null

    private var button_submit: Button? = null
    private var input_details: EditText? = null
    private var input_subject: EditText? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_task)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        toolbar_title = findViewById(R.id.toolbar_title)


        loginManager = LoginManager(this)

        toolbar_title?.setText(resources.getString(R.string.string_title_newtask))


        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        input_startDate = findViewById(R.id.input_startDate)
        input_startDate?.setOnClickListener(this)
        input_startTime = findViewById(R.id.input_startTime)
        input_startTime?.setOnClickListener(this)
        input_endDate = findViewById(R.id.input_endDate)
        input_endDate?.setOnClickListener(this)
        input_endTime = findViewById(R.id.input_endTime)
        input_endTime?.setOnClickListener(this)

        projectNameArray = ArrayList()
        spinner_project = findViewById(R.id.spinner_project)
        statusNameArray = ArrayList()
        spinner_status = findViewById(R.id.spinner_status)
        groupNameArray = ArrayList()
        spinner_group = findViewById(R.id.spinner_group)
        spinner_priority = findViewById(R.id.spinner_priority)
        priorityNameArray = ArrayList()
        spinner_incharge = findViewById(R.id.spinner_incharge)
        inchargeNameArray = ArrayList()
        imageview_attached_doc = findViewById(R.id.imageview_attached_doc)
        ic_attach_task_doc = findViewById(R.id.ic_attach_task_doc)
        ic_attach_task_doc?.setOnClickListener(this)


        ic_add_cc = findViewById(R.id.ic_add_cc)
        ic_add_cc?.setOnClickListener(this)
        ic_add_to = findViewById(R.id.ic_add_to)
        ic_add_to?.setOnClickListener(this)

        textview_To = findViewById(R.id.textview_To)
        textview_Cc = findViewById(R.id.textview_Cc)

        input_details = findViewById(R.id.input_details)

        input_subject = findViewById(R.id.input_subject)

        button_submit = findViewById(R.id.button_submit)
        button_submit?.setOnClickListener(this)

        marshMallowPermission = MarshMallowPermission(this)


        setCurrentDate()
        setCurrentTime()

        loadSpinnerValues()


        spinner_project?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    projectID = ""
                }
                else
                {
                   projectID = UserDataManager?.allProjectsList?.get(position -1 )?.getProjectID()
                }
            }

        }
        spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    statusID = ""
                }
                else
                {
                    statusID = TasksDataManager?.tasksStatus?.get(position -1 )?.StatusID
                }
            }

        }
        spinner_priority?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    priorityID = ""
                }
                else
                {
                    priorityID = TasksDataManager?.tasksPriorities?.get(position -1 )?.priorityID
                }
            }

        }



        spinner_group?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

             if(position.equals(0))
             {

                 UserDataManager?.arraylist_selected_userIDs_Cc?.clear()
                 UserDataManager?.arraylist_selected_userIDs_To?.clear()
                 ic_add_to?.isClickable = false
                 ic_add_to?.isEnabled = false
                 ic_add_cc?.isClickable = false
                 ic_add_cc?.isEnabled = false

                 textview_To?.setText("")
                 textview_Cc?.setText("")
             }
             else
             {
                 ic_add_to?.isClickable = true
                 ic_add_to?.isEnabled = true
                 ic_add_cc?.isClickable = true
                 ic_add_cc?.isEnabled = true


                 groupID = UserDataManager?.userGroupsList?.get(position - 1)?.groupID

                 getGroupMembers()

                 textview_To?.setText("")
                 textview_Cc?.setText("")

             }
            }

        }

        spinner_incharge?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    inchargeID = ""
                }
                else
                {
                    inchargeID = UserDataManager?.usersInfoList?.get(position -1 )?.userID
                }
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }





    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }


    override fun onResume() {
        super.onResume()

        selectedNames = ""

        if(UserDataManager?.type.equals("TO")) {


            selectedNames = ""

            for (i in 0..UserDataManager?.arraylist_selected_userIDs_To!!.size - 1) {

                for (j in 0..UserDataManager?.groupMembersList!!.size - 1)
                    if (UserDataManager?.arraylist_selected_userIDs_To?.get(i)?.equals(UserDataManager?.groupMembersList?.get(j)?.userID) == true) {

                        selectedNames = selectedNames + UserDataManager?.groupMembersList?.get(j)?.fullName + ", "
                    }

            }

            textview_To?.setText(selectedNames)
        }


        else if(UserDataManager?.type.equals("CC")) {

            selectedNames = ""

            for (i in 0..UserDataManager?.arraylist_selected_userIDs_Cc!!.size - 1) {

                for (j in 0..UserDataManager?.groupMembersList!!.size - 1)
                    if (UserDataManager?.arraylist_selected_userIDs_Cc?.get(i)?.equals(UserDataManager?.groupMembersList?.get(j)?.userID) == true) {

                        selectedNames = selectedNames + UserDataManager?.groupMembersList?.get(j)?.fullName + ", "
                    }

            }

            textview_Cc?.setText(selectedNames)
        }



    }


    override fun onClick(v: View?) {
    when(v?.id)
    {

        R.id.input_startDate->
        {
            setStartDate()
        }
        R.id.input_startTime->
        {
           setStartTime()
        }
        R.id.input_endDate->
        {
            setEndDate()
        }
        R.id.input_endTime->
        {
            setEndTime()
        }
        R.id.ic_attach_task_doc ->
        {
            registerForContextMenu(ic_attach_task_doc)
            ic_attach_task_doc?.showContextMenu()
        }

        R.id.ic_add_to->
        {
            var intent = Intent(this@NewTask,MembersListActivity::class.java)

            intent.putExtra("TITLE","TO")

            startActivity(intent)

            overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)

        }
        R.id.ic_add_cc->
        {
            var intent = Intent(this@NewTask,MembersListActivity::class.java)
            intent.putExtra("TITLE","CC")

            startActivity(intent)

            overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)
        }

        R.id.button_submit->
        {
            details = input_details?.text.toString()
            subject = input_subject?.text.toString()

            if (subject?.isEmpty() || subject?.equals("") || subject?.equals(" ") || subject?.equals(null)) {
                input_subject?.setError("Enter Subject")
                input_subject?.isFocusable
            }
           else  if (spinner_project!!.selectedItemPosition == 0) {
                AlertManager("Select Project", 4000, this@NewTask)
            } else if (spinner_status!!.selectedItemPosition == 0) {
                AlertManager("Select Status", 4000, this@NewTask)
            }
            else if (spinner_priority!!.selectedItemPosition == 0) {
                AlertManager("Select Priority", 4000, this@NewTask)
            }
            else if (spinner_group!!.selectedItemPosition == 0) {
                AlertManager("Select Group", 4000, this@NewTask)
            }

            else if (UserDataManager?.arraylist_selected_userIDs_To!!.size.equals(0)) {
                AlertManager("Add TO Recipient", 4000, this@NewTask)
            }

            else if (UserDataManager?.arraylist_selected_userIDs_Cc!!.size.equals(0)) {
                AlertManager("Add CC Recipient", 4000, this@NewTask)
            }
            else if (spinner_incharge!!.selectedItemPosition == 0) {
                AlertManager("Select Incharge", 4000, this@NewTask)
            }
            else if (textview_To?.text?.toString()!!.isNullOrEmpty() || textview_To?.text?.toString()!!.isNullOrEmpty()) {
                AlertManager("Select Incharge", 4000, this@NewTask)
            }
            else if (spinner_incharge!!.selectedItemPosition == 0) {
                AlertManager("Select Incharge", 4000, this@NewTask)
            }else {
                addTask()
            }
            }

        }

    }



    fun getGroupMembers()
    {
        try {

            progressDialog = ProgressDialog.show(this@NewTask, "Loading Members..", "Please wait....", true)
            progressDialog?.setCancelable(false)

            UserDataManager?.groupMembersList?.clear()
            //TasksDataManager?.tasksStatus?.clear()

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.GroupId,groupID.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))


            UrlBuilder?.setUrl(Module.users, RequestCode.getgroupmembers,requestParameters)

            Log.d("response","USERS  URL: "+ UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response","GROUP MEMBERS : "+response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    progressDialog?.dismiss()

                }
                else
                {
                    try {
                        val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                        for(i in 0..data.length() - 1)
                        {
                            var result = data.getJSONObject(i)

                            var userId = result.getInt(JsonKeys.variables.UserID)
                            var firstName = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                            var lastName = result.getString(JsonKeys.variables.KEY_LASTNAME)
                            var fullName = firstName +" "+ lastName


                            UserDataManager?.insertGroupMembers(userId,firstName,lastName,fullName)

                        }

                    } catch (e: Exception) {
                    } finally {
                    }
                    progressDialog?.dismiss()


                }

            },
                    Response.ErrorListener {
                        error ->

                        progressDialog?.dismiss()

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
            progressDialog?.dismiss()



        } finally {
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if (attachment.equals(false)) {

            getMenuInflater().inflate(R.menu.new_attachment_menu, menu)
        } else if (attachment.equals(true)) {
            getMenuInflater().inflate(R.menu.attachment_menu, menu)

        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        // val info = item?.getMenuInfo() as AdapterContextMenuInfo

        when (item?.itemId) {
            R.id.context_camera -> {
                openCamera()
                return true
            }

            R.id.context_gallery -> {
                openGallery()
                return true
            }

            R.id.context_remove_attachment -> {
                try {
                    imageview_attached_doc?.setImageDrawable(null)
                    imageview_attached_doc?.setImageBitmap(null)
                    base64String = ""

                } catch (e: Exception) {
                } finally {
                }
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }
    fun openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!marshMallowPermission.checkPermissionForCamera()) {
                marshMallowPermission.requestPermissionForCamera()

            } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage()
            } else {
                /* if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                     marshMallowPermission.requestPermissionForExternalStorage()
                 }*/
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val mediaStorageDir = File(
                        Environment.getExternalStorageDirectory().toString()
                                + File.separator
                                + "abc"
                                + File.separator
                                + "abc"
                )



                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs()
                }

                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(Date())
                try {
                    imageFile = File.createTempFile(
                            "IMG_" + timeStamp, /* prefix */
                            ".jpg", /* suffix */
                            mediaStorageDir      /* directory */
                    )



                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
                    startActivityForResult(takePictureIntent, RESULT_LOAD_CAMERA)
                } catch (e: IOException) {
                    Log.d("camera", e.message)
                    Log.d("camera", e.printStackTrace().toString())
                } finally {
                    Log.d("camera", "final")
                }
            }
        } else {
            val camera_intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            imageFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "test.jpg")
            imageFileuri = Uri.fromFile(imageFile)
            camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileuri)
            camera_intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            try {
                if (camera_intent.resolveActivity(getPackageManager()) != null) {

                    startActivityForResult(camera_intent, RESULT_LOAD_CAMERA)
                } else {
                    Log.d("Imagelog", "package manager null")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("Camera Activity", e.message)

            } finally {
                Log.d("Camera Activity", "final")

            }
        }
    }


    fun openGallery() {

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage()
        } else {
            val gallery_intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(gallery_intent, RESULT_LOAD_IMAGE)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0) {
            when (resultCode) {
                Activity.RESULT_OK -> if (imageFile?.exists() == true) {

                    if (bitmap_object != null) {
                        bitmap_object = null
                    } else if (bitmap != null) {
                        bitmap = null
                    }
                    // bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(imageFile?.getAbsolutePath()), 1200)

                    // bitmap_object = BitmapFactory.decodeFile(imageFile?.absolutePath)

                    bitmap_object = getRotatedBitmap(imageFile!!.absolutePath)

                    //bitmap_object = scaleBitmapDown(bitmap_object!!, 1200)


                    fileName = imageFile!!.name

                    // base64String = getBase64(bitmap_object!!)

                    var a = GetBase64String(scaleBitmapDown(bitmap_object!!, 1200)!!)
                    a.execute()


                    saveImage(bitmap_object!!)

                    imageview_attached_doc?.setImageBitmap(bitmap_object)


                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    /*Glide.with(this@GeneralPost)
                            .load(Uri.fromFile(imageFile))
                            .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                            .into(imageview_add_image)*/
                    //filename = "bitmap.png"


                    //imageview_add_image?.setImageBitmap(bitmap_object)
                    //picture_string = getBase64(bitmap_object!!)!!
                    // var stream = this.openFileOutput(filename, Context.MODE_PRIVATE)
                    // bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream)


                    //Cleanup
                    // stream.close()
                    // bitmap_object?.recycle();

                    // var load = SetImageFromBitmap(getRotatedBitmap(imageFile?.absolutePath!!), imageview_image!!)
                    // load.execute()
                    //Log.d("Imagelog", "base 64: "+getBase64(bitmap_object!!))
                    //Toast.makeText(this,"Picture was saved at "+imageFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
                    //Log.d("Imagelog", "Picture was saved at " + imageFile?.getAbsolutePath())
                } else {
                    Toast.makeText(this, "Error loading Picture", Toast.LENGTH_LONG).show()
                    Log.d("Imagelog", "Error loading Picture")
                }
                Activity.RESULT_CANCELED ->
                    //Toast.makeText(this,"Result canceled",Toast.LENGTH_LONG).show();
                    Log.d("Imagelog", "Result canceled")
            }
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor = contentResolver.query(selectedImage,
                            filePathColumn, null, null, null)
                    cursor!!.moveToFirst()


                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    var picturePath = cursor.getString(columnIndex)
                    cursor.close()


                    // imageByteArray = convertToByteArray(bitmap_object!!)
                    if (bitmap_object != null) {
                        bitmap_object = null

                    } else if (bitmap != null) {
                        bitmap = null
                    }


                    // bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(picturePath),1200)
                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    bitmap_object = BitmapFactory.decodeFile(picturePath)

                    bitmap_object = scaleBitmapDown(bitmap_object!!, 1200)


                    var a = GetBase64String(bitmap_object!!)
                    a.execute()


                    fileName = picturePath


                    imageview_attached_doc?.setImageBitmap(bitmap_object)

                    //filename = "bitmap.png"


                    //Write file
                    //var stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
                    //bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    //stream.close();

                    // var load = SetImageFromBitmap(getRotatedBitmap(picturePath), imageview_image!!)
                    //load.execute()
                    //bitmap_object?.recycle();


                    //picture_string = getBase64(compressBitmap(bitmap_object!!))


                    //Log.d("Imagelog", "base 64 image load: "+getBase64(bitmap_object!!))


                    // Log.d("Imagelog",imageview_add_image?.toString())

                    //imageview_add_image?.setImageBitmap(compressBitmap(bitmap_object!!))
                    //Toast.makeText(getApplicationContext(),picturePath,Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    fun saveImage(image: Bitmap): String {
        var savedImagePath: String? = null

        var imageFileName = "IMG" + getTimeStamp() + ".jpg"
        var storageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "/Bizggro")
        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            var imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.getAbsolutePath()
            try {
                var fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            // Add the image to the system gallery
            galleryAddPic(savedImagePath)
            toast("Image saved into folder: FileDownloadTask in Gallery")
        } else {
            toast("ERROR SAVING IMAGE")
        }
        return savedImagePath!!
    }

    fun galleryAddPic(imagePath: String) {
        var mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        var f = File(imagePath)
        var contentUri = Uri.fromFile(f)
        mediaScanIntent.setData(contentUri)
        sendBroadcast(mediaScanIntent)
    }

    private inner class GetBase64String(bitmap: Bitmap) : AsyncTask<Void, Void, Void>() {
        var bitmap: Bitmap? = null
        val outputStream = ByteArrayOutputStream()
        var base64: String = ""


        init {

            this.bitmap = bitmap
            Log.d("response", "in constructor ")


        }

        override fun onPreExecute() {

            progressDialog = ProgressDialog.show(this@NewTask, "Loading attachment..", resources.getString(R.string.pleaseWait), true)
            progressDialog?.setCancelable(false)

            Log.d("response", "pre execute ")

        }

        override fun doInBackground(vararg params: Void?): Void? {

            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
            Log.d("response", "do in background: " + base64.toString())
            Log.d("response", "do in background: ")


            return null
        }

        override fun onPostExecute(result: Void?) {

            base64String = base64

            attachment = true
            fileSize = bitmap?.byteCount?.toLong()!!
            Log.d("response", "post execute: ")

            Log.d("response", "post execute: " + base64String.toString())
            progressDialog?.dismiss()

        }

    }


    fun getTimeStamp(): String? {
        val tsLong = System.currentTimeMillis() / 1000
        val ts = tsLong.toString()

        return ts
    }

    fun getRotatedBitmap(imagePath: String): Bitmap {

        val f = File(imagePath)
        val exif = ExifInterface(f.getPath())
        val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        var angle = 0

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            angle = 90
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            angle = 180
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            angle = 270
        }

        val mat = Matrix()
        mat.postRotate(angle.toFloat())

        val bmp = BitmapFactory.decodeStream(FileInputStream(f), null, null)
        val correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, mat, true)

        return correctBmp

    }

    fun scaleBitmapDown(bitmap: Bitmap, maxDimension: Int): Bitmap {
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var resizedWidth = maxDimension
        var resizedHeight = maxDimension

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = (resizedHeight * originalWidth.toFloat() / originalHeight.toFloat()).toInt()
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension
            resizedHeight = (resizedWidth * originalHeight.toFloat() / originalWidth.toFloat()).toInt()
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = maxDimension
        }
        val a = "Width: $resizedWidth\nHeight: $resizedHeight"
        Log.d("imagedimen", a)
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false)

    }

    private fun setStartDate(){


        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_startDate?.text =  year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth
            startDate = year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth

        }, y, m, d)

        dpd.show()



    }

    private fun setStartTime() {


        val calendar = Calendar.getInstance()
        val h = calendar.get(Calendar.HOUR)
        val m = calendar.get(Calendar.MINUTE)





        val tmp = TimePickerDialog(this@NewTask,TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->

        input_startTime?.text = hourOfDay.toString() + " " + ":" + " " + minute.toString()

            startTime = hourOfDay.toString() + " " + ":" + " " + minute.toString()
        },h,m,true)

        tmp.show()



    }


    private fun setEndDate(){


        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_endDate?.text =  year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth

            endDate = year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth
        }, y, m, d)

        dpd.show()



    }

    private fun setEndTime() {


        val calendar = Calendar.getInstance()
        val h = calendar.get(Calendar.HOUR)
        val m = calendar.get(Calendar.MINUTE)





        val tmp = TimePickerDialog(this@NewTask,TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->

            input_endTime?.text = hourOfDay.toString() + " " + ":" + " " + minute.toString()

            endTime = hourOfDay.toString() + " " + ":" + " " + minute.toString()
        },h,m,true)

        tmp.show()



    }

    fun setCurrentDate()
    {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_startDate?.setText(formattedDate)
        input_endDate?.setText(formattedDate)

        startDate = formattedDate
        endDate = formattedDate

    }

    private fun setCurrentTime() {

        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("hh:mm")
        val formattedDate = df.format(c.time)

        input_startTime?.setText(formattedDate)
        input_endTime?.setText(formattedDate)



    }

    fun loadSpinnerValues()
    {
        projectNameArray?.add("Select Project")
        for (i in 0..UserDataManager?.allProjectsList!!.size - 1) {
            if (UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false) {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }

        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.spinner_item_style, projectNameArray)

        spinner_project?.adapter = spinnerProjectAdapter



        statusNameArray?.add("Select Status")
        for (i in 0..TasksDataManager?.tasksStatus!!.size - 1) {
            if (TasksDataManager?.tasksStatus?.get(i)?.Status?.equals(null) == false) {
                statusNameArray?.add(TasksDataManager?.tasksStatus?.get(i)?.Status.toString()!!)

            }

        }

        var spinnerStatusAdapter = ArrayAdapter(this, R.layout.spinner_item_style, statusNameArray)

        spinner_status?.adapter = spinnerStatusAdapter




        groupNameArray?.add("Select Group")
        for (i in 0..UserDataManager?.userGroupsList!!.size - 1) {
            if (UserDataManager?.userGroupsList?.get(i)?.groupName?.equals(null) == false) {
                groupNameArray?.add(UserDataManager?.userGroupsList?.get(i)?.groupName.toString()!!)

                Log.d("response","group names: "+UserDataManager?.userGroupsList?.get(i)?.groupName.toString())

            }

        }

        var spinnerGroupAdapter = ArrayAdapter(this, R.layout.spinner_item_style, groupNameArray)

        spinner_group?.adapter = spinnerGroupAdapter


        priorityNameArray?.add("Select Priority")
        for (i in 0..TasksDataManager?.tasksPriorities!!.size - 1) {
            if (TasksDataManager?.tasksPriorities?.get(i)?.priorityName?.equals(null) == false) {
                priorityNameArray?.add(TasksDataManager?.tasksPriorities?.get(i)?.priorityName.toString()!!)

                Log.d("response","priority names: "+TasksDataManager?.tasksPriorities?.get(i)?.priorityName.toString()!!)

            }

        }

        var spinnerPriorityAdapter = ArrayAdapter(this, R.layout.spinner_item_style, priorityNameArray)

        spinner_priority?.adapter = spinnerPriorityAdapter



        inchargeNameArray?.add("Select Incharge")
        for (i in 0..UserDataManager?.usersInfoList!!.size - 1) {
            if (UserDataManager?.usersInfoList?.get(i)?.fullName?.equals(null) == false) {
                inchargeNameArray?.add(UserDataManager?.usersInfoList?.get(i)?.fullName?.toString()!!)

                Log.d("response","priority names: "+UserDataManager?.usersInfoList?.get(i)?.fullName?.toString()!!)

            }

        }

        var spinnerInchargeAdapter = ArrayAdapter(this, R.layout.spinner_item_style, inchargeNameArray)

        spinner_incharge?.adapter = spinnerInchargeAdapter


    }


    fun addTask() {

        Log.d("response","in add transaction")

        progressDialog = ProgressDialog.show(this@NewTask, "Adding Task", resources.getString(R.string.pleaseWait), true)
        progressDialog?.setCancelable(false)
        //UrlBuilder?.setAddTransactionUrl(description,amount,date,categoryID,projectID,transactionTypeID,loginManager?.getCompanyID())

//        Log.d("response",UrlBuilder?.getAddTransactionUrl())

        var request = object : JsonObjectRequest(Method.POST, Constants.URL_ADDTASK, null, Response.Listener<JSONObject>
        { response ->

            Log.d("response", "response: " + response.toString())

            progressDialog?.dismiss()


            var description = response.getString(JsonKeys.variables.KEY_DESCRIPTION)


            if (description!!.equals("Success")) {

                alert("Task Added Succesfully").show()

            /*    Handler().postDelayed(object : Runnable {

                    // Using handler with postDelayed called runnable run method


                    override fun run() {

                        var intent = Intent(this@NewTask, AllTransactions::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                        finish()
                    }
                }, 2 * 1000) // wait for 5 seconds*/

            } else {
                alert("Error Adding Task")
            }


        },
                Response.ErrorListener { error ->

                    Log.d("response", "error: " + error.message.toString())

                    progressDialog?.dismiss()

                }
        ) {

            override fun getBody(): ByteArray {

                var jsonObject = JSONObject()


                var toArray = JSONArray()
                var ccArray = JSONArray()

               // Log.d("response","array elements into string: "+ UserDataManager?.arraylist_selected_userIDs_To?.joinToString(","))

                var toList: String = ""
                var ccList: String = ""


                for(i in 0..UserDataManager?.arraylist_selected_userIDs_To!!.size - 1)
                {


                    if(i.equals(0))
                    {
                        
                        toList = UserDataManager?.arraylist_selected_userIDs_To?.get(i).toString()

                    }
                    else {

                        toList = toList + ","+UserDataManager?.arraylist_selected_userIDs_To?.get(i).toString()

                    }


                    }

                for(i in 0..UserDataManager?.arraylist_selected_userIDs_Cc!!.size - 1)

                {


                    if(i.equals(0))
                    {
                        ccList = UserDataManager?.arraylist_selected_userIDs_Cc?.get(i).toString()
                    }
                    else {

                        ccList = ccList + ","+UserDataManager?.arraylist_selected_userIDs_Cc?.get(i).toString()

                    }


                }


                Log.d("response","array elements into string: "+ toList)
                Log.d("response","array elements into string: "+ ccList)


            /*    for (i in 0..UserDataManager?.arraylist_selected_userIDs_To!!.size - 1) {
                    toArray.put(UserDataManager?.arraylist_selected_userIDs_To?.get(i))
                }

                for (i in 0..UserDataManager?.arraylist_selected_userIDs_Cc!!.size - 1) {
                    ccArray.put(UserDataManager?.arraylist_selected_userIDs_Cc?.get(i))
                }*/

                jsonObject.put(Parameters.Description, details)
                jsonObject.put(Parameters.UserId, loginManager?.getUserID())
                jsonObject.put(Parameters.CompanyId, loginManager?.getCompanyID())
                jsonObject.put(Parameters.ModuleId, 2)
                jsonObject.put(Parameters.ReferenceId, projectID)
                jsonObject.put(Parameters.Title, subject)

                jsonObject.put(Parameters.ProjectId, projectID)
                jsonObject.put(Parameters.StatusId, statusID)

                jsonObject.put(Parameters.PriorityId, priorityID)

                jsonObject.put(Parameters.StartDate,startDate)
                jsonObject.put(Parameters.EndDate, endDate)

                jsonObject.put(Parameters.TOList,toList)


                jsonObject.put(Parameters.CCList,ccList)

                jsonObject.put(Parameters.Attachment, fileName)
                jsonObject.put(Parameters.FileSize, fileSize)
                jsonObject.put(Parameters.AttachmentString, base64String)


                jsonObject.put(Parameters.TaskInchargeId, inchargeID)


                Log.d("response", jsonObject.toString())


                return jsonObject.toString().toByteArray()
            }
        }
        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController?.instance?.addToRequestQueue(request)
    }



}
