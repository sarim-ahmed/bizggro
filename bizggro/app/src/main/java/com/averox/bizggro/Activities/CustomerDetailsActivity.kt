package com.averox.bizggro.Activities

import android.content.Context
import android.content.Intent
import android.media.Image
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.*
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.CustomersDataManager
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.customer_contact_info_item.*
import kotlinx.android.synthetic.main.customer_info_item.*
import kotlinx.android.synthetic.main.customer_projects_item.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.*

class CustomerDetailsActivity : AppCompatActivity(), View.OnClickListener {


    private var toolbar: Toolbar? = null
    private var data_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var customerID: Any? = null
    private var textview_heading: TextView? = null
    private var button_contact_info: ImageButton? = null
    private var button_customer_info: ImageButton? = null
    private var button_address_info:  ImageButton? = null
    private var button_billing_info: ImageButton? = null
    private var button_shipping_info: ImageButton? = null
    private var button_social_info: ImageButton? = null
    private var button_productofinterest_info: ImageButton? = null
    private var button_task_info: ImageButton? = null
    private var button_event_info: ImageButton? = null
    private var button_projects_info: ImageButton? = null
    private var button_campaigns_info: ImageButton? = null
    private var button_transactions_info: ImageButton? = null
    private var button_purchaseOrders_info: ImageButton? = null
    private var button_newsletters_info: ImageButton? = null
    private var radio: RadioGroup? = null
    private var cust: RadioButton? = null
    private var cont: RadioButton? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_details)


        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        data_view = findViewById(R.id.data_view)

        loginManager = LoginManager(Constants.context!!)

        button_contact_info = findViewById(R.id.button_contact_info)
        button_contact_info?.setOnClickListener(this)
        button_customer_info = findViewById(R.id.button_customer_info)
        button_customer_info?.setOnClickListener(this)
        button_address_info = findViewById(R.id.button_address_info)
        button_address_info?.setOnClickListener(this)
        button_billing_info = findViewById(R.id.button_billing_info)
        button_billing_info?.setOnClickListener(this)
        button_shipping_info = findViewById(R.id.button_shipping_info)
        button_shipping_info?.setOnClickListener(this)
        button_social_info = findViewById(R.id.button_social_info)
        button_social_info?.setOnClickListener(this)
        button_productofinterest_info = findViewById(R.id.button_product_of_interest_info)
        button_productofinterest_info?.setOnClickListener(this)
        button_task_info = findViewById(R.id.button_tasks_info)
        button_task_info?.setOnClickListener(this)
        button_event_info = findViewById(R.id.button_events_info)
        button_event_info?.setOnClickListener(this)
        button_projects_info = findViewById(R.id.button_projects_info)
        button_projects_info?.setOnClickListener(this)
        button_campaigns_info = findViewById(R.id.button_campaigns_info)
        button_campaigns_info?.setOnClickListener(this)
        button_transactions_info = findViewById(R.id.button_transactions_info)
        button_transactions_info?.setOnClickListener(this)
        button_purchaseOrders_info = findViewById(R.id.button_purchaseOrder_info)
        button_purchaseOrders_info?.setOnClickListener(this)
        button_newsletters_info = findViewById(R.id.button_newsletters_info)
        button_newsletters_info?.setOnClickListener(this)





        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE

        var intent = intent
        var extras = intent.extras
        customerID = extras?.getInt("CUSTOMER_ID")

        textview_heading = findViewById(R.id.textview_heading)


        getCustomerDetails()

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

        try {
            CustomersDataManager?.basicInfoList?.clear()
            CustomersDataManager?.contractsList?.clear()
            CustomersDataManager?.eventsList?.clear()
            CustomersDataManager?.projectsList?.clear()
            CustomersDataManager?.newslettersList?.clear()
            CustomersDataManager?.posList?.clear()
            CustomersDataManager?.transactionsList?.clear()
            CustomersDataManager?.campaingsList?.clear()
            CustomersDataManager?.socialInfoList?.clear()
            CustomersDataManager?.productsList?.clear()
            CustomersDataManager?.tasksList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }


    override fun onClick(v: View?) {
when(v?.id) {
    R.id.button_contact_info -> {

        loadContactInfo()

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)


    }
    R.id.button_customer_info ->
    {
loadCustomerInfo()
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_address_info-> {

        loadAddressInfo()
        button_address_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_billing_info-> {
        loadBillingInfo()
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }

    R.id.button_shipping_info-> {
        loadShippingInfo()
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_social_info-> {
        loadSocialInfo()
        button_social_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)


        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)


    }

    R.id.button_product_of_interest_info-> {
        loadProductsofInterestInfo()
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }

    R.id.button_tasks_info-> {
        loadTasksInfo()
        button_task_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }

    R.id.button_events_info-> {
        loadEventsInfo()
        button_event_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_projects_info-> {
        loadProjectsInfo()
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_campaigns_info-> {
        loadCampaignsInfo()
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }

    R.id.button_transactions_info-> {
        loadTransactionsInfo()
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_purchaseOrder_info-> {
        loadPurchaseOrdersInfo()
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }
    R.id.button_newsletters_info-> {
        loadNewsLettersInfo()
        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

        button_contact_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_contact_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_social_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_social_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)

    }


}

}




    fun loadContactInfo()
    {

        textview_heading?.setText(resources.getString(R.string.contact_information))
        var adapter = ContactInfoAdapter(this, CustomersDataManager?.basicInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE
    }
    fun loadCustomerInfo()
    {
        textview_heading?.setText(resources.getString(R.string.customer_information))
        var adapter = CustomerInfoAdapter(this,CustomersDataManager?.basicInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE
    }
    fun loadAddressInfo()
    {
        textview_heading?.setText(resources.getString(R.string.address_information))

        var adapter = AddressAdapter(this,CustomersDataManager?.basicInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadBillingInfo()
    {
        textview_heading?.setText(resources.getString(R.string.billing_address))

        var adapter = BillingAdapter(this,CustomersDataManager?.basicInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadShippingInfo()
    {
        textview_heading?.setText(resources.getString(R.string.shipping_address))

        var adapter = ShippingAdapter(this,CustomersDataManager?.basicInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE


    }
    fun loadSocialInfo()
    {
        textview_heading?.setText(resources.getString(R.string.social_profiles))
        var adapter = SocialsAdapter(this,CustomersDataManager?.socialInfoList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadProductsofInterestInfo()
    {
        textview_heading?.setText(resources.getString(R.string.products_of_interest))

        var adapter = ProductsAdapter(this,CustomersDataManager?.productsList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE


    }
    fun loadTasksInfo()
    {
        textview_heading?.setText(resources.getString(R.string.tasks))

        var adapter = TasksAdapter(this,CustomersDataManager?.tasksList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadEventsInfo()
    {
        textview_heading?.setText(resources.getString(R.string.events))

        var adapter = EventsAdapter(this,CustomersDataManager?.eventsList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadProjectsInfo()
    {
        textview_heading?.setText(resources.getString(R.string.projects))

        var adapter = ProjectsAdapter(this,CustomersDataManager?.projectsList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadCampaignsInfo()
    {
        textview_heading?.setText(resources.getString(R.string.campaigns))

        var adapter = CampaignsAdapter(this,CustomersDataManager?.campaingsList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadTransactionsInfo()
    {
        textview_heading?.setText(resources.getString(R.string.transactions))

        var adapter = TransactionsAdapter(this,CustomersDataManager?.transactionsList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadPurchaseOrdersInfo()
    {
        textview_heading?.setText(resources.getString(R.string.purchase_order))

        var adapter = POsAdapter(this,CustomersDataManager?.posList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE

    }
    fun loadNewsLettersInfo()
    {
        textview_heading?.setText(resources.getString(R.string.newsletter))

        var adapter = NewsLettersAdapter(this,CustomersDataManager?.newslettersList!!)
        data_view?.layoutManager = LinearLayoutManager(this)
        data_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.GONE


    }

    fun getCustomerDetails()
    {



        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        /*UserDataManager.allTransactionsList?.clear()
        var adapter = CustomAdapter(this,UserDataManager.allTransactionsList!!)
        transactions_view?.layoutManager = LinearLayoutManager(this)
        transactions_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE*/

        /*  UserDataManager.customersListList?.clear()
          var adapter = CustomAdapter(this,UserDataManager.customersListList!!)
          transactions_view?.layoutManager = LinearLayoutManager(this)
          transactions_view?.adapter = adapter
          adapter.notifyDataSetChanged()
          loader?.visibility = View.VISIBLE
  */

/*        UserDataManager.customersList?.clear()
        var adapter = CustomAdapter(this, UserDataManager.customersList!!)
        customers_view?.layoutManager = LinearLayoutManager(this)
        customers_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE*/



        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,""+loginManager?.getCompanyID()))
        requestParameters.add(BasicNameValuePair(Parameters.CustomerId,""+customerID))


        UrlBuilder?.setUrl(Module.customers, RequestCode.getcustomer,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->


            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))

            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))
            }
            else
            {

                Log.d("response","in else ")

                val data = response.optJSONObject(JsonKeys.arrays.KEY_DATA)

                try {
                var basicData = data.optJSONArray(JsonKeys.arrays.basic)

                    Log.d("response","basic Data: "+basicData?.toString())

                    var socialInfo = data.optJSONArray(JsonKeys.arrays.socials)
                var products = data.optJSONArray(JsonKeys.arrays.products)
                var campaigns = data.optJSONArray(JsonKeys.arrays.campaigns)
                var transactions = data.optJSONArray(JsonKeys.arrays.transactionsandinvoices)
                var pos = data.optJSONArray(JsonKeys.arrays.pos)
                var newsletters = data.optJSONArray(JsonKeys.arrays.newsletters)
                var projects = data.optJSONArray(JsonKeys.arrays.projects)
                var contracts = data.optJSONArray(JsonKeys.arrays.contracts)
                var tasks = data.optJSONArray(JsonKeys.arrays.tasks)
                var events = data.optJSONArray(JsonKeys.arrays.events)




                    for(i in 0..basicData.length() - 1)
                    {
                        var result = basicData.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationdate = result.getString(JsonKeys.variables.CREATIONDATE)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var state = result.getString(JsonKeys.variables.KEY_STATE)
                        var countryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var details = result.getString(JsonKeys.variables.DETAILS)
                        var companyId = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var dateupdated = result.getString(JsonKeys.variables.DATEUPDATED)
                        var createdby = result.getString(JsonKeys.variables.CREATEDBY)
                        var updatedby = result.getString(JsonKeys.variables.UPDATEDBY)
                        var contacttypeid = result.getInt(JsonKeys.variables.CONTACTTYPEID)
                        var companyname = result.getString(JsonKeys.variables.KEY_COMPANYNAME)
                        var categoryid = result.getInt(JsonKeys.variables.CategoryId)
                        var zipcode = result.getString(JsonKeys.variables.KEY_ZIPCODE)
                        var ishidden = result.getBoolean(JsonKeys.variables.ISHIDDEN)
                        var customerid = result.getInt(JsonKeys.variables.CUSTOMERID)
                        var covertedbyid = result.getInt(JsonKeys.variables.CONVERTEDBY)
                        var dateconverted = result.getString(JsonKeys.variables.DATECONVERTED)
                        var typeid = result.getInt(JsonKeys.variables.TYPEID)
                        var creditwothinessid = result.getInt(JsonKeys.variables.CREDITWORTHINESSID)
                        var creditWorthness = result.optString(JsonKeys.variables.CreditWorthinessType)
                        var contactpersonid = result.getInt(JsonKeys.variables.CONTACTPERSONID)
                        var contactPersonName = result.optString(JsonKeys.variables.ContactPersonUserName)
                        var paymenttermsid = result.getInt(JsonKeys.variables.PAYMENTTERMSID)
                        var paymentTerm = result.optString(JsonKeys.variables.Term)
                        var billingstreet = result.getString(JsonKeys.variables.BILLINGSTREET)
                        var billingcity = result.getString(JsonKeys.variables.BILLINGCITY)
                        var billingstate = result.getString(JsonKeys.variables.BILLINGSTATE)
                        var billingzipcode = result.getString(JsonKeys.variables.BILLINGZIPCODE)
                        var billingcountryid = result.getInt(JsonKeys.variables.BILLINGCOUNTRYID)
                        var shippingstreet = result.getString(JsonKeys.variables.SHIPPINGSTREET)
                        var shippingcity = result.getString(JsonKeys.variables.SHIPPINGCITY)
                        var shippingstate = result.getString(JsonKeys.variables.SHIPPINGSTATE)
                        var shippingzipcode = result.getString(JsonKeys.variables.SHIPPINGZIPCODE)
                        var shippingcountryid = result.getInt(JsonKeys.variables.SHIPPINGCOUNTRYID)
                        var type = result.getString(JsonKeys.variables.TYPE)
                        var convertedbyuser = result.getString(JsonKeys.variables.CONVERTEDBYUSER)
                        var contactCategory = result.getString(JsonKeys.variables.ContactCategory)
                        var countryName = result.getString(JsonKeys.variables.CountryName)
                        var billingCountry = result.getString(JsonKeys.variables.BillingCountry)
                        var shippingCountry = result.getString(JsonKeys.variables.ShippingCountry)


                        CustomersDataManager?.insertBasicInfo(contactId, firstname,lastname, email,phone,mobile, address, creationdate,  city, state, countryID,countryName, details,companyId,
                                dateupdated, createdby, updatedby, contacttypeid,companyname, categoryid,contactCategory, zipcode, ishidden
                                , customerid, covertedbyid,dateconverted,typeid,creditwothinessid,creditWorthness,contactpersonid
                                ,contactPersonName,paymenttermsid,paymentTerm,  billingstreet, billingcity, billingstate, billingzipcode, billingcountryid
                                ,shippingstreet, shippingcity, shippingstate, shippingzipcode,
                                shippingcountryid, type,convertedbyuser,billingCountry,shippingCountry)


                        textview_heading?.setText(resources.getString(R.string.contact_information))

                        var adapter = ContactInfoAdapter(this, CustomersDataManager?.basicInfoList!!)
                        data_view?.layoutManager = LinearLayoutManager(this)
                        data_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                        button_contact_info?.setBackgroundColor(resources.getColor(R.color.buttonPressed))
                        button_contact_info?.imageTintList = resources.getColorStateList(R.color.smokeWhite)

                        button_customer_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_customer_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_address_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_address_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_billing_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_billing_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_shipping_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_shipping_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_productofinterest_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_productofinterest_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_task_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_task_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_event_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_event_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_projects_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_projects_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_campaigns_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_campaigns_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_transactions_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_transactions_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_purchaseOrders_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_purchaseOrders_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)
                        button_newsletters_info?.setBackgroundColor(resources.getColor(R.color.transparent))
                        button_newsletters_info?.imageTintList = resources.getColorStateList(R.color.customerButtonColor)



                    }
                    for(i in 0..socialInfo.length() - 1)
                    {
                        var result = socialInfo.getJSONObject(i)

                        var id = result.getInt(JsonKeys.variables.KEY_ID)
                        var facebook = result.getString(JsonKeys.variables.Facebook)
                        var twitter = result.getString(JsonKeys.variables.Twitter)
                        var googlePlus = result.getString(JsonKeys.variables.GooglePlus)
                        var instagram = result.getString(JsonKeys.variables.Instagram)
                        var linkedin = result.getString(JsonKeys.variables.LinkedIn)
                        var pinterest = result.getString(JsonKeys.variables.Pinterest)
                        var skype = result.getString(JsonKeys.variables.Skype)
                        var whatsapp = result.getString(JsonKeys.variables.WhatsApp)

                        CustomersDataManager?.insertSocialInfo(id,facebook,twitter,googlePlus,instagram, linkedin,pinterest,skype,whatsapp)
                    }
                    for(i in 0..products.length() - 1)
                    {
                        var result = products.getJSONObject(i)

                        var id = result.getInt(JsonKeys.variables.KEY_ID)
                        var productId = result.getInt(JsonKeys.variables.ProductId)
                        var customerId = result.getInt(JsonKeys.variables.CUSTOMERID)
                        var name = result?.getString(JsonKeys.variables.ProductName)
                        var desc = result?.getString(JsonKeys.variables.ProductsDesc)
                        var unitPrice = result?.getInt(JsonKeys.variables.UnitPrice)

                        CustomersDataManager?.insertProducts(id,productId,customerId,
                                name,desc,unitPrice)

                    }
                    for(i in 0..campaigns.length() - 1)
                    {
                        var result = campaigns.getJSONObject(i)

                        var campaignId = result.getInt(JsonKeys.variables.CampaignID)
                        var campaignName = result.getString(JsonKeys.variables.CampaignName)
                        var sourceCode = result.getString(JsonKeys.variables.SourceCode)
                        var campaignStatus =  result.getString(JsonKeys.variables.CampaignStatus)
                        var campaignType = result.getString(JsonKeys.variables.CampaignType)
                        var startdate = result.getString(JsonKeys.variables.StartDate)
                        var revenue = result.getInt(JsonKeys.variables.RevenueTarget)

                        CustomersDataManager?.insertCampaigns(campaignId,sourceCode,campaignName,
                                campaignStatus,campaignType,startdate,revenue)

                    }
                    for(i in 0..transactions.length() - 1)
                    {
                        var result = transactions.getJSONObject(i)

                        var description = result.getString(JsonKeys.variables.KEY_DESCRIPTION)
                        var amount = result.getInt(JsonKeys.variables.KEY_AMOUNT)
                        var date = result.getString(JsonKeys.variables.KEY_DATE)
                        var invoiceNumber = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                        var category = result.getString(JsonKeys.variables.KEY_CATEGORYNAME)
                        var transactionType = result.getString(JsonKeys.variables.TransactionTypeName)

                        CustomersDataManager?.insertTransactions(description,amount,invoiceNumber,transactionType,category,date)


                    }
                    for(i in 0..pos.length() - 1)
                    {
                        var result = pos.getJSONObject(i)

                        var poId = result.getInt(JsonKeys.variables.POId)
                        var projectId = result.getInt(JsonKeys.variables.ProjectId)
                        var invoiceNumber = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                        var poNumber = result.getString(JsonKeys.variables.KEY_PONUMBER)
                        var duedate = result.getString(JsonKeys.variables.KEY_DUEDATE)
                        var discountAmount = result.getInt(JsonKeys.variables.DiscountAmount)
                        var projectName =  result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                        var transactionStatus = result.getString(JsonKeys.variables.KEY_TRANSACTIONSTATUS)
                        var notes = result.getString(JsonKeys.variables.Notes)


                        CustomersDataManager?.insertPos(poId,projectId,invoiceNumber,duedate,poNumber,discountAmount,
                                projectName,transactionStatus,notes)

                    }
                    for(i in 0..newsletters.length() - 1)
                    {
                        var result = newsletters.getJSONObject(i)

                        var newsLetterId =  result.getInt(JsonKeys.variables.NewsLetterID)
                        var newsLetterName = result.getString(JsonKeys.variables.NewsLetterName)
                        var decription = result.getString(JsonKeys.variables.KEY_DESCRIPTION)
                        var status = result.getString(JsonKeys.variables.Status)
                        var type = result.getString(JsonKeys.variables.TYPE)
                        var sendDate = result.getString(JsonKeys.variables.SendDate)
                        var sentBy = result.getString(JsonKeys.variables.SentByUsername)

                        CustomersDataManager?.insertNewsLetters(newsLetterId,newsLetterName,decription,
                                status,type,sendDate,sentBy)

                    }
                    for(i in 0..projects.length() - 1)
                    {
                        var result = projects.getJSONObject(i)

                        var projectId =  result.getInt(JsonKeys.variables.KEY_PROJECTID)
                        var projectName = result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                        var contactPerson = result.getString(JsonKeys.variables.ContactPersonName)
                        var status = result.getString(JsonKeys.variables.Status)
                        var dateStarted = result.getString(JsonKeys.variables.StartDate)
                        var endDate = result.getString(JsonKeys.variables.KEY_ENDDATE)

                        CustomersDataManager?.insertProjects(projectId,projectName,contactPerson,status,
                                dateStarted,endDate)

                    }

                    for(i in 0..tasks.length() - 1)
                    {
                        var result = tasks.getJSONObject(i)

                        var title = result.getString(JsonKeys.variables.Title)
                        var startDate = result.getString(JsonKeys.variables.StartDate)
                        var endDate = result.getString(JsonKeys.variables.KEY_ENDDATE)
                        var status = result.getString(JsonKeys.variables.Status)
                        var username = result.getString(JsonKeys.variables.UserName)

                        CustomersDataManager?.insertTasks(title,status,startDate,endDate,username)

                    }
                    for(i in 0..events.length() - 1)
                    {
                        var result = events.getJSONObject(i)

                        var eventId = result.getInt(JsonKeys.variables.EventID)
                        var name = result.getString(JsonKeys.variables.EventName)
                        var conductBy = result.getString(JsonKeys.variables.ConductedByUser)
                        var startDate = result.getString(JsonKeys.variables.StartDate)
                        var endDate = result.getString(JsonKeys.variables.KEY_ENDDATE)
                        var contactPerson = result.getString(JsonKeys.variables.ContactPerson)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_PHONE)
                        var status = result.getString(JsonKeys.variables.Status)
                        var guest = result.getString(JsonKeys.variables.InvitedGuest)

                        CustomersDataManager?.insertEvents(eventId,name,startDate,endDate,status,guest,contactPerson
                        ,email,phone,mobile,conductBy)

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: "+e.message.toString())
                    Log.d("response","stack trace: "+e.stackTrace.toString())

                } finally {
                }


            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE

                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    internal inner class ContactInfoAdapter(private val context: Context, arraylist: ArrayList<CustomerBasicDataAdapter>) : RecyclerView.Adapter<ContactInfoAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerBasicDataAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_contact_info_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]




            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/


            holder.firstName_view?.setText(""+list_items.firstname)
            holder.lastName_view?.setText(""+list_items.lastname)
            holder.email_view?.setText(""+list_items.email)
            holder.mobile_view?.setText(""+list_items.mobile)
            holder.phone_view?.setText(""+list_items.phone)
            holder.company_view?.setText(""+list_items.companyname)
            holder.category_view?.setText(""+list_items.contactCategory)
            holder.details_view?.setText(""+list_items.details)

            /*  holder.sr_view?.setText(""+sr_number)
              holder.date_view?.setText(""+list_items.getDate())
              holder.description_view?.setText(""+list_items.getDescription())
              holder.category_view?.setText(""+list_items.getCategory())
              holder.amount_view?.setText(""+list_items.getAmount())
  */
            Log.d("holder",""+list_items.firstname)
            Log.d("holder",""+list_items.mobile)

            Log.d("holder",""+list_items.companyname)

            Log.d("holder",""+list_items.convertedbyuser)
        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var firstName_view: TextView? = null
            var lastName_view: TextView? = null
            var mobile_view: TextView? = null
            var email_view: TextView? = null
            var phone_view: TextView? = null
            var company_view: TextView? = null
            var category_view: TextView? = null
            var details_view: TextView? = null

            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                firstName_view = itemView.findViewById(R.id.firstname_view)
                lastName_view =  itemView.findViewById(R.id.lastname_view)
                mobile_view = itemView.findViewById(R.id.mobile_view)
                phone_view = itemView.findViewById(R.id.phone_view)
                email_view = itemView.findViewById(R.id.email_view)
                company_view = itemView.findViewById(R.id.company_view)
                category_view = itemView.findViewById(R.id.category_view)
                details_view = itemView.findViewById(R.id.details_view)
            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

            /*    var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                startActivity(intent)*/

            }
        }

        fun removeAt(position: Int) {
            CustomersDataManager?.basicInfoList!!.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, CustomersDataManager?.basicInfoList!!.size)
        }

    }
    internal inner class CustomerInfoAdapter(private val context: Context, arraylist: ArrayList<CustomerBasicDataAdapter>) : RecyclerView.Adapter<CustomerInfoAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerBasicDataAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_info_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]


            holder.contact_person?.setText(""+list_items.contactPersonUsername)
            holder.type?.setText(""+list_items.type)
            holder.credit_worthness?.setText(""+list_items.creditWorthness)
            holder.payment_terms?.setText(""+list_items.paymentTerms)
            holder.date_converted?.setText(""+list_items.dateconverted)
            holder.convertedBy?.setText(""+list_items.convertedbyuser)

            holder.details?.setText(""+list_items.details)

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var contact_person: TextView? = null
            var type: TextView? = null
            var credit_worthness: TextView? = null
            var payment_terms: TextView? = null
            var date_converted: TextView? = null
            var convertedBy: TextView? = null
            var details: TextView? = null

            init {

                itemView.setOnClickListener(this)
                contact_person = itemView.findViewById(R.id.contact_person_view)
                type = itemView.findViewById(R.id.type_view)

                credit_worthness = itemView.findViewById(R.id.credit_worthness_view)
                payment_terms = itemView.findViewById(R.id.payment_terms_view)
                date_converted =  itemView.findViewById(R.id.date_converted_view)
                convertedBy = itemView.findViewById(R.id.convertedby_view)
                details = itemView.findViewById(R.id.details_view)


            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)

               /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                        UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                        UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                        UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                toast(""+itemPosition)
*/

               /* var intent = Intent(thisthis@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                startActivity(intent)*/

            }
        }

        fun removeAt(position: Int) {
           /* arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)*/
        }

    }
    internal inner class SocialsAdapter(private val context: Context, arraylist: ArrayList<CustomerSocialProfilesAdapter>) : RecyclerView.Adapter<SocialsAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerSocialProfilesAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_social_profiles_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            var sr_number = position+1

            holder.facebook?.setText(""+list_items.facebook)
            holder.twitter?.setText(""+list_items.twitter)
            holder.googleplus?.setText(""+list_items.googlePlus)
            holder.linkedin?.setText(""+list_items.linkedin)
            holder.instagram?.setText(""+list_items.instagram)
            holder.pinterest?.setText(""+list_items.pinterest)
            holder.skype?.setText(""+list_items.skype)
            holder.whatsapp?.setText(""+list_items.whatsapp)


        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var facebook: TextView? = null
            var twitter: TextView? = null
            var googleplus: TextView? = null
            var linkedin: TextView? = null
            var instagram: TextView? = null
            var pinterest: TextView? = null
            var skype: TextView? = null
            var whatsapp: TextView? = null

            init {

                itemView.setOnClickListener(this)
                facebook = itemView.findViewById(R.id.facebook_view)
                twitter = itemView.findViewById(R.id.twitter_view)

                googleplus = itemView.findViewById(R.id.googleplus_view)
                linkedin = itemView.findViewById(R.id.linkedin_view)
                instagram =  itemView.findViewById(R.id.instagram_view)
                pinterest= itemView.findViewById(R.id.pinterest_view)
                skype = itemView.findViewById(R.id.skype_view)
                whatsapp = itemView.findViewById(R.id.whatsapp_view)

            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)
/*
 alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/


                /*var intent = Intent(thisthis@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                startActivity(intent)
*/
            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }
    internal inner class ProductsAdapter(private val context: Context, arraylist: ArrayList<CustomerProductsAdapter>) : RecyclerView.Adapter<ProductsAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerProductsAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_products_of_interest_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]


            var sr_number = position+1

            holder.sr_view?.setText(""+sr_number)
            holder.name_view?.setText(""+list_items.productName)
            holder.description_view?.setText(""+list_items.productDesc)
            holder.unitprice_view?.setText(""+list_items.unitPrice)




        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var name_view: TextView? = null
            var description_view: TextView? = null
            var unitprice_view: TextView? = null


            init {

                itemView.setOnClickListener(this)


                sr_view = itemView.findViewById(R.id.sr_view)
                name_view = itemView.findViewById(R.id.name_view)
                description_view =  itemView.findViewById(R.id.description_view)
                unitprice_view = itemView.findViewById(R.id.unitprice_view)


            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)

/* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)


                var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                startActivity(intent)*/

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }
     internal inner class CampaignsAdapter(private val context: Context, arraylist: ArrayList<CustomerCampaignsAdapter>) : RecyclerView.Adapter<CampaignsAdapter.MyViewHolder>() {

         //Creating an arraylist of POJO objects
         //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
         private val inflater: LayoutInflater
         internal var view: View? = null
         internal var holder: MyViewHolder? = null
         var arraylist: ArrayList<CustomerCampaignsAdapter> = ArrayList()

         init {

             inflater = LayoutInflater.from(context)

             this.arraylist = arraylist

         }

         //This method inflates view present in the RecyclerView
         override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
             view = inflater.inflate(R.layout.customers_list_item, parent, false)
             holder = MyViewHolder(view!!)
             return holder!!
         }

         //Binding the data using get() method of POJO object
         override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
             val list_items = arraylist[position]


             var sr_number = position+1

             holder.sr_view?.setText(""+sr_number)
             holder.name_view?.setText(""+list_items.campaignName)
             holder.code_view?.setText(""+list_items.sourceCode)
             holder.status_view?.setText(""+list_items.campaignStatus)
             holder.type_view?.setText(""+list_items.campaignType)
             holder.startdate_view?.setText(""+list_items.startDate)
             holder.revenue_view?.setText(""+list_items.revenue)



         }

         //Setting the arraylist
         fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
             notifyItemRangeChanged(0, arraylist.size)

         }

         override fun getItemCount(): Int {

             Log.d("holder",""+arraylist.size)

             return arraylist.size


         }

         //View holder class, where all view components are defined
         internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

             var sr_view: TextView? = null
             var name_view: TextView? = null
             var code_view: TextView? = null
             var status_view: TextView? = null
             var type_view: TextView? = null
             var startdate_view: TextView? = null
             var revenue_view: TextView? = null

             init {

                 itemView.setOnClickListener(this)


                 sr_view = itemView.findViewById(R.id.sr_view)
                 name_view = itemView.findViewById(R.id.name_view)
                 code_view =  itemView.findViewById(R.id.code_view)
                 status_view = itemView.findViewById(R.id.status_view)
                 type_view = itemView.findViewById(R.id.type_view)
                 startdate_view = itemView.findViewById(R.id.startdate_view)
                 revenue_view = itemView.findViewById(R.id.revenue_view)

             }

             override fun onClick(v: View) {

                 val itemPosition = data_view?.getChildLayoutPosition(v)

/*  alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                  toast(""+itemPosition)


                 var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                 intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                 startActivity(intent)*/

             }
         }

         fun removeAt(position: Int) {
             arraylist.removeAt(position)
             notifyItemRemoved(position)
             notifyItemRangeChanged(0, arraylist.size)
         }

     }
      internal inner class TransactionsAdapter(private val context: Context, arraylist: ArrayList<CustomerTransactionandInvoicesAdpater>) : RecyclerView.Adapter<TransactionsAdapter.MyViewHolder>() {

          //Creating an arraylist of POJO objects
          //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
          private val inflater: LayoutInflater
          internal var view: View? = null
          internal var holder: MyViewHolder? = null
          var arraylist: ArrayList<CustomerTransactionandInvoicesAdpater> = ArrayList()

          init {

              inflater = LayoutInflater.from(context)

              this.arraylist = arraylist

          }

          //This method inflates view present in the RecyclerView
          override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
              view = inflater.inflate(R.layout.customer_transactions_item, parent, false)
              holder = MyViewHolder(view!!)
              return holder!!
          }

          //Binding the data using get() method of POJO object
          override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
              val list_items = arraylist[position]



              var sr_number = position+1

              holder.sr_view?.setText(""+sr_number)
              holder.desc_view?.setText(""+list_items.description)
              holder.invoiceNumber_view?.setText(""+list_items.invoiceNumber)
              holder.category_view?.setText(""+list_items.categoryName)
              holder.date_view?.setText(""+list_items.date)
              holder.amount_view?.setText(""+list_items.amount)
              holder.transactionType_view?.setText(""+list_items.transactionTypeName)




          }

          //Setting the arraylist
          fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
              notifyItemRangeChanged(0, arraylist.size)

          }

          override fun getItemCount(): Int {

              Log.d("holder",""+arraylist.size)

              return arraylist.size


          }

          //View holder class, where all view components are defined
          internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

              var sr_view: TextView? = null
              var desc_view: TextView? = null
              var invoiceNumber_view: TextView? = null
              var category_view: TextView? = null
              var date_view: TextView? = null
              var amount_view: TextView? = null
              var transactionType_view: TextView? = null

              init {

                  itemView.setOnClickListener(this)

                  sr_view = itemView.findViewById(R.id.sr_view)
                  desc_view = itemView.findViewById(R.id.description_view)
                  invoiceNumber_view =  itemView.findViewById(R.id.invoice_number_view)
                  category_view = itemView.findViewById(R.id.category_view)
                  date_view = itemView.findViewById(R.id.date_view)
                  amount_view = itemView.findViewById(R.id.amount_view)
                  transactionType_view = itemView.findViewById(R.id.transactionType_view)

              }

              override fun onClick(v: View) {

                 /* val itemPosition = customers_view?.getChildLayoutPosition(v)

   alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                   toast(""+itemPosition)


                  var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                  intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                  startActivity(intent)*/

              }
          }

          fun removeAt(position: Int) {
              arraylist.removeAt(position)
              notifyItemRemoved(position)
              notifyItemRangeChanged(0, arraylist.size)
          }

      }
      internal inner class POsAdapter(private val context: Context, arraylist: ArrayList<CustomerPOsAdapter>) : RecyclerView.Adapter<POsAdapter.MyViewHolder>() {

          //Creating an arraylist of POJO objects
          //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
          private val inflater: LayoutInflater
          internal var view: View? = null
          internal var holder: MyViewHolder? = null
          var arraylist: ArrayList<CustomerPOsAdapter> = ArrayList()

          init {

              inflater = LayoutInflater.from(context)

              this.arraylist = arraylist

          }

          //This method inflates view present in the RecyclerView
          override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
              view = inflater.inflate(R.layout.customer_purchaseorders_item, parent, false)
              holder = MyViewHolder(view!!)
              return holder!!
          }

          //Binding the data using get() method of POJO object
          override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
              val list_items = arraylist[position]



              var sr_number = position+1

              holder.sr_view?.setText(""+sr_number)
              holder.ponumber_view?.setText(""+list_items.poNumber)
              holder.project_view?.setText(""+list_items.projectName)
              holder.invoiceNumber_view?.setText(""+list_items.invoiceNumber)
              holder.note_view?.setText(""+list_items.notes)
              holder.duedate_view?.setText(""+list_items.dueDate)

          }

          //Setting the arraylist
          fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
              notifyItemRangeChanged(0, arraylist.size)

          }

          override fun getItemCount(): Int {

              Log.d("holder",""+arraylist.size)

              return arraylist.size


          }

          //View holder class, where all view components are defined
          internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

              var sr_view: TextView? = null
              var ponumber_view: TextView? = null
              var project_view: TextView? = null
              var invoiceNumber_view: TextView? = null
              var note_view: TextView? = null
              var duedate_view: TextView? = null

              init {

                  itemView.setOnClickListener(this)


                  sr_view = itemView.findViewById(R.id.sr_view)
                  ponumber_view = itemView.findViewById(R.id.name_view)
                  invoiceNumber_view =  itemView.findViewById(R.id.companyname_view)
                  project_view = itemView.findViewById(R.id.project_view)
                  note_view = itemView.findViewById(R.id.mobile_view)
                  duedate_view = itemView.findViewById(R.id.type_view)


              }

              override fun onClick(v: View) {

      /*            val itemPosition = customers_view?.getChildLayoutPosition(v)

   alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                   toast(""+itemPosition)


                  var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                  intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                  startActivity(intent)*/

              }
          }

          fun removeAt(position: Int) {
              arraylist.removeAt(position)
              notifyItemRemoved(position)
              notifyItemRangeChanged(0, arraylist.size)
          }

      }
      internal inner class NewsLettersAdapter(private val context: Context, arraylist: ArrayList<CustomerNewslettersAdapter>) : RecyclerView.Adapter<NewsLettersAdapter.MyViewHolder>() {

          //Creating an arraylist of POJO objects
          //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
          private val inflater: LayoutInflater
          internal var view: View? = null
          internal var holder: MyViewHolder? = null
          var arraylist: ArrayList<CustomerNewslettersAdapter> = ArrayList()

          init {

              inflater = LayoutInflater.from(context)

              this.arraylist = arraylist

          }

          //This method inflates view present in the RecyclerView
          override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
              view = inflater.inflate(R.layout.customer_newsletters_item, parent, false)
              holder = MyViewHolder(view!!)
              return holder!!
          }

          //Binding the data using get() method of POJO object
          override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
              val list_items = arraylist[position]



              var sr_number = position+1

              holder.sr_view?.setText(""+sr_number)
              holder.title_view?.setText(""+list_items.newLetterName)
              holder.status_view?.setText(""+list_items.status)
              holder.senddate_view?.setText(""+list_items.sendDate)
              holder.type_view?.setText(""+list_items.type)
              holder.sentBy_view?.setText(""+list_items.sentBy)



/*
              Log.d("holder",""+list_items.firstname)
              Log.d("holder",""+list_items.mobile)

              Log.d("holder",""+list_items.companyname)

              Log.d("holder",""+list_items.convertedbyuser)*/
          }

          //Setting the arraylist
          fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
              notifyItemRangeChanged(0, arraylist.size)

          }

          override fun getItemCount(): Int {

              Log.d("holder",""+arraylist.size)

              return arraylist.size


          }

          //View holder class, where all view components are defined
          internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

              var sr_view: TextView? = null
              var title_view: TextView? = null
              var type_view: TextView? = null
              var status_view: TextView? = null
              var senddate_view: TextView? = null
              var sentBy_view: TextView? = null

              init {

                  itemView.setOnClickListener(this)

                  sr_view = itemView.findViewById(R.id.sr_view)
                  title_view = itemView.findViewById(R.id.title_view)
                  status_view =  itemView.findViewById(R.id.status_view)
                  senddate_view = itemView.findViewById(R.id.senddate_view)
                  sentBy_view = itemView.findViewById(R.id.sentby_view)
                  type_view = itemView.findViewById(R.id.type_view)


              }

              override fun onClick(v: View) {

                  val itemPosition = data_view?.getChildLayoutPosition(v)

   /*alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                   toast(""+itemPosition)


                  var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                  intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                  startActivity(intent)*/

              }
          }

          fun removeAt(position: Int) {
              arraylist.removeAt(position)
              notifyItemRemoved(position)
              notifyItemRangeChanged(0, arraylist.size)
          }

      }
       internal inner class ProjectsAdapter(private val context: Context, arraylist: ArrayList<CustomerProjectsAdapter>) : RecyclerView.Adapter<ProjectsAdapter.MyViewHolder>() {

            //Creating an arraylist of POJO objects
            //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
            private val inflater: LayoutInflater
            internal var view: View? = null
            internal var holder: MyViewHolder? = null
            var arraylist: ArrayList<CustomerProjectsAdapter> = ArrayList()

            init {

                inflater = LayoutInflater.from(context)

                this.arraylist = arraylist

            }

            //This method inflates view present in the RecyclerView
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
                view = inflater.inflate(R.layout.customer_projects_item, parent, false)
                holder = MyViewHolder(view!!)
                return holder!!
            }

            //Binding the data using get() method of POJO object
            override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
                val list_items = arraylist[position]

                var sr_number = position+1

                holder.sr_view?.setText(""+sr_number)
                holder.name_view?.setText(""+list_items.projectName)
                holder.contactperson_view?.setText(""+list_items.contactPerson)
                holder.status_view?.setText(""+list_items.status)
                holder.datestarted_view?.setText(""+list_items.dateStarted)
                holder.enddate_view?.setText(""+list_items.endDate)





            }

            //Setting the arraylist
            fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
                notifyItemRangeChanged(0, arraylist.size)

            }

            override fun getItemCount(): Int {

                Log.d("holder",""+arraylist.size)

                return arraylist.size


            }

            //View holder class, where all view components are defined
            internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

                var sr_view: TextView? = null
                var name_view: TextView? = null
                var contactperson_view: TextView? = null
                var status_view: TextView? = null
                var datestarted_view: TextView? = null
                var enddate_view: TextView? = null

                init {

                    itemView.setOnClickListener(this)

                    sr_view = itemView.findViewById(R.id.sr_view)
                    name_view = itemView.findViewById(R.id.name_view)
                    contactperson_view =  itemView.findViewById(R.id.contact_person_view)
                    status_view = itemView.findViewById(R.id.status_view)
                    datestarted_view = itemView.findViewById(R.id.startdate_view)
                    enddate_view = itemView.findViewById(R.id.enddate_view)

                }

                override fun onClick(v: View) {

                    val itemPosition = data_view?.getChildLayoutPosition(v)

    /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                             UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                             UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                             UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                     toast(""+itemPosition)


                    var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                    intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                    startActivity(intent)*/

                }
            }

            fun removeAt(position: Int) {
                arraylist.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeChanged(0, arraylist.size)
            }

        }
        internal inner class ContractsAdapter(private val context: Context, arraylist: ArrayList<CustomerContractsAdapter>) : RecyclerView.Adapter<ContractsAdapter.MyViewHolder>() {

           //Creating an arraylist of POJO objects
           //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
           private val inflater: LayoutInflater
           internal var view: View? = null
           internal var holder: MyViewHolder? = null
           var arraylist: ArrayList<CustomerContractsAdapter> = ArrayList()

           init {

               inflater = LayoutInflater.from(context)

               this.arraylist = arraylist

           }

           //This method inflates view present in the RecyclerView
           override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
               view = inflater.inflate(R.layout.customer_contract_info_item, parent, false)
               holder = MyViewHolder(view!!)
               return holder!!
           }

           //Binding the data using get() method of POJO object
           override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
               val list_items = arraylist[position]


               var sr_number = position+1

               holder.sr_view?.setText(""+sr_number)
               holder.title_view?.setText(""+list_items.docTitle)
               holder.datesigned_view?.setText(""+list_items.dateSigned)
               holder.addedby_view?.setText(""+list_items.addedBy)



           }

           //Setting the arraylist
           fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
               notifyItemRangeChanged(0, arraylist.size)

           }

           override fun getItemCount(): Int {

               Log.d("holder",""+arraylist.size)

               return arraylist.size


           }

           //View holder class, where all view components are defined
           internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

               var sr_view: TextView? = null
               var title_view: TextView? = null
               var datesigned_view: TextView? = null
               var addedby_view: TextView? = null


               init {

                   itemView.setOnClickListener(this)

                   sr_view = itemView.findViewById(R.id.sr_view)
                   title_view = itemView.findViewById(R.id.title_view)
                   datesigned_view =  itemView.findViewById(R.id.datesigned_view)
                   addedby_view = itemView.findViewById(R.id.added_by_view)


               }

               override fun onClick(v: View) {

                   val itemPosition = data_view?.getChildLayoutPosition(v)
/*
    alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                            UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                            UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                            UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                    toast(""+itemPosition)


                   var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                   intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                   startActivity(intent)*/

               }
           }

           fun removeAt(position: Int) {
               arraylist.removeAt(position)
               notifyItemRemoved(position)
               notifyItemRangeChanged(0, arraylist.size)
           }

       }
     internal inner class TasksAdapter(private val context: Context, arraylist: ArrayList<CustomerTasksAdapter>) : RecyclerView.Adapter<TasksAdapter.MyViewHolder>() {

          //Creating an arraylist of POJO objects
          //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
          private val inflater: LayoutInflater
          internal var view: View? = null
          internal var holder: MyViewHolder? = null
          var arraylist: ArrayList<CustomerTasksAdapter> = ArrayList()

          init {

              inflater = LayoutInflater.from(context)

              this.arraylist = arraylist

          }

          //This method inflates view present in the RecyclerView
          override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
              view = inflater.inflate(R.layout.customer_task_item, parent, false)
              holder = MyViewHolder(view!!)
              return holder!!
          }

          //Binding the data using get() method of POJO object
          override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
              val list_items = arraylist[position]


              var sr_number = position+1

              holder.sr_view?.setText(""+sr_number)
              holder.title_view?.setText(""+list_items.title)
              holder.status_view?.setText(""+list_items.status)
              holder.startdate_view?.setText(""+list_items.startDate)
              holder.enddate_view?.setText(""+list_items.endDate)
              holder.assignedBy_view?.setText(""+list_items.username)


          }

          //Setting the arraylist
          fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
              notifyItemRangeChanged(0, arraylist.size)

          }

          override fun getItemCount(): Int {

              Log.d("holder",""+arraylist.size)

              return arraylist.size


          }

          //View holder class, where all view components are defined
          internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

              var sr_view: TextView? = null
              var title_view: TextView? = null
              var status_view: TextView? = null
              var startdate_view: TextView? = null
              var enddate_view: TextView? = null
              var assignedBy_view: TextView? = null

              init {

                  itemView.setOnClickListener(this)


                  sr_view = itemView.findViewById(R.id.sr_view)
                  title_view = itemView.findViewById(R.id.title_view)
                  status_view =  itemView.findViewById(R.id.status_view)
                  startdate_view = itemView.findViewById(R.id.startdate_view)
                  enddate_view = itemView.findViewById(R.id.enddate_view)
                  assignedBy_view = itemView.findViewById(R.id.assignedby_view)

              }

              override fun onClick(v: View) {

                  val itemPosition = data_view?.getChildLayoutPosition(v)

/*   alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                           UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                   toast(""+itemPosition)


                  var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                  intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                  startActivity(intent)*/

              }
          }

          fun removeAt(position: Int) {
              arraylist.removeAt(position)
              notifyItemRemoved(position)
              notifyItemRangeChanged(0, arraylist.size)
          }

      }
      internal inner class EventsAdapter(private val context: Context, arraylist: ArrayList<CustomerEventsAdapter>) : RecyclerView.Adapter<EventsAdapter.MyViewHolder>() {

         //Creating an arraylist of POJO objects
         //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
         private val inflater: LayoutInflater
         internal var view: View? = null
         internal var holder: MyViewHolder? = null
         var arraylist: ArrayList<CustomerEventsAdapter> = ArrayList()

         init {

             inflater = LayoutInflater.from(context)

             this.arraylist = arraylist

         }

         //This method inflates view present in the RecyclerView
         override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
             view = inflater.inflate(R.layout.customer_event_item, parent, false)
             holder = MyViewHolder(view!!)
             return holder!!
         }

         //Binding the data using get() method of POJO object
         override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
             val list_items = arraylist[position]



             var sr_number = position+1

             holder.sr_view?.setText(""+sr_number)
             holder.name_view?.setText(""+list_items.eventName)
             holder.email_view?.setText(""+list_items.email)
             holder.mobile_view?.setText(""+list_items.mobile)
             holder.status_view?.setText(""+list_items.status)
             holder.startdate_view?.setText(""+list_items.startDate)
             holder.enddate_view?.setText(""+list_items.endTime)
             holder.contactPerson_view?.setText(""+list_items.contactPerson)
             holder.phone_view?.setText(""+list_items.phone)



         }

         //Setting the arraylist
         fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
             notifyItemRangeChanged(0, arraylist.size)

         }

         override fun getItemCount(): Int {

             Log.d("holder",""+arraylist.size)

             return arraylist.size


         }

         //View holder class, where all view components are defined
         internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

             var sr_view: TextView? = null
             var name_view: TextView? = null
             var status_view: TextView? = null
             var startdate_view: TextView? = null
             var enddate_view: TextView? = null
             var contactPerson_view: TextView? = null
             var email_view: TextView? = null
             var phone_view: TextView? = null
             var mobile_view: TextView? = null

             init {

                 itemView.setOnClickListener(this)

                 sr_view = itemView.findViewById(R.id.sr_view)
                 name_view = itemView.findViewById(R.id.name_view)
                 status_view =  itemView.findViewById(R.id.status_view)
                 startdate_view = itemView.findViewById(R.id.startdate_view)
                 enddate_view = itemView.findViewById(R.id.enddate_view)
                 contactPerson_view = itemView.findViewById(R.id.contact_person_view)
                 email_view = itemView.findViewById(R.id.email_view)
                 phone_view = itemView.findViewById(R.id.phone_view)
                 mobile_view = itemView.findViewById(R.id.mobile_view)

             }

             override fun onClick(v: View) {

                 val itemPosition = data_view?.getChildLayoutPosition(v)
/*
  alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                          UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                  toast(""+itemPosition)


                 var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                 intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                 startActivity(intent)*/

             }
         }

         fun removeAt(position: Int) {
             arraylist.removeAt(position)
             notifyItemRemoved(position)
             notifyItemRangeChanged(0, arraylist.size)
         }

     }

     internal inner class AddressAdapter(private val context: Context, arraylist: ArrayList<CustomerBasicDataAdapter>) : RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerBasicDataAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_address_info_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]


            holder.address_view?.setText(""+list_items.address)
            holder.city_view?.setText(""+list_items.city)
            holder.state_view?.setText(""+list_items.state)
            holder.zipCode_view?.setText(""+list_items.zipcode)
            holder.country_view?.setText(""+list_items.countryName)



        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var address_view: TextView? = null
            var city_view: TextView? = null
            var state_view: TextView? = null
            var zipCode_view: TextView? = null
            var country_view: TextView? = null


            init {

                itemView.setOnClickListener(this)

                address_view = itemView.findViewById(R.id.address_view)
                city_view = itemView.findViewById(R.id.city_view)
                state_view =  itemView.findViewById(R.id.state_view)
                zipCode_view = itemView.findViewById(R.id.zipcode_view)
                country_view = itemView.findViewById(R.id.country_view)


            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)

/* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)


                var intent = Intent(this@CustomerDetailsActivity,CustomerDetailsActivity::class.java)
                intent.putExtra("CUSTOMER_ID",""+UserDataManager?.customersList?.get(itemPosition!!)?.customerid)
                startActivity(intent)*/

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

      internal inner class BillingAdapter(private val context: Context, arraylist: ArrayList<CustomerBasicDataAdapter>) : RecyclerView.Adapter<BillingAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerBasicDataAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_billing_shipping_address_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]




            holder.street_view?.setText(""+list_items.billingstreet)
            holder.city_view?.setText(""+list_items.billingcity)
            holder.state_view?.setText(""+list_items.billingstate)
            holder.zipcode_view?.setText(""+list_items.billingzipcode)
            holder.country_view?.setText(""+list_items.billingCountry)



        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var street_view: TextView? = null
            var city_view: TextView? = null
            var state_view: TextView? = null
            var zipcode_view: TextView? = null
            var country_view: TextView? = null


            init {


                street_view = itemView.findViewById(R.id.street_view)
                city_view =  itemView.findViewById(R.id.city_view)
                state_view = itemView.findViewById(R.id.state_view)
                zipcode_view = itemView.findViewById(R.id.zipcode_view)
                country_view = itemView.findViewById(R.id.country_view)

            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)



            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

        internal inner class ShippingAdapter(private val context: Context, arraylist: ArrayList<CustomerBasicDataAdapter>) : RecyclerView.Adapter<ShippingAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<CustomerBasicDataAdapter> = ArrayList()

        init {

            inflater = LayoutInflater.from(context)

            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.customer_billing_shipping_address_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]




            holder.street_view?.setText(""+list_items.shippingstreet)
            holder.city_view?.setText(""+list_items.shippingcity)
            holder.state_view?.setText(""+list_items.shippingstate)
            holder.zipcode_view?.setText(""+list_items.shippingzipcode)
            holder.country_view?.setText(""+list_items.shippingCountry)



        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<CustomersListAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var street_view: TextView? = null
            var city_view: TextView? = null
            var state_view: TextView? = null
            var zipcode_view: TextView? = null
            var country_view: TextView? = null


            init {


                street_view = itemView.findViewById(R.id.street_view)
                city_view =  itemView.findViewById(R.id.city_view)
                state_view = itemView.findViewById(R.id.state_view)
                zipcode_view = itemView.findViewById(R.id.zipcode_view)
                country_view = itemView.findViewById(R.id.country_view)
            }

            override fun onClick(v: View) {

                val itemPosition = data_view?.getChildLayoutPosition(v)



            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

}
