package com.averox.bizggro.Adapters

/**
 * Created by Sarim on 12/7/2017.
 */
class FinancialsAdapter {

    private var date: String? = null
    private var description: String? = null
    private var category: String? = null
    private var amount: Int? = null
    private var category_name: String? = null
    private var category_id: Int? = null
    private var invoice_id: Int? = null
    private var invoice_number: String? = null
    private var irs_category_name: String? = null
    private var irs_category_id: Int? = null
    private var project_name: String? = null
    private var po_number: String? = null
    private var due_date: String? = null
    private var status: String? = null
    private var attachment: String? = null
    private var taxID: Int? = null
    private var taxName: String? = null
    private var rate: Int? = null
    private var company_id: Int? = null
    private var addedBy: String? = null
    private var dateAdded: String? = null
    private var frequency: String? = null
    private var started_on: String? = null
    private var ending_on: String? = null
    private var next_run: String? = null
    private var transactionID: Int? = null
    private var transactionType: String? = null
    private var statusID: Int? = null


    constructor(transactionID: Int?,transactionType: String?)
    {
        this.transactionID = transactionID
        this.transactionType = transactionType
    }



    constructor(date: String?, description: String?, category: String?, amount: Int?) {
        this.date = date
        this.description = description
        this.category = category
        this.amount = amount
    }


    constructor(category_name: String?, category_id: Int?, irs_category_name: String?, irs_category_id: Int?) {
        this.category_id = category_id
        this.category_name = category_name
        this.irs_category_id = irs_category_id
        this.irs_category_name = irs_category_name
    }

    constructor(invoice_id: Int?, invoice_number: String?, project_name: String?, po_number: String?, due_date: String?, status: String?,statusID: Int?, attachment: String?) {
        this.invoice_number = invoice_number
        this.invoice_id = invoice_id
        this.project_name = project_name
        this.po_number = po_number
        this.due_date = due_date
        this.status = status
        this.attachment = attachment
        this.statusID
    }
    constructor( invoice_number: String?, project_name: String?, po_number: String?,frequency: String?, started_on: String?, ending_on: String?, status: String?,next_run: String?, attachment: String?) {
        this.invoice_number = invoice_number
        this.project_name = project_name
        this.po_number = po_number
        this.status = status
        this.attachment = attachment
        this.frequency = frequency
        this.started_on = started_on
        this.ending_on = ending_on
        this.next_run = next_run
    }

    constructor( invoice_number: String?, project_name: String?, po_number: String?, due_date: String?, status: String?, attachment: String?) {
        this.invoice_number = invoice_number
        this.project_name = project_name
        this.po_number = po_number
        this.due_date = due_date
        this.status = status
        this.attachment = attachment
    }
    constructor(taxID: Int?,taxName: String?,rate: Int?,companyid: Int?,addedBy: String?,dateAdded: String?)
    {
        this.taxID = taxID
        this.taxName = taxName
        this.rate = rate
        this.company_id = companyid
        this.addedBy = addedBy
        this.dateAdded = dateAdded


    }

    fun getDate(): String? {
        return date
    }

    fun getDescription(): String? {
        return description
    }

    fun getCategory(): String? {
        return category
    }

    fun getAmount(): Int? {
        return amount
    }

    fun getCategoryID(): Int? {
        return category_id
    }

    fun getCategoryName(): String? {
        return category_name
    }

    fun getInvoiceID(): Int? {
        return invoice_id
    }

    fun getInvoiceNumber(): String? {
        return invoice_number
    }

    fun getIrsCategoryID(): Int? {
        return irs_category_id
    }

    fun getIrsCategoryName(): String? {
        return irs_category_name
    }

    fun getProjectName(): String? {
        return project_name
    }

    fun getPoNumber(): String? {
        return po_number
    }

    fun getAttachment(): String? {
        return attachment
    }

    fun getDueDate(): String? {
        return due_date
    }

    fun getStatus(): String? {
        return status
    }

    fun getTransactionCategoryName(): String?
    {
        return category_name
    }
    fun getTaxID(): Int?
    {
        return taxID
    }
    fun getAddedBy(): String?
    {
        return addedBy
    }
    fun getDateAdded(): String?
    {
        return dateAdded
    }
    fun getRate(): Int?
    {
        return rate
    }
    fun getCompanyID(): Int?
    {
        return company_id
    }
    fun getTaxName(): String?
    {
        return taxName
    }

    fun getFrequency(): String? {return frequency}
    fun getStartedOn():String? {return started_on}
    fun getEndingOn():String? {return ending_on}
    fun getNextRun(): String? {return next_run}
    fun getTransactionID(): Int? {return transactionID}
    fun getTransactionType(): String? {return transactionType}
    fun getStatusID(): Int? {return statusID}



}
