package com.averox.bizggro.HelperClasses

import android.content.ContentQueryMap
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.util.Log
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.message.BasicNameValuePair
import java.net.URLClassLoader
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Sarim on 11/29/2017.
 */
public object UrlBuilder {
    private var loginUrl: String? = null
    private var allTransactionsUrl: String? = null
    private var allProjectsUrl: String? = null
    private var allTasksUrl: String? = null
    private var companyCategoriesUrl: String? = null
    private var allInvoicesUrl: String? = null
    private var allTaxUrl: String? = null
    private var allPOsUrl: String? = null
    private var allSubscriptionsUrl: String? = null
    private var addTransactionUrl: String? = null
    private var url: String? = null


    fun setUrl(module: String,requestCode: String,parameters: LinkedList<NameValuePair>)
    {
        var tempList: LinkedList<NameValuePair> = parameters
        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        for (a in tempList) {

            Log.d("response",""+a)

            if(!a.value.isNullOrEmpty())
            {
                requestParameters.add(a)
            }

        }

        val paramString = URLEncodedUtils.format(requestParameters, "utf-8")

        url = Constants.URL_BASE+module+"?RequestCode="+requestCode+"&"+paramString
    }
    fun getUrl(): String?
    {
        return url
    }

    fun setLoginUrl(email: String, password: String) {

        loginUrl = Constants.URL_BASE_USERS_MODULE + "RequestCode=authenticatecompanyuser&" + "email=" + email + "&" + "password=" + password

    }

    fun getLoginUrl(): String? {

        return loginUrl

    }

    fun setAllTransactionsUrl(companyid: Int?, projectID: Any?, categoryID: Any?,transactionTypeID: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,description: Any?) {

        var projectid : Any? = projectID
        var categoryid: Any? = categoryID
        var transactiontypeid: Any? = transactionTypeID
        var fromdate: Any? = fromDate
        var todate: Any? = toDate
        var applydatefilter: Any? = applyDateFilter
        var description: Any? = description

        if((projectid as Any?) == null)  projectid  = "" as String
        if((categoryid as Any?) == null){ categoryid = "" as String}
        if((transactiontypeid as Any?) == null) transactiontypeid  = "" as String
        if((fromdate as Any?) == null)  fromdate  = "" as String
        if((todate as Any?) == null)  todate  = "" as String
        if((applydatefilter as Any?) == null)  applydatefilter  = "" as String
        if((description as Any?) == null){ description = "" as String}

        allTransactionsUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=getfilteredtransactions&" + "CompanyId=" + companyid + "&ProjectId="+ projectid + "&CategoryID=" +categoryid+ "&TransactionTypeID=" +transactiontypeid+ "&FromDate=" +fromdate+ "&ToDate=" +todate+ "&ApplyDateFilter=" +applydatefilter+ "&Description=" + description

    }

    fun getAllTransactionsUrl(): String? {
        return allTransactionsUrl
    }

    fun setAllProjectsUrl(companyid: Int?) {
        allProjectsUrl = Constants.URL_BASE_PROJECTS_MODULE + "RequestCode=getallprojects&" + "CompanyId=" + companyid
    }

    fun getAllProjectsUrl(): String? {
        return allProjectsUrl
    }

    /*fun setAllTasksUrl()
    {
        allTasksUrl = Constants.URL_BASE_TASKS_MODULE+"RequestCode=getallprojects&" + "CompanyId=" + companyid
    }

    fun getAllTasksUrl(): String?
    {
        return getAllTasksUrl()
    }*/

    fun setCompanyCategoriesUrl(company_id: Int?) {
        companyCategoriesUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=getcompanycategories&" + "CompanyId=" + company_id
    }

    fun getCompanyCategoriesUrl(): String? {
        return companyCategoriesUrl
    }

    fun setAllInvoicesUrl(company_id: Int?) {
        allInvoicesUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=getinvoices&" + "CompanyId=" + company_id
    }

    fun getAllInvoicesUrl(): String? {
        return allInvoicesUrl
    }

    fun setAllTaxUrl(company_id: Int?) {
        allTaxUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=gettaxes&" + "CompanyId=" + company_id

    }

    fun getAllTaxUrl(): String? {
        return allTaxUrl
    }

    fun setAllPOsUrl(company_id: Int?) {
        allPOsUrl = "http://app.bizggro.com/api/bookkeeping.aspx?" + "RequestCode=getpos&" + "CompanyId=" + company_id
    }

    fun getAllPOsUrl(): String? {
        return allPOsUrl
    }

    fun setAllSubscriptionsUrl(company_id: Int?)
    {
        allSubscriptionsUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=getsubscriptions&" + "CompanyId=" + company_id
    }

    fun getAllSubscriptionsUrl(): String?
    {
        return allSubscriptionsUrl
    }

    fun setAddTransactionUrl(description: String?,amount: String?,date: String?,categoryID: Int?,projectID: Int?,transactionTypeID: Int?,companyID: Int?)
    {
        addTransactionUrl = Constants.URL_BASE_BOOKKEEPING_MODULE + "RequestCode=addtransaction&" + "Description=" + description+"&" + "Amount=" + amount+"&" + "Date=" + date+"&" + "CategoryID=" + categoryID+"&" + "ProjectID=" + projectID+"&" + "TransactionTypeID=" +transactionTypeID+"&" + "CompanyID=" +companyID
    }
    fun getAddTransactionUrl(): String?
    {
        return addTransactionUrl
    }
}