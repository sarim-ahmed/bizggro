package com.averox.bizggro.Adapters

/**
 * Created by Sarim on 1/16/2018.
 */
class CustomerBasicDataAdapter(var contactID: Any?, var firstname: Any?, var lastname: Any?, var email: Any?, var phone: Any?, var mobile: Any?,
                               var address: Any?, var creationdate: Any?, var city: Any?, var state: Any?, var countryid: Any?,var countryName: Any?,
                               var details: Any?, var companyid: Any?, var dateupdated: Any?, var createdby: Any?, var updatedby: Any?,
                               var contacttypeId: Any?, var companyname: Any?, var categoryid: Any?,var contactCategory: Any?, var zipcode: Any?, var ishidden: Any?
                               , var customerid: Any?, var convertedby: Any?, var dateconverted: Any?, var typeid: Any?, var creditworthinessid: Any?, var creditWorthness: Any?, var contactpersonid: Any?,var contactPersonUsername: Any?,
                                var paymenttermsid: Any?,var paymentTerms: Any?, var billingstreet: Any?, var billingcity: Any?, var billingstate: Any?, var billingzipcode: Any?, var billingcountryid: Any?
                               , var shippingstreet: Any?, var shippingcity: Any?, var shippingstate: Any?, var shippingzipcode: Any?, var shippingcountryid: Any?, var type: Any?, var convertedbyuser: Any?,var billingCountry: Any?,var shippingCountry: Any?
)
