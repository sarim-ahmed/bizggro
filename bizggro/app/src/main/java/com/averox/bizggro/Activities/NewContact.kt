package com.averox.bizggro.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.ContactsDataManager
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class NewContact : AppCompatActivity(),View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var button_basic: ImageButton? = null
    private var button_address: ImageButton? = null
    private var layout_basicInfo: LinearLayout? = null
    private var layout_addressInfo: LinearLayout? = null
    private var button_create: Button? = null
    private var input_firstname: EditText? = null
    private var input_lastname: EditText? = null
    private var input_mobile: EditText? = null
    private var input_email: EditText? = null
    private var input_phone: EditText? = null
    private var input_company: EditText? = null
    private var spinner_category: Spinner? = null
    private var input_detail: EditText? = null
    private var input_street: EditText? = null
    private var input_city: EditText? = null
    private var input_state: EditText? = null
    private var input_zipCode: EditText? = null
    private var spinner_country: Spinner? = null
    private var firstname: Any? = ""
    private var lastname: Any? = ""
    private var mobile: Any? = ""
    private var email: Any? = ""
    private var phone: Any? = ""
    private var company: Any? = ""
    private var detail: Any? = ""
    private var street: Any? = ""
    private var city: Any? = ""
    private var state: Any? =""
    private var zipCode: Any? = ""
    private var categoryNameArray: ArrayList<String>? = null
    private var countryNameArray: ArrayList<String>? = null

    private var categoryID: Any? = ""
    private var countryID: Any? = ""
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var progressDialog: ProgressDialog? = null





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_contact)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        input_firstname = findViewById(R.id.input_firstname)
        input_lastname = findViewById(R.id.input_lastname)
        input_detail = findViewById(R.id.input_detail)
        input_mobile = findViewById(R.id.input_mobile)
        input_email = findViewById(R.id.input_email)
        input_phone = findViewById(R.id.input_phone)
        input_company = findViewById(R.id.input_company)
        spinner_category = findViewById(R.id.spinner_category)
        input_street = findViewById(R.id.input_street)
        input_city = findViewById(R.id.input_city)
        input_state = findViewById(R.id.input_state)
        input_zipCode = findViewById(R.id.input_zipCode)
        spinner_country = findViewById(R.id.spinner_country)

        button_address = findViewById(R.id.button_address)
        button_address?.setOnClickListener(this)
        button_basic = findViewById(R.id.button_basic)
        button_basic?.setOnClickListener(this)
        button_create = findViewById(R.id.button_create)
        button_create?.setOnClickListener(this)

        layout_addressInfo =  findViewById(R.id.layout_addressInfo)
        layout_basicInfo = findViewById(R.id.layout_basicInfo)

        categoryNameArray = ArrayList()
        countryNameArray = ArrayList()

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE


        showBasicForm()
        loadSpinnerValues()


        spinner_category?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    categoryID = ""
                }
                else
                {
                    categoryID = ContactsDataManager?.categoriesList?.get(position - 1)?.categoryId
                    Log.d("response", "categoryId: " + categoryID)
                }




            }

        }



        spinner_country?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    countryID = ""
                }
                else
                {
                    countryID = ContactsDataManager?.countriesList?.get(position)?.CountryId
                    Log.d("newTransaction", "categorytId: " + countryID)
                }




            }

        }

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
    }

    override fun onClick(v: View?) {
when(v?.id)
{
    R.id.button_basic->
    {
        showBasicForm()



    }
    R.id.button_address->
    {
      showAddressForm()


    }
    R.id.button_create->
    {
        /*firstname = Uri.encode(input_firstname?.text.toString().trim(), "UTF-8")
        lastname = Uri.encode(input_lastname?.text.toString().trim(), "UTF-8")
        email = Uri.encode(input_email?.text.toString().trim(), "UTF-8")
        company = Uri.encode(input_company?.text.toString().trim(), "UTF-8")*/

        firstname = input_firstname?.text.toString().trim()
        lastname = input_lastname?.text.toString().trim()
        email = input_email?.text.toString().trim()
        company = input_company?.text.toString().trim()

        if(firstname?.toString()!!.isEmpty() || firstname!!.equals(" "))
        {
            input_firstname?.setError(resources.getString(R.string.required))
        }
        else if(lastname?.toString()!!.isEmpty() || lastname!!.equals(" "))
        {
            input_lastname?.setError(resources.getString(R.string.required))
        }
        else if(email?.toString()!!.isEmpty() || email!!.equals(" "))
        {
            input_email?.setError(resources.getString(R.string.required))
        }
        else if(company?.toString()!!.isEmpty() || company!!.equals(" "))
        {
            input_company?.setError(resources.getString(R.string.required))
        }
        else if (spinner_category!!.selectedItemPosition == 0) {

            AlertManager("Select Category", 4000, this@NewContact)
        }

        else {

            try {
            } catch (e: Exception) {
            } finally {
            }
            createContact()
        }

    }
}
    }
    fun showBasicForm()
    {
        layout_basicInfo?.visibility = View.VISIBLE
        layout_addressInfo?.visibility = View.GONE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info_pressed))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info))

        button_basic?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
        button_address?.setBackgroundDrawable(null)
    }
    fun showAddressForm()
    {
        layout_basicInfo?.visibility = View.GONE
        layout_addressInfo?.visibility = View.VISIBLE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info_pressed))

        button_basic?.setBackgroundDrawable(null)
        button_address?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
    }




    fun loadSpinnerValues()
    {

        countryNameArray?.add("Select One")
        var spinnerCountryAdapter = ArrayAdapter(this, R.layout.spinner_item_style,countryNameArray)


        for(i in 0..ContactsDataManager?.countriesList!!.size - 1)
        {
            if(ContactsDataManager?.countriesList?.get(i)?.Name?.equals(null) == false)
            {
                countryNameArray?.add(ContactsDataManager?.countriesList?.get(i)?.Name.toString())

            }

        }


        spinner_country?.adapter = spinnerCountryAdapter


        categoryNameArray?.add("Select One")
        var spinnerCategoryAdapter = ArrayAdapter(this, R.layout.spinner_item_style,categoryNameArray)


        for(i in 0..ContactsDataManager.categoriesList!!.size - 1)
        {
            if(ContactsDataManager.categoriesList?.get(i)?.category?.equals(null) == false)
            {
                categoryNameArray?.add(ContactsDataManager?.categoriesList?.get(i)?.category.toString()!!)

            }

        }


        spinner_category?.adapter = spinnerCategoryAdapter



    }


    fun createContact()
    {
        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,

        progressDialog = ProgressDialog.show(this, "Accessing Data", "Please wait....", true)
        progressDialog?.setCancelable(false)


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.UserId,loginManager?.getUserID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FirstName,firstname.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.LastName,lastname?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Email,email?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Mobile,mobile?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Phone,phone?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CompanyName,company?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CategoryId,categoryID?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Address,street?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.City,city?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.State,state?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ZipCode,zipCode?.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CountryID,countryID?.toString()))


        UrlBuilder?.setUrl(Module.contacts, RequestCode.addcontact,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
         if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.success)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                progressDialog?.dismiss()

                // loader?.visibility = View.GONE



                    alert(resources.getString(R.string.contactAdded)).show()

                    Handler().postDelayed(object : Runnable {

                        // Using handler with postDelayed called runnable run method


                        override fun run() {

                            var intent = Intent(this@NewContact,AllContacts::class.java)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                            finish()
                        }
                    }, 2 * 1000) // wait for 5 seconds




            }
            else
         {
             progressDialog?.dismiss()
alert("Error").show()
         }



        },
                Response.ErrorListener {
                    error ->



                    Log.d("response","error: " +error.message.toString())

                    progressDialog?.dismiss()
                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)
    }
}
