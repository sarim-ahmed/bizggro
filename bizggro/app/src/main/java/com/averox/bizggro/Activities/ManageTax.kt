package com.averox.bizggro.Activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.Constants
import com.averox.bizggro.HelperClasses.JsonKeys
import com.averox.bizggro.HelperClasses.UrlBuilder
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.tax_item.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.json.JSONObject

class ManageTax : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var tax_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_tax)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tax_view = findViewById(R.id.tax_view)

        loginManager = LoginManager(Constants.context!!)
        loader = findViewById(R.id.loader)

        getAllTax()
    }
    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

        try {
            UserDataManager?.allTaxList?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }

    fun getAllTax()
    {

        UrlBuilder.setAllTaxUrl(loginManager?.getCompanyID())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getAllTaxUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)

            }
            else
            {
                val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                for(i in 0..data.length() - 1)
                {
                    var result = data.getJSONObject(i)

                    var taxID = result.getInt(JsonKeys.variables.KEY_TAXID)
                    var taxName = result.getString(JsonKeys.variables.KEY_NAME)
                    var rate = result.getInt(JsonKeys.variables.KEY_RATE)
                    var companyID = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                    var added_by = result.getString(JsonKeys.variables.KEY_USERNAME)
                    var dateAdded = result.getString(JsonKeys.variables.KEY_DATEADDED)

                    UserDataManager.insertAllTaxes(taxID,taxName,rate,companyID,added_by,dateAdded)


                    var adapter = CustomAdapter(this)
                    tax_view?.adapter = adapter
                    tax_view?.layoutManager = LinearLayoutManager(this)


                }


            }

            loader?.visibility = View.GONE

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE
                    alert(resources.getString(R.string.networkProblem))

                }

        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    internal inner class CustomAdapter(private val context: Context) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null

        init {
            inflater = LayoutInflater.from(context)
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.tax_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = UserDataManager.allTaxList!![position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/

            var sr = position + 1

            holder.sr_view?.setText(""+sr)

            holder.tax_title_view?.setText(""+list_items.getTaxName())
            holder.rate_view?.setText(""+list_items.getRate())
            holder.added_by_view?.setText(""+list_items.getAddedBy())

            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, UserDataManager.allTaxList!!.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+ UserDataManager.allTaxList!!.size)

            return UserDataManager.allTaxList!!.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var tax_title_view: TextView? = null
            var rate_view: TextView? = null
            var added_by_view: TextView? = null

            init {

                itemView.setOnClickListener(this)
                sr_view = itemView.findViewById(R.id.sr_view)
                tax_title_view = itemView.findViewById(R.id.tax_title_view)
                rate_view = itemView.findViewById(R.id.rate_view)
                added_by_view = itemView.findViewById(R.id.added_by_view)


            }

            override fun onClick(v: View) {

                val itemPosition = tax_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

            }
        }

        fun removeAt(position: Int) {
            UserDataManager.allTaxList?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, UserDataManager.allTaxList!!.size)
        }

    }

}
