package com.averox.bizggro.Activities

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.HelperClasses.Constants
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager

class Categories : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var categories_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        categories_view = findViewById(R.id.categories_view)

        loginManager = LoginManager(Constants.context!!)

        // getAllInvoices()
        loader = findViewById(R.id.loader)

        var adapter = CustomAdapter(this)
        categories_view?.adapter = adapter
        categories_view?.layoutManager = LinearLayoutManager(this)

        loader?.visibility = View.GONE


    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }

    internal inner class CustomAdapter(private val context: Context) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null

        init {
            inflater = LayoutInflater.from(context)
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.categories_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = UserDataManager.companyCategoriesList!![position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/

            var sr_number = position+1

            holder.sr_view?.setText(""+sr_number)
            holder.transaction_category_view?.setText(""+list_items.getTransactionCategoryName())
            holder.irs_category_view?.setText(""+list_items.getIrsCategoryName())


            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, UserDataManager.companyCategoriesList!!.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+ UserDataManager.companyCategoriesList!!.size)

            return UserDataManager.companyCategoriesList!!.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var transaction_category_view: TextView? = null
            var irs_category_view: TextView? = null


            init
            {

                itemView.setOnClickListener(this)
                sr_view = itemView.findViewById(R.id.sr_view)
                transaction_category_view = itemView.findViewById(R.id.transaction_category_view)
                irs_category_view = itemView.findViewById(R.id.irs_category_view)

            }

            override fun onClick(v: View) {

                val itemPosition = categories_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

            }
        }

        fun removeAt(position: Int) {
            UserDataManager.companyCategoriesList?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, UserDataManager.companyCategoriesList!!.size)
        }

    }


}
