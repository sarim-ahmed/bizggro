package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 11/28/2017.
 */
public class UserDataManager
{
    companion object {


        public var allTransactionsList: ArrayList<TransactionsAdapter>? = null
        public var allProjectsList: ArrayList<ProjectsAdapter>? = null
        public var companyCategoriesList: ArrayList<FinancialsAdapter>? = null
        public var allInvoicesList: ArrayList<FinancialsAdapter>? = null
        public var allTaxList: ArrayList<FinancialsAdapter>? = null
        public var allPOsList: ArrayList<FinancialsAdapter>? = null
        public var allSubscriptionsList: ArrayList<FinancialsAdapter>? = null
        public var transactionTypeList: ArrayList<FinancialsAdapter>? = null
        public var customersList: ArrayList<CustomersListAdapter>? = null
        public var customerTypeList: ArrayList<CustomersTypeAdapter>? = null
        public var incomeExpenseDataList: ArrayList<IncomeExpenseChartDataAdapter>? = null
        public var userGroupsList: ArrayList<UsersGroupsAdapter>? = null
        public var usersInfoList: ArrayList<UsersInfoAdapter>? = null
        public var groupMembersList: ArrayList<GroupMembersAdapter>? = null
        public var arraylist_selected_userIDs_To: ArrayList<Int>? = null
        public var arraylist_selected_userIDs_Cc: ArrayList<Int>? = null
        public var type: String? = null


        init {
            allTransactionsList = ArrayList<TransactionsAdapter>()
            allProjectsList = ArrayList<ProjectsAdapter>()
            companyCategoriesList = ArrayList<FinancialsAdapter>()
            allInvoicesList = ArrayList<FinancialsAdapter>()
            allTaxList = ArrayList<FinancialsAdapter>()
            allPOsList = ArrayList<FinancialsAdapter>()
            allSubscriptionsList = ArrayList<FinancialsAdapter>()
            transactionTypeList = ArrayList<FinancialsAdapter>()
            customersList = ArrayList<CustomersListAdapter>()
            customerTypeList = ArrayList<CustomersTypeAdapter>()
            incomeExpenseDataList = ArrayList<IncomeExpenseChartDataAdapter>()
            userGroupsList = ArrayList<UsersGroupsAdapter>()
            usersInfoList = ArrayList<UsersInfoAdapter>()
            groupMembersList = ArrayList<GroupMembersAdapter>()
            arraylist_selected_userIDs_To = ArrayList()
            arraylist_selected_userIDs_Cc = ArrayList()

        }

        fun insertAllTransactions(transactionID: Any?,description: Any?,amount: Any?,date: Any?,categoryID: Any?, projectID: Any?,
                                  transactionTypeID: Any?, companyID: Any?, invoiceNumber: Any?,
                                   attachment: Any?, poNumber: Any?, categoryName: Any?,
                                   documentName: Any?,invoiceID: Any?, poID: Any?,projectName: Any?,
                                  transactionTypeName: Any?, documentUrl: Any?)
        {
            allTransactionsList?.add(TransactionsAdapter(transactionID,description,amount,date,categoryID, projectID,
            transactionTypeID, companyID, invoiceNumber,
            attachment, poNumber,categoryName,documentName,invoiceID,poID,projectName,
                    transactionTypeName,documentUrl))
        }

        fun insertAllProjects(projectID: Int?,projectName: String?)
        {
            allProjectsList?.add(ProjectsAdapter(projectID,projectName))
        }
        fun insertCompanyCategories(category_name: String?,category_id: Int?,irs_name: String?,irs_category_id: Int?)
        {
            companyCategoriesList?.add(FinancialsAdapter(category_name,category_id,irs_name,irs_category_id))
        }
        fun insertAllInvoices( invoice_id: Int?,invoice_number: String?,project_name: String?,po_number: String?,due_date: String?,status: String?,statusID: Int?,attachment: String?)
        {
            allInvoicesList?.add(FinancialsAdapter(invoice_id,invoice_number,project_name,po_number,due_date,status,statusID,attachment))
        }

        fun insertAllTaxes(taxid: Int?,name: String?,rate: Int?,companyid: Int?,addedby: String?,dateAdded: String?)
        {
            allTaxList?.add(FinancialsAdapter(taxid,name,rate,companyid,addedby,dateAdded))
        }
        fun insertAllPOs(invoice_number: String?,project_name: String?,po_number: String?,due_date: String?,status: String?,attachment: String?)
        {
            allPOsList?.add(FinancialsAdapter(invoice_number,project_name,po_number,due_date,status,attachment))
        }

        fun insertAllSubscriptions(invoice_number: String?, project_name: String?, po_number: String?,frequency: String?, started_on: String?, ending_on: String?, status: String?,next_run: String?, attachment: String?)
        {
            allSubscriptionsList?.add(FinancialsAdapter(invoice_number, project_name, po_number,frequency, started_on, ending_on, status,next_run, attachment))
        }

        fun insertTransactionTypes(transactionID: Int?,transactionType: String?)
        {
            transactionTypeList?.add(FinancialsAdapter(transactionID,transactionType))
        }

        fun insertCustomers( contactID: Any?, firstname: Any?,lastname: Any?, email: Any?,phone: Any?,mobile: Any?,
                            address: Any?, creationdate: Any?,  city: Any?, state: Any?, countryid: Any?,
                             details: Any?,companyid: Any?, dateupdated: Any?, createdby: Any?, updatedby: Any?,
                            contacttypeId: Any?,companyname: Any?, categoryid: Any?, zipcode: Any?, ishidden: Any?
                            , customerid: Any?, convertedby: Any?,dateconverted: Any?,typeid: Any?,creditworthinessid: Any?,contactpersonid: Any?
                            ,paymenttermsid: Any?,  billingstreet: Any?, billingcity: Any?, billingstate: Any?, billingzipcode: Any?, billingcountryid: Any?
                            ,shippingstreet: Any?, shippingcity: Any?, shippingstate: Any?, shippingzipcode: Any?,
                             shippingcountryid: Any?, type: Any?,convertedbyuser: Any?)
        {
            customersList?.add(CustomersListAdapter(contactID, firstname,lastname, email,phone,mobile, address, creationdate,  city, state, countryid, details,companyid,
                    dateupdated, createdby, updatedby, contacttypeId,companyname, categoryid, zipcode, ishidden
            , customerid, convertedby,dateconverted,typeid,creditworthinessid,contactpersonid
            ,paymenttermsid,  billingstreet, billingcity, billingstate, billingzipcode, billingcountryid
            ,shippingstreet, shippingcity, shippingstate, shippingzipcode,
            shippingcountryid, type,convertedbyuser))
        }

        fun insertCustomerTypes(typeid: Int?, type: String?, companyid: Int?, addedby: Int?,
                                 dateadded: String?, updatedby: String?,dateupdated: String?,
                                username: String? )
        {
            customerTypeList?.add(CustomersTypeAdapter(typeid, type, companyid, addedby,
            dateadded, updatedby,dateupdated,
            username ))
        }

        fun insertIncomeExpenseData(name: Any?, amount: Int?)
        {
            incomeExpenseDataList?.add(IncomeExpenseChartDataAdapter(name,amount))
        }

        fun insertUserGroups(groupID: Any?,groupName: Any?,companyID: Any?,dateCreated: Any?,dateUpdated: Any?,createdBy: Any?)
        {
            userGroupsList?.add(UsersGroupsAdapter(groupID,groupName,companyID,dateCreated,dateUpdated,createdBy))
        }


        fun insertUsersInfo(userID: Any?, firstName: Any?, lastName: Any?, fullName: Any?)
        {
            usersInfoList?.add(UsersInfoAdapter(userID, firstName, lastName, fullName))
        }

        fun insertGroupMembers(userID: Any?, firstName: Any?, lastName: Any?, fullName: Any?)
        {
            groupMembersList?.add(GroupMembersAdapter(userID, firstName, lastName, fullName))
        }


    }
}