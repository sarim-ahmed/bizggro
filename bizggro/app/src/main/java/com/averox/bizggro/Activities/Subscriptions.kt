package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.activity_all_transactions.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*

class Subscriptions : AppCompatActivity(), View.OnClickListener {


    private var toolbar: Toolbar? = null
    private var subscription_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_projects: Spinner? = null
    private var spinner_status: Spinner? = null
    private var spinner_frequency: Spinner? = null
    private var spinner_date: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var statusNameArray: ArrayList<String>? = null
    private var frequencyNameArray: ArrayList<String>? = null
    private var frequencyID: Any = ""
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var invoice_po_notes: Any = ""
    private var projectID: Any = ""
    private var statusID: Any = ""
    private var applyDateFilter: Any = ""
    private var input_invoice_number: EditText? = null
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var currentDate: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscriptions)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subscription_view = findViewById(R.id.subscription_view)

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)


        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_projects = findViewById(R.id.spinner_projects)
        spinner_status = findViewById(R.id.spinner_status)
        spinner_frequency = findViewById(R.id.spinner_frequency)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)

        input_fromDate = findViewById(R.id.input_fromdate)
        input_fromDate?.setOnClickListener(this)
        input_toDate = findViewById(R.id.input_todate)
        input_toDate?.setOnClickListener(this)

        input_invoice_number = findViewById(R.id.input_invoice_number)

        projectNameArray = ArrayList()
        statusNameArray = ArrayList()
        frequencyNameArray = ArrayList()


        getAllSubscriptions(loginManager?.getCompanyID(), "", "", "", "1918/01/01",currentDate, "","")
        loadSpinnerValues()
        setCurrentDate()
        spinner_date?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position.equals(0)) {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = 0
                } else if (position.equals(1)) {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1

                }
            }
        }

        spinner_projects?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    projectID = ""
                }
                else {
                    projectID = UserDataManager.allProjectsList?.get(position - 1)?.getProjectID()!!

                    Log.d("newTransaction", "projectId: " + projectID)
                }
            }

        }

        spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position.equals(0)) {
                    statusID = ""
                } else if (position.equals(1)) {
                    statusID = 3
                } else if (position.equals(2)) {
                    statusID = 1
                }
            }
        }
        spinner_frequency?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position.equals(0)) {
                    frequencyID = ""
                } else {
                    frequencyID = spinner_frequency?.getItemAtPosition(position)!!
                    Log.d("response", "" + frequencyID)
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {
            UserDataManager?.allSubscriptionsList?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }


    fun getAllSubscriptions(companyid: Int?, projectID: Any?,frequencyID: Any?, statusID: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,invoice_po_notes: Any?) {


        try {

            UserDataManager.allSubscriptionsList?.clear()
            var adapter = CustomAdapter(this)
            subscription_view?.adapter = adapter
            subscription_view?.layoutManager = LinearLayoutManager(this)

            loader?.visibility = View.VISIBLE

            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.Keyword,invoice_po_notes.toString()))

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.StatusID,statusID.toString()))

            requestParameters.add(BasicNameValuePair(Parameters.Duration,frequencyID.toString()))

            requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))

            UrlBuilder.setUrl(Module.bookkeeping, RequestCode.getsubscriptions,requestParameters)
            Log.d("response", "" + UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(), null, Response.Listener<JSONObject> {

                response ->

                Log.d("response", response.toString())
                Log.d("response", response.getString(JsonKeys.objects.KEY_DESCRIPTION))

                if (response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound))) {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    loader?.visibility = View.GONE
                    alert(resources.getString(R.string.NoDataFound)).show()

                } else {


                    try {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                        for (i in 0..data.length() - 1) {
                            var result = data.getJSONObject(i)
                            var invoice_number = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                            var project_name = result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                            var po_number = result.getString(JsonKeys.variables.KEY_PONUMBER)
                            var duedate = result.getString(JsonKeys.variables.KEY_DUEDATE)
                            var status = result.getString(JsonKeys.variables.KEY_TRANSACTIONSTATUS)
                            var attachment = result.getString(JsonKeys.variables.KEY_ATTACHMENT)
                            var frequency = "" + result.getInt(JsonKeys.variables.KEY_FREQUENCY) + " Every"
                            var started_on = result.getString(JsonKeys.variables.KEY_STARTDATE)
                            var ending_on = result.getString(JsonKeys.variables.KEY_ENDDATE)
                            var next_run = result.getString(JsonKeys.variables.KEY_NEXTRUN)

                            var never_ending = result.getBoolean(JsonKeys.variables.KEY_NEVERENDING)

                            if (never_ending) {
                                ending_on = resources.getString(R.string.never)
                            }



                            UserDataManager.insertAllSubscriptions(invoice_number, project_name, po_number, frequency, started_on, ending_on, status, next_run, attachment)


                            var adapter = CustomAdapter(this)
                            subscription_view?.adapter = adapter
                            subscription_view?.layoutManager = LinearLayoutManager(this)


                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                }
                loader?.visibility = View.GONE


            },
                    Response.ErrorListener { error ->
                        loader?.visibility = View.GONE
                        alert(resources.getString(R.string.networkProblem))

                    }
            ) {}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response", "" + e.message)
        } finally {
        }

    }

    internal inner class CustomAdapter(private val context: Context) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null

        init {
            inflater = LayoutInflater.from(context)
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.subscription_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = UserDataManager.allSubscriptionsList!![position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/

            var sr_number = position + 1

            holder.sr_view?.setText("" + sr_number)
            holder.invoice_number_view?.setText("" + list_items.getInvoiceNumber())
            holder.project_view?.setText("" + list_items.getProjectName())
            holder.po_view?.setText("" + list_items.getPoNumber())
            holder.status_view?.setText("" + list_items.getStatus())
            holder.attachment_view?.setText("" + list_items.getAttachment())
            holder.frequency?.setText("" + list_items.getFrequency())
            holder.nextrun?.setText("" + list_items.getNextRun())
            holder.startedon?.setText("" + list_items.getStartedOn())
            holder.endingon?.setText("" + list_items.getEndingOn())


            Log.d("holder", "" + list_items.getProjectName())
            Log.d("holder", "" + list_items.getEndingOn())

            Log.d("holder", "" + list_items.getStartedOn())

            Log.d("holder", "" + list_items.getNextRun())

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, UserDataManager.allSubscriptionsList!!.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder", "" + UserDataManager.allSubscriptionsList!!.size)

            return UserDataManager.allSubscriptionsList!!.size

        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var invoice_number_view: TextView? = null
            var project_view: TextView? = null
            var po_view: TextView? = null
            var status_view: TextView? = null
            var attachment_view: TextView? = null
            var startedon: TextView? = null
            var endingon: TextView? = null
            var nextrun: TextView? = null
            var frequency: TextView? = null

            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                sr_view = itemView.findViewById(R.id.sr_view)
                invoice_number_view = itemView.findViewById(R.id.invoice_number_view)
                project_view = itemView.findViewById(R.id.project_view)
                po_view = itemView.findViewById(R.id.po_view)
                status_view = itemView.findViewById(R.id.status_view)
                attachment_view = itemView.findViewById(R.id.attachment_view)
                startedon = itemView.findViewById(R.id.startedon_view)
                endingon = itemView.findViewById(R.id.endingon_view)
                nextrun = itemView.findViewById(R.id.nextrun_view)
                frequency = itemView.findViewById(R.id.frequency_view)

            }

            override fun onClick(v: View) {

                val itemPosition = subscription_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

            }

        }

        fun removeAt(position: Int) {
            UserDataManager.allSubscriptionsList?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, UserDataManager.allSubscriptionsList!!.size)
        }

    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.input_todate -> {
                setUpToDate()
            }
            R.id.input_fromdate -> {
                setUpFromDate()
            }
            R.id.button_search -> {

                invoice_po_notes = Uri.encode(input_invoice_number?.text.toString().trim(), "UTF-8")

                if (invoice_po_notes.equals(null)) {
                    invoice_po_notes = ""
                }

                Log.d("response", "keyword:: " + invoice_po_notes.toString())
                fromDate = Uri.encode(input_fromDate?.text.toString(), "UTF-8")

                toDate = Uri.encode(input_toDate?.text.toString(), "UTF-8")


                if(applyDateFilter.equals(0))
                {
                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()
                }


                getAllSubscriptions(loginManager?.getCompanyID(), projectID,frequencyID, statusID, fromDate, toDate, applyDateFilter, invoice_po_notes)

            }
            R.id.button_reset -> {
                input_invoice_number?.setText("")
                spinner_projects?.setSelection(0)
                spinner_status?.setSelection(0)
                spinner_date?.setSelection(0)

                getAllSubscriptions(loginManager?.getCompanyID(), "", "", "", "1918/01/01",currentDate, "","")

            }
        }
    }


    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_fromdate?.setText(resources.getString(R.string.fromDate))
        input_todate?.setText(resources.getString(R.string.toDate))
        currentDate =  formattedDate
    }

    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth )
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

        fun loadSpinnerValues() {

            projectNameArray?.add("Any Project")
            var spinnerProjectAdapter = ArrayAdapter(this, R.layout.spinner_item_style, projectNameArray)

            spinner_projects?.adapter = spinnerProjectAdapter
            for (i in 0..UserDataManager?.allProjectsList!!.size - 1) {
                if (UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false) {
                    projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

                }

            }


            spinner_projects?.adapter = spinnerProjectAdapter


            var spinnerCategoryAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.transaction_status_array))
            spinner_categories?.adapter = spinnerCategoryAdapter


            var spinnerStatusAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.transaction_status_array))

            spinner_status?.adapter = spinnerStatusAdapter


            var spinnerFrequencyAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.frequency_array))

            spinner_frequency?.adapter = spinnerFrequencyAdapter

            var spinnerDateAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.array_datefilter))

            spinner_date?.adapter = spinnerDateAdapter


        }


}
