package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.activity_all_transactions.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class POs : AppCompatActivity(), View.OnClickListener{

    private var toolbar: Toolbar? = null
    private var pos_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_projects: Spinner?= null
    private var spinner_status: Spinner? = null
    private var spinner_date: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var statusNameArray: ArrayList<String>? = null
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var invoice_po_notes: Any = ""
    private var projectID: Any = ""
    private var statusID: Any = ""
    private var applyDateFilter: Any = ""
    private var input_invoice_number: EditText? = null
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var description: Any = ""
    private var categoryID: Any = ""
    private var transactionTypeID: Any? = null
    private var poList: ArrayList<FinancialsAdapter>? = null
    private var currentDate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pos)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        pos_view = findViewById(R.id.pos_view)

        loginManager = LoginManager(Constants.context!!)
        loader = findViewById(R.id.loader)

        var adapter = CustomAdapter(this,UserDataManager.allPOsList!!)
        pos_view?.adapter = adapter
        pos_view?.layoutManager = LinearLayoutManager(this)
        loader?.visibility = View.GONE


        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_projects = findViewById(R.id.spinner_projects)
        spinner_status = findViewById(R.id.spinner_status)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)

        input_fromDate = findViewById(R.id.input_fromdate)
        input_fromDate?.setOnClickListener(this)
        input_toDate = findViewById(R.id.input_todate)
        input_toDate?.setOnClickListener(this)

        input_invoice_number = findViewById(R.id.input_invoice_number)

        projectNameArray = ArrayList()
        statusNameArray  = ArrayList()

        poList = ArrayList()

        setCurrentDate()
        loadSpinnerValues()

       // getAllPos(loginManager?.getCompanyID(), "", "", "", "", "", "","")

        getAllPos(loginManager?.getCompanyID(), "", "", "1918/01/01",currentDate, "", "")


        spinner_date?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position.equals(0))
                {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = 0
                }
                else if(position.equals(1))
                {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1

                }
            }
        }

        spinner_projects?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    projectID = ""
                }
                else {
                    projectID = UserDataManager.allProjectsList?.get(position - 1)?.getProjectID()!!

                    Log.d("newTransaction", "projectId: " + projectID)
                }
            }

        }
        spinner_status?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position.equals(0))
                {
                    statusID = ""
                }
                else if(position.equals(1))
                {
                    statusID = 4


                }
                else if(position.equals(2))
                {
                    statusID = 2

                }
            }
        }


    }
    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }

    fun getAllPos(companyid: Int?, projectID: Any?, statusID: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,invoice_po_notes: Any?)
    {

        try {
            poList?.clear()
            var adapter = CustomAdapter(this,poList!!)
            pos_view?.adapter = adapter
            pos_view?.layoutManager = LinearLayoutManager(this)
            loader?.visibility = View.VISIBLE


            var requestParameters: LinkedList<NameValuePair> = LinkedList()

            requestParameters.add(BasicNameValuePair(Parameters.Keyword,invoice_po_notes.toString()))

            requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.StatusID,statusID.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
            requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))


            UrlBuilder.setUrl(Module.bookkeeping,RequestCode.getpos,requestParameters)
            Log.d("response",""+UrlBuilder.getUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",response.toString())
                if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                {
                    //progressDialog?.dismiss()
                    //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                    //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                    loader?.visibility = View.GONE
                    alert(resources.getString(R.string.NoDataFound)).show()

                }
                else
                {
                    val data = response.getJSONArray(JsonKeys.arrays.KEY_DATA)

                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var invoice_number = result.getString(JsonKeys.variables.KEY_INVOICENUMBER)
                        var project_name = result.getString(JsonKeys.variables.KEY_PROJECTNAME)
                        var po_number = result.getString(JsonKeys.variables.KEY_PONUMBER)
                        var duedate = result.getString(JsonKeys.variables.KEY_DUEDATE)
                        var status = result.getString(JsonKeys.variables.KEY_TRANSACTIONSTATUS)
                        var attachment = result.getString(JsonKeys.variables.KEY_ATTACHMENT)

                        poList?.add(FinancialsAdapter(invoice_number,project_name,po_number,duedate,status,attachment))


                        var adapter = CustomAdapter(this,poList!!)
                        pos_view?.adapter = adapter
                        pos_view?.layoutManager = LinearLayoutManager(this)
                    }
                }
                loader?.visibility = View.GONE


            },
                    Response.ErrorListener {
                        error ->
                        loader?.visibility = View.GONE

                    }
            ){}

            ApplicationController.instance?.addToRequestQueue(request)
        } catch (e: Exception) {
            Log.d("response",""+e.message)
        } finally {
        }

    }

    internal inner class CustomAdapter(private val context: Context, arraylist: ArrayList<FinancialsAdapter>) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<FinancialsAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)
            this.arraylist = arraylist

        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.pos_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist!![position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/

            var sr_number = position+1

            holder.sr_view?.setText(""+sr_number)
            holder.invoice_number_view?.setText(""+list_items.getInvoiceNumber())
            holder.project_view?.setText(""+list_items.getProjectName())
            holder.po_view?.setText(""+list_items.getPoNumber())
            holder.duedate_view?.setText(""+list_items.getDueDate())
            holder.status_view?.setText(""+list_items.getStatus())
            holder.attachment_view?.setText(""+list_items.getAttachment())

            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, arraylist!!.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+ arraylist!!.size)

            return arraylist!!.size

        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var sr_view: TextView? = null
            var invoice_number_view: TextView? = null
            var project_view: TextView? = null
            var po_view: TextView? = null
            var duedate_view: TextView? = null
            var status_view: TextView? = null
            var attachment_view: TextView? = null

            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                sr_view = itemView.findViewById(R.id.sr_view)
                invoice_number_view = itemView.findViewById(R.id.invoice_number_view)
                project_view = itemView.findViewById(R.id.project_view)
                po_view = itemView.findViewById(R.id.po_view)
                duedate_view = itemView.findViewById(R.id.duedate_view)
                status_view = itemView.findViewById(R.id.status_view)
                attachment_view =  itemView.findViewById(R.id.attachment_view)

            }

            override fun onClick(v: View) {

                val itemPosition = pos_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/


            }
        }

        fun removeAt(position: Int) {
            UserDataManager.allPOsList?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, UserDataManager.allPOsList!!.size)
        }

    }



    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.input_todate -> {
                setUpToDate()
            }
            R.id.input_fromdate -> {
                setUpFromDate()
            }
            R.id.button_search -> {

                invoice_po_notes = Uri.encode(input_invoice_number?.text.toString().trim(), "UTF-8")

                if (invoice_po_notes.equals(null)) {
                    invoice_po_notes = ""
                }

                Log.d("response", "keyword:: " + invoice_po_notes.toString())
                //fromDate = Uri.encode(input_fromDate?.text.toString(), "UTF-8")
                fromDate = input_fromDate?.text.toString()

                //toDate = Uri.encode(input_toDate?.text.toString(), "UTF-8")
                toDate = input_toDate?.text.toString()


                if(applyDateFilter.equals(0))
                {
                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()
                }

                getAllPos(loginManager?.getCompanyID(), projectID, statusID, fromDate, toDate, applyDateFilter, invoice_po_notes)



            }
            R.id.button_reset -> {
                input_invoice_number?.setText("")
                spinner_projects?.setSelection(0)
                spinner_status?.setSelection(0)
                spinner_date?.setSelection(0)

                input_fromdate?.setText(resources.getString(R.string.fromDate))
                input_todate?.setText(resources.getString(R.string.toDate))

                getAllPos(loginManager?.getCompanyID(), "", "", "1918/01/01",currentDate, "", "")

            }
        }    }

    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)
        input_fromdate?.setText(resources.getString(R.string.fromDate))
        input_todate?.setText(resources.getString(R.string.toDate))
        currentDate = formattedDate
    }

    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth )
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }
    fun loadSpinnerValues()
    {

        projectNameArray?.add("Any Project")
        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.spinner_item_style, projectNameArray)

        spinner_projects?.adapter = spinnerProjectAdapter
        for(i in 0..UserDataManager?.allProjectsList!!.size - 1)
        {
            if(UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false)
            {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }


        spinner_projects?.adapter = spinnerProjectAdapter





        var spinnerStatusAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.po_status_array))

        spinner_status?.adapter = spinnerStatusAdapter



        var spinnerDateAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.array_datefilter))

        spinner_date?.adapter = spinnerDateAdapter


    }

}
