package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 2/26/2018.
 */
class OpportunitiesDataManager
{
    companion object {
        public var opportunitiesList: ArrayList<OpportunitiesDetailAdapter>? = null
        public var stagesList: ArrayList<OpportunitiesStagesAdapter>? = null
        public var typesList: ArrayList<OpportunitiesTypeAdapter>? = null



        init {
            opportunitiesList = ArrayList<OpportunitiesDetailAdapter>()
            stagesList = ArrayList<OpportunitiesStagesAdapter>()
            typesList = ArrayList<OpportunitiesTypeAdapter>()
        }



        fun insertOpportunites(contactId: Any?,firstname: Any?,lastname: Any?, email: Any?,
                        phone: Any?, mobile: Any?, address: Any?,creationDate: Any?,
                        city: Any?,state: Any?,countryId: Any?,details: Any?,
                        companyId: Any?,dateUpdated: Any?,createdById: Any?,
                        updatedBy: Any?,contactTypeId: Any?, companyName: Any?, categoryId: Any?,
                        zipCode: Any?, isHidden: Any?,oppoertunityID: Any?,leadID: Any?,
                               stageID: Any?, description: Any?, typeID: Any?, budget: Any?,
                               OrderExpectedDate: Any?, ConvertedBy: Any?,
                                convertedDate: Any?,AssignedTo: Any?, ProposalWriterId: Any?,
                              Competitor: Any?,ContactThroughId: Any?,Stage: Any?,
                               Type: Any?,convertedByUserName: Any?,ContactThroughUserName: Any?,
                               ProposalWriterUserName: Any?,AssignedToUserName: Any?,
                                ContactCategory:  Any?,CountryName: Any?)
        {
            opportunitiesList?.add(OpportunitiesDetailAdapter(contactId,firstname,lastname,email,
                    phone, mobile,address,creationDate, city,state, countryId,details,
                    companyId,dateUpdated, createdById,
                    updatedBy, contactTypeId,companyName,categoryId,
                    zipCode, isHidden,oppoertunityID,leadID,
            stageID, description, typeID, budget,
            OrderExpectedDate, ConvertedBy,
            convertedDate,AssignedTo, ProposalWriterId,
            Competitor,ContactThroughId,Stage,
            Type,convertedByUserName,ContactThroughUserName,
            ProposalWriterUserName,AssignedToUserName,
            ContactCategory,CountryName))
        }

        fun insertTypes(typeId: Any?, type: Any?)
        {
            typesList?.add(OpportunitiesTypeAdapter(typeId,type))
        }

        fun insertStages(stageId: Any?, stage: Any?)
        {
            stagesList?.add(OpportunitiesStagesAdapter(stageId,stage))
        }

    }
}