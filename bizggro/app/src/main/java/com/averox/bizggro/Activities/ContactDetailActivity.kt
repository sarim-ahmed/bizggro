package com.averox.bizggro.Activities

import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.averox.bizggro.HelperClasses.AlertManager
import com.averox.bizggro.HelperClasses.Constants
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.ContactsDataManager
import com.averox.bizggro.UserManagement.LeadsDataManager
import com.averox.bizggro.UserManagement.LoginManager
import java.util.ArrayList

class ContactDetailActivity : AppCompatActivity(), View.OnClickListener  {

    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var button_basic: ImageButton? = null
    private var button_address: ImageButton? = null
    private var layout_basicInfo: LinearLayout? = null
    private var layout_addressInfo: LinearLayout? = null
    private var button_create: Button? = null
    private var textview_firstname: TextView? = null
    private var textview_lastname: TextView? = null
    private var textview_mobile: TextView? = null
    private var textview_email: TextView? = null
    private var textview_phone: TextView? = null
    private var textview_company: TextView? = null
    private var textview_category: TextView? = null
    private var textview_detail: TextView? = null
    private var textview_street: TextView? = null
    private var textview_city: TextView? = null
    private var textview_state: TextView? = null
    private var textview_zipCode: TextView? = null
    private var textview_country: TextView? = null
    private var firstname: Any? = ""
    private var lastname: Any? = ""
    private var mobile: Any? = ""
    private var email: Any? = ""
    private var phone: Any? = ""
    private var company: Any? = ""
    private var detail: Any? = ""
    private var street: Any? = ""
    private var city: Any? = ""
    private var state: Any? =""
    private var zipCode: Any? = ""
    private var category: Any? = null
    private var country: Any? = null

    private var categoryID: Any? = ""
    private var countryID: Any? = ""
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var progressDialog: ProgressDialog? = null
    private var position: Int? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_detail)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        textview_firstname = findViewById(R.id.textview_firstname)
        textview_lastname = findViewById(R.id.textview_lastname)
        textview_detail = findViewById(R.id.textview_detail)
        textview_mobile = findViewById(R.id.textview_mobile)
        textview_email = findViewById(R.id.textview_email)
        textview_phone = findViewById(R.id.textview_phone)
        textview_company = findViewById(R.id.textview_company)
        textview_category = findViewById(R.id.textview_category)
        textview_street = findViewById(R.id.textview_street)
        textview_city = findViewById(R.id.textview_city)
        textview_state = findViewById(R.id.textview_state)
        textview_zipCode = findViewById(R.id.textview_zipCode)
        textview_country = findViewById(R.id.textview_country)

        button_address = findViewById(R.id.button_address)
        button_address?.setOnClickListener(this)
        button_basic = findViewById(R.id.button_basic)
        button_basic?.setOnClickListener(this)
        button_create = findViewById(R.id.button_create)
        button_create?.setOnClickListener(this)

        layout_addressInfo =  findViewById(R.id.layout_addressInfo)
        layout_basicInfo = findViewById(R.id.layout_basicInfo)



        loginManager = LoginManager(Constants.context!!)

        showBasicData()

        loadData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {

            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)



    }


    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_basic->
            {
                showBasicData()

            }
            R.id.button_address->
            {
                showAddressData()


            }

        }
    }

    fun showBasicData()
    {
        layout_basicInfo?.visibility = View.VISIBLE
        layout_addressInfo?.visibility = View.GONE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info_pressed))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info))

        button_basic?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
        button_address?.setBackgroundDrawable(null)
    }
    fun showAddressData()
    {
        layout_basicInfo?.visibility = View.GONE
        layout_addressInfo?.visibility = View.VISIBLE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info_pressed))

        button_basic?.setBackgroundDrawable(null)
        button_address?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
    }


    fun loadData()
    {

        var intent = intent
        var extras = intent.extras
        position = extras?.getInt("CONTACT_POSITION")


        firstname = ContactsDataManager?.contactDetailList?.get(position!!)?.firstname
        lastname = ContactsDataManager?.contactDetailList?.get(position!!)?.lastname
        mobile = ContactsDataManager?.contactDetailList?.get(position!!)?.mobile
        phone = ContactsDataManager?.contactDetailList?.get(position!!)?.phone
        email = ContactsDataManager?.contactDetailList?.get(position!!)?.email
        company = ContactsDataManager?.contactDetailList?.get(position!!)?.companyName
        detail = ContactsDataManager?.contactDetailList?.get(position!!)?.details
        street = ContactsDataManager?.contactDetailList?.get(position!!)?.address
        city = ContactsDataManager?.contactDetailList?.get(position!!)?.city
        state = ContactsDataManager?.contactDetailList?.get(position!!)?.state
        zipCode = ContactsDataManager?.contactDetailList?.get(position!!)?.zipCode
        category = ContactsDataManager?.contactDetailList?.get(position!!)?.Category
        country = ContactsDataManager?.contactDetailList?.get(position!!)?.countryName


        textview_firstname?.text =  firstname.toString()
        textview_lastname?.text = lastname.toString()
        textview_mobile?.text = mobile.toString()
        textview_phone?.text = phone.toString()
        textview_email?.text = email.toString()
        textview_company?.text = company.toString()
        textview_detail?.text = detail.toString()
        textview_street?.text = street.toString()
        textview_city?.text = city.toString()
        textview_state?.text = state.toString()
        textview_zipCode?.text = zipCode.toString()
        textview_category?.text = category.toString()
        textview_country?.text = country.toString()



    }

}
