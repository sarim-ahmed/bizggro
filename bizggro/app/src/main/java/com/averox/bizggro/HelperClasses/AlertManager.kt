package com.averox.bizggro.HelperClasses

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AlertDialog
import java.util.*

/**
 * Created by Sarim on 11/29/2017.
 */
class AlertManager(alertMessage :String,time: Long, context: Context)  {



    init {


        val mHandler = Handler(Looper.getMainLooper())
        mHandler.post {
            // Your UI updates here
            val dialog = AlertDialog.Builder(
                    context)
            dialog.setMessage(alertMessage)
            dialog.setCancelable(true)
            val dlg = dialog.create()
            dlg.show()
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    dlg.dismiss()
                    timer.cancel() //this will cancel the timer of the system
                }
            }, time)
        }
    }

}