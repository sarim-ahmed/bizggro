package com.averox.bizggro.Activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Adapters.SignatureRequestAdapter
import com.averox.bizggro.Adapters.TaskAssignedToMeAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.DocSignDataManager
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.TasksDataManager
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject
import java.util.*

class SignatureRequest : AppCompatActivity() {


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var title: String? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var item_position: Int? = null

    private var docs_view: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature_request)


        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")

        toolbar_title = findViewById(R.id.toolbar_title)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        loader = findViewById(R.id.loader)

        loginManager = LoginManager(this@SignatureRequest)

        docs_view = findViewById(R.id.docs_view)

        getSignRequestList()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {

        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }

    fun getSignRequestList()
    {


        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        DocSignDataManager?.signatureRequestList?.clear()
        var adapter = SignatureRequestAdapter(this, DocSignDataManager?.signatureRequestList!!)
        docs_view?.layoutManager = LinearLayoutManager(this)
        docs_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()
        requestParameters.add(BasicNameValuePair(Parameters.Email,loginManager?.getEmail().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))

        //requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))

        /*       requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.CategoryID,categoryID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.TransactionTypeID,transactionTypeID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.Description,description.toString()))
       */

        UrlBuilder?.setUrl(Module.documentSign, RequestCode.getrequests,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var useremail = result.getString(JsonKeys.variables.useremail)
                        var id = result.getString(JsonKeys.variables.Id)
                        var docId = result.getInt(JsonKeys.variables.DocId)
                        var docTitle = result.getString(JsonKeys.variables.DocTitle)
                        var originalDocId = result.getInt(JsonKeys.variables.originalDocId)
                        var previousDocId = result.getInt(JsonKeys.variables.previousDocId)
                        var dateSigned = result.getString(JsonKeys.variables.dateSigned)
                        var documentUrl =  result.getString(JsonKeys.variables.documentURL)





                       DocSignDataManager?.insertSignatureRequests(useremail,id,docId,docTitle,originalDocId,previousDocId,dateSigned,documentUrl)

                        var adapter = SignatureRequestAdapter(this, DocSignDataManager?.signatureRequestList!!)
                        docs_view?.layoutManager = LinearLayoutManager(this)
                        docs_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }

                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                }

                finally
                {

                }

            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)
    }

    internal inner class SignatureRequestAdapter(private val context: Context, arraylist: ArrayList<com.averox.bizggro.Adapters.SignatureRequestAdapter>) : RecyclerView.Adapter<SignatureRequestAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<com.averox.bizggro.Adapters.SignatureRequestAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.signature_request_items, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/
            var sr_number = position+1

            holder.name_view?.setText(""+list_items.docTitle)
            holder.sentBy_view?.setText(""+list_items.useremail)
            holder.date_view?.setText(""+list_items.dateSigned)

/*
            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())*/

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<com.averox.bizggro.Adapters.SignatureRequestAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var name_view: TextView? = null
            var sentBy_view: TextView? = null
            var date_view: TextView? = null


            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                name_view = itemView.findViewById(R.id.name_view)
                sentBy_view = itemView.findViewById(R.id.sentBy_view)
                date_view =  itemView.findViewById(R.id.date_view)


            }

            override fun onClick(v: View) {

                val itemPosition = docs_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                item_position =  itemPosition

                var intent = Intent(this@SignatureRequest,CompleteSignature::class.java)

                //intent.putExtra("TITLE",resources.getString(R.string.assignedToMe))
                intent.putExtra("ITEM_POSITION",itemPosition)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

                //registerForContextMenu(tasks_view)
                //tasks_view?.showContextMenu()

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

}
