package com.averox.bizggro.UserManagement

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.averox.bizggro.Activities.LoginActivity
import com.averox.bizggro.HelperClasses.Constants

/**
 * Created by Sarim on 11/28/2017.
 */

public class LoginManager(var context: Context)
{

    private var pref: SharedPreferences? = null
    internal var editor: SharedPreferences.Editor? = null
    internal var PRIVATE_MODE = 0
    internal var _context: Context? = null
    val isLoggedIn: Boolean
        get() = pref!!.getBoolean(IS_LOGIN, false)

    init {
        this._context = Constants.context
        pref = _context?.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref?.edit()
    }





    companion object {

        // Sharedpref file name
        private val PREF_NAME = "UserLoginPref"

        private val IS_LOGIN = "IsLoggedIn"

        // All Shared Preferences Keys

        val KEY_FIRST_NAME = "first_name"

        val KEY_LAST_NAME = "last_name"

        val KEY_EMAIL = "email"

        val KEY_USER_TYPE = "user_type"

        val KEY_COMPANY_ID = "company_id"

        val KEY_COMPANY_NAME = "company_name"

        val KEY_PASSWORD = "password"

        val KEY_USER_ID = "user_id"

    }

    //this function creates login session

    fun createLoginSession(email: String?, password: String?, user_id: Int?, first_name: String?, last_name: String?, user_type: Int?, company_name: String?, company_id: Int?) {

        // Storing login value as TRUE

        // Storing data in sharedpref

        editor?.putString(KEY_EMAIL, email)
        if (user_id != null) {
            editor?.putInt(KEY_USER_ID, user_id)
        }
        editor?.putString(KEY_FIRST_NAME, first_name)
        editor?.putString(KEY_LAST_NAME, last_name)
        editor?.putBoolean(IS_LOGIN, true)
        if (user_type != null) {
            editor?.putInt(KEY_USER_TYPE, user_type)
        }
        editor?.putString(KEY_COMPANY_NAME, company_name)
        if (company_id != null) {
            editor?.putInt(KEY_COMPANY_ID, company_id)
        }

        // commit changes

        editor!!.commit()

    }

    fun savePass(password: String?)

    {

        editor!!.putString(KEY_PASSWORD, password)
        editor!!.commit()

    }

    //this function checks the login session is already created or not

    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {

            // user is not logged in redirect him to LoginActivity Activity
            val i = Intent(Constants.context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            // Starting LoginActivity Activity
            Constants.context?.startActivity(i)

        }


    }


     fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor!!.clear()
        editor!!.commit()

        // After logout redirect user to Loing Activity
        val i = Intent(Constants.context, LoginActivity::class.java)
        // Closing all the Activities
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)


        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)


        // Starting LoginActivity Activity
        Constants.context?.startActivity(i)

    }

    // Get Login State
    fun isLogedIn(): Boolean {
        return pref!!.getBoolean(IS_LOGIN, false)
    }


    fun getFirstName(): String? {

        return pref?.getString(KEY_FIRST_NAME, null)
    }
    fun getLastName(): String? {

        return pref?.getString(KEY_LAST_NAME, null)
    }

    fun getFullName(): String? {

        return pref?.getString(KEY_FIRST_NAME, null)+" "+pref?.getString(KEY_LAST_NAME, null)
    }

    fun getUserID(): Int? {
        return pref?.getInt(KEY_USER_ID, 0)

    }
    fun getUserType(): Int? {

        return pref?.getInt(KEY_USER_TYPE, 0)

    }

    fun getEmail(): String?
    {
        return pref?.getString(KEY_EMAIL, null)
    }


    fun getPassword(): String? {
        return pref?.getString(KEY_PASSWORD,  null)

    }



    fun getCompanyName(): String?{
        return pref!!.getString(KEY_COMPANY_NAME,  null)

    }
    fun getCompanyID(): Int?
    {
        return pref?.getInt(KEY_COMPANY_ID,  0)

    }

}