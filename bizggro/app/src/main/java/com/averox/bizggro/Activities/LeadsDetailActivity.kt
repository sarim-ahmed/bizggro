package com.averox.bizggro.Activities

import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.averox.bizggro.HelperClasses.Constants
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.ContactsDataManager
import com.averox.bizggro.UserManagement.LeadsDataManager
import com.averox.bizggro.UserManagement.LoginManager
import org.w3c.dom.Text

class LeadsDetailActivity : AppCompatActivity(), View.OnClickListener  {


    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var button_basic: ImageButton? = null
    private var button_address: ImageButton? = null
    private var layout_basicInfo: LinearLayout? = null
    private var layout_addressInfo: LinearLayout? = null
    private var button_create: Button? = null
    private var textview_firstname: TextView? = null
    private var textview_lastname: TextView? = null
    private var textview_mobile: TextView? = null
    private var textview_email: TextView? = null
    private var textview_phone: TextView? = null
    private var textview_company: TextView? = null
    private var textview_category: TextView? = null
    private var textview_detail: TextView? = null
    private var textview_street: TextView? = null
    private var textview_city: TextView? = null
    private var textview_state: TextView? = null
    private var textview_zipCode: TextView? = null
    private var textview_country: TextView? = null
    private var textview_owner: TextView? = null
    private var textview_status: TextView? = null
    private var textview_dateConverted: TextView? = null
    private var textview_convertedBy: TextView? = null
    private var textview_noOfEmployees: TextView? = null
    private var textview_source: TextView? = null
    private var textview_annualRevenue: TextView? = null
    private var textview_rating: TextView? = null
    private var textview_title: TextView? = null
    private var firstname: Any? = ""
    private var lastname: Any? = ""
    private var mobile: Any? = ""
    private var email: Any? = ""
    private var phone: Any? = ""
    private var company: Any? = ""
    private var detail: Any? = ""
    private var street: Any? = ""
    private var city: Any? = ""
    private var state: Any? =""
    private var zipCode: Any? = ""
    private var category: Any? = ""
    private var country: Any? = ""
    private var owner: Any? = ""
    private var status: Any? = ""
    private var dateConverted: Any? = ""
    private var convertedBy: Any? = ""
    private var noOfEmployees: Any? = ""
    private var source: Any? = ""
    private var annualRevenue: Any? = ""
    private var rating: Any? = ""
    private var title: Any? = ""
    private var button_lead_info: ImageButton? = null
    private var layout_leadInfo: LinearLayout? = null

    private var categoryID: Any? = ""
    private var countryID: Any? = ""
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var progressDialog: ProgressDialog? = null
    private var position: Int? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leads_detail)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        textview_firstname = findViewById(R.id.textview_firstname)
        textview_lastname = findViewById(R.id.textview_lastname)
        textview_detail = findViewById(R.id.textview_detail)
        textview_mobile = findViewById(R.id.textview_mobile)
        textview_email = findViewById(R.id.textview_email)
        textview_phone = findViewById(R.id.textview_phone)
        textview_company = findViewById(R.id.textview_company)
        textview_category = findViewById(R.id.textview_category)
        textview_street = findViewById(R.id.textview_street)
        textview_city = findViewById(R.id.textview_city)
        textview_state = findViewById(R.id.textview_state)
        textview_zipCode = findViewById(R.id.textview_zipCode)
        textview_country = findViewById(R.id.textview_country)
        textview_owner = findViewById(R.id.textview_owner)
        textview_status = findViewById(R.id.textview_status)
        textview_dateConverted = findViewById(R.id.textview_dateConverted)
        textview_convertedBy = findViewById(R.id.textview_convertedBy)
        textview_noOfEmployees = findViewById(R.id.textview_noOfEmployees)
        textview_source = findViewById(R.id.textview_source)
        textview_annualRevenue = findViewById(R.id.textview_annualRevenue)
        textview_rating = findViewById(R.id.textview_rating)
        textview_title = findViewById(R.id.textview_title)

        button_address = findViewById(R.id.button_address)
        button_address?.setOnClickListener(this)
        button_basic = findViewById(R.id.button_basic)
        button_basic?.setOnClickListener(this)
        button_create = findViewById(R.id.button_create)
        button_create?.setOnClickListener(this)
        button_lead_info = findViewById(R.id.button_lead_info)
        button_lead_info?.setOnClickListener(this)

        layout_addressInfo =  findViewById(R.id.layout_addressInfo)
        layout_basicInfo = findViewById(R.id.layout_basicInfo)
        layout_leadInfo = findViewById(R.id.layout_leadInfo)



        loginManager = LoginManager(Constants.context!!)

        showBasicData()

        loadData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {

            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)



    }


    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_basic->
            {
                showBasicData()

            }
            R.id.button_address->
            {
                showAddressData()


            }
            R.id.button_lead_info->
            {
                showLeadDate()
            }

        }
    }

    fun showBasicData()
    {
        layout_basicInfo?.visibility = View.VISIBLE
        layout_addressInfo?.visibility = View.GONE
        layout_leadInfo?.visibility = View.GONE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info_pressed))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info))
        button_lead_info?.setImageDrawable(resources.getDrawable(R.drawable.ic_lead_info))


        button_basic?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
        button_address?.setBackgroundDrawable(null)
        button_lead_info?.setBackgroundDrawable(null)

    }
    fun showAddressData()
    {
        layout_basicInfo?.visibility = View.GONE
        layout_addressInfo?.visibility = View.VISIBLE
        layout_leadInfo?.visibility = View.GONE

        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info_pressed))
        button_lead_info?.setImageDrawable(resources.getDrawable(R.drawable.ic_lead_info))

        button_basic?.setBackgroundDrawable(null)
        button_address?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
        button_lead_info?.setBackgroundDrawable(null)

    }

    fun showLeadDate()
    {
        layout_basicInfo?.visibility = View.GONE
        layout_addressInfo?.visibility = View.GONE
        layout_leadInfo?.visibility = View.VISIBLE


        button_basic?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_general_info))
        button_address?.setImageDrawable(resources.getDrawable(R.drawable.ic_contact_address_info))
        button_lead_info?.setImageDrawable(resources.getDrawable(R.drawable.ic_lead_info_pressed))

        button_lead_info?.setBackgroundDrawable(resources.getDrawable(R.drawable.border_bottom))
        button_address?.setBackgroundDrawable(null)
        button_basic?.setBackgroundDrawable(null)

    }


    fun loadData()
    {

        var intent = intent
        var extras = intent.extras
        position = extras?.getInt("LEAD_POSITION")


        firstname = LeadsDataManager.leadsList?.get(position!!)?.firstname
        lastname = LeadsDataManager.leadsList?.get(position!!)?.lastname
        mobile = LeadsDataManager.leadsList?.get(position!!)?.mobile
        phone = LeadsDataManager.leadsList?.get(position!!)?.phone
        email = LeadsDataManager.leadsList?.get(position!!)?.email
        company = LeadsDataManager.leadsList?.get(position!!)?.companyName
        detail = LeadsDataManager.leadsList?.get(position!!)?.details
        street = LeadsDataManager.leadsList?.get(position!!)?.address
        city = LeadsDataManager.leadsList?.get(position!!)?.city
        state = LeadsDataManager.leadsList?.get(position!!)?.state
        zipCode = LeadsDataManager.leadsList?.get(position!!)?.zipCode
        category = LeadsDataManager.leadsList?.get(position!!)?.category
        country = LeadsDataManager.leadsList?.get(position!!)?.countryName
        owner = LeadsDataManager.leadsList?.get(position!!)?.ownerUserName
        status = LeadsDataManager.leadsList?.get(position!!)?.status
        dateConverted = LeadsDataManager.leadsList?.get(position!!)?.convertedDate
        convertedBy = LeadsDataManager.leadsList?.get(position!!)?.convertedByUserName
        noOfEmployees = LeadsDataManager.leadsList?.get(position!!)?.noOfEmployees
        source = LeadsDataManager.leadsList?.get(position!!)?.sourceName
        annualRevenue = LeadsDataManager.leadsList?.get(position!!)?.annualRevenue
        rating = LeadsDataManager.leadsList?.get(position!!)?.rating
        title = LeadsDataManager.leadsList?.get(position!!)?.leadTitle



        textview_firstname?.text =  firstname.toString()
        textview_lastname?.text = lastname.toString()
        textview_mobile?.text = mobile.toString()
        textview_phone?.text = phone.toString()
        textview_email?.text = email.toString()
        textview_company?.text = company.toString()
        textview_detail?.text = detail.toString()
        textview_street?.text = street.toString()
        textview_city?.text = city.toString()
        textview_state?.text = state.toString()
        textview_zipCode?.text = zipCode.toString()
        textview_owner?.setText(owner.toString())
        textview_status?.text = status.toString()
        textview_dateConverted?.text = dateConverted.toString()
        textview_convertedBy?.text = convertedBy.toString()
        textview_noOfEmployees?.text = noOfEmployees.toString()
        textview_source?.text = source.toString()
        textview_annualRevenue?.text = annualRevenue.toString()
        textview_rating?.text = rating.toString()
        textview_title?.text = title.toString()
        textview_category?.text = category.toString()
        textview_country?.text = country.toString()




    }
}
