package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Adapters.TaskAssignedByMeAdapter
import com.averox.bizggro.Adapters.TaskAssignedToMeAdapter
import com.averox.bizggro.Adapters.TransactionsAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.TasksDataManager
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.activity_all_transactions.*
import kotlinx.android.synthetic.main.activity_pos.*
import kotlinx.android.synthetic.main.activity_tasks_list.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

    class TasksList : AppCompatActivity(), View.OnClickListener {


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var title: String? = null
    private var tasks_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var item_position: Int? = null


    private var spinner_project: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var spinner_status: Spinner? = null
    private var statusNameArray: ArrayList<String>? = null
    private var spinner_group: Spinner? = null
    private var groupNameArray: ArrayList<String>? = null
    private var spinner_priority: Spinner? = null
    private var priorityNameArray: ArrayList<String>? = null

    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var description: Any = ""
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var applyDateFilter: Any = ""
    private var spinner_date: Spinner? = null
        private var currentDate: String? = null
        private var input_description: EditText? = null

        private var projectID: Any? = null
        private var statusID: Any? = null
        private var priorityID: Any? = null

        private var applyFilters: Boolean = false

        private var button_search: ImageButton? = null
        private var button_reset: ImageButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tasks_list)

        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")

        toolbar_title = findViewById(R.id.toolbar_title)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        var intent = intent
        var extras = intent.extras

        title = extras?.getString("TOOLBAR_TITLE")

        toolbar_title?.setText(title)

        tasks_view = findViewById(R.id.tasks_view)

        loginManager = LoginManager(Constants.context!!)


        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE
        input_description = findViewById(R.id.input_description)


        if(title!!.equals(resources.getString(R.string.assignedToMe)))
        {
            getAssignedToMe("","","","","","false","")

        }
        else if(title!!.equals(resources.getString(R.string.assignedByMe)))
        {
            getAssignedByMe("","","","","","false","")
        }

        input_fromDate = findViewById(R.id.input_fromdate)
        input_toDate = findViewById(R.id.input_todate)
        input_fromDate?.setOnClickListener(this)
        input_toDate?.setOnClickListener(this)

        projectNameArray = ArrayList()
        spinner_project = findViewById(R.id.spinner_project)
        statusNameArray = ArrayList()
        spinner_status = findViewById(R.id.spinner_status)
        groupNameArray = ArrayList()
        spinner_group = findViewById(R.id.spinner_group)
        spinner_priority = findViewById(R.id.spinner_priority)
        priorityNameArray = ArrayList()
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)


        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        loadSpinnerValues()

        setCurrentDate()



        spinner_date?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position.equals(0))
                {
                    layout_date?.visibility = View.GONE
                    applyFilters = false
                    applyDateFilter = 0

                }
                else if(position.equals(1))

                {

                    layout_date?.visibility = View.VISIBLE
                    applyFilters = true
                    applyDateFilter = 1

                }

            }

        }

        spinner_project?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    projectID = ""
                }
                else
                {
                    projectID = UserDataManager?.allProjectsList?.get(position -1)?.getProjectID()
                }
            }

        }

        spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    statusID = ""
                }
                else
                {
                    statusID = TasksDataManager?.tasksStatus?.get(position -1 )?.StatusID
                }
            }

        }

        spinner_priority?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    priorityID = ""
                }
                else
                {
                    priorityID = TasksDataManager?.tasksPriorities?.get(position -1 )?.priorityID
                }
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {

        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }

    fun getAssignedToMe( projectID: Any?, priorityID: Any?,statusID: Any?,fromDate: Any?,toDate: Any?,applyFilters: Any?,description: Any?)
    {
        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        TasksDataManager.assignedToMe?.clear()
        var adapter = AssignedToMeAdapter(this, TasksDataManager!!.assignedToMe!!)
        tasks_view?.layoutManager = LinearLayoutManager(this)
        tasks_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.UserID,loginManager?.getUserID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,description.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.PriorityId,priorityID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.StatusID,statusID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyFilters,applyFilters.toString()))

        //requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))
        //requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))

        /*       requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.CategoryID,categoryID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.TransactionTypeID,transactionTypeID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.Description,description.toString()))
       */

        UrlBuilder?.setUrl(Module.tasks, RequestCode.gettasksassignedtome,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var taskID = result.getInt(JsonKeys.variables.TaskID)
                        var title = result.getString(JsonKeys.variables.Title)
                        var statusID = result.getInt(JsonKeys.variables.StatusID)
                        var priority = result.getString(JsonKeys.variables.Priority)
                        var userID = result.getInt(JsonKeys.variables.UserID)
                        var projectID = result.getInt(JsonKeys.variables.ProjectID)
                        var startDate = result.getString(JsonKeys.variables.StartDate)
                        var endDate =  result.getString(JsonKeys.variables.KEY_ENDDATE)
                        var attachment = result.getString(JsonKeys.variables.Attachment)
                        var taskDesc = result.getString(JsonKeys.variables.TaskDesc)
                        var taskInchargeID  = result.getInt(JsonKeys.variables.TaskInchargeID)
                        var flag = result.getString(JsonKeys.variables.Flag)
                        var sendDate = result.getString(JsonKeys.variables.SendDate)
                        var moduleID = result.getInt(JsonKeys.variables.ModuleID)
                        var referenceID =  result.getInt(JsonKeys.variables.ReferenceID)
                        var pendingDays = result.getInt(JsonKeys.variables.PendingDays)
                        var pendingDate = result.getString(JsonKeys.variables.PendingDate)
                        var completedDate = result.getString(JsonKeys.variables.CompletedDate)
                        var companyID = result.getInt(JsonKeys.variables.CompanyID)
                        var customerId = result.getInt(JsonKeys.variables.CustomerId)
                        var userName = result.getString(JsonKeys.variables.UserName)
                        var status = result.getString(JsonKeys.variables.Status)
                        var taskPriority = result.getString(JsonKeys.variables.TaskPriority)
                        var taskInchargeName = result.getString(JsonKeys.variables.TaskInchargeName)
                        var name = result.getString(JsonKeys.variables.Name)
                        var projectName = result.getString(JsonKeys.variables.ProjectName)
                        var daysLeft = result.getString(JsonKeys.variables.DaysLeft)
                        var documentUrl = result.getString(JsonKeys.variables.DocumentURL)



                        TasksDataManager?.insertAssignedToMe(taskID,title,statusID,priority, userID,
                                projectID,startDate, endDate,attachment,
                                taskDesc,taskInchargeID, flag, sendDate,
                                moduleID, referenceID,pendingDays, pendingDate,
                                completedDate,companyID, customerId, userName,
                                status, taskPriority,taskInchargeName,name,projectName,
                                daysLeft, documentUrl)


                        var adapter = AssignedToMeAdapter(this, TasksDataManager!!.assignedToMe!!)
                        tasks_view?.layoutManager = LinearLayoutManager(this)
                        tasks_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE


                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                }

                finally
                {

                }

            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)
    }

    fun getAssignedByMe( projectID: Any?,  priorityID: Any?,statusID: Any?, fromDate: Any?,toDate: Any?,applyFilters: Any?,description: Any?)
    {


        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        TasksDataManager.assignedByMe?.clear()
        var adapter = AssignedByMeAdapter(this, TasksDataManager!!.assignedByMe!!)
        tasks_view?.layoutManager = LinearLayoutManager(this)
        tasks_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()
        requestParameters.add(BasicNameValuePair(Parameters.UserID,loginManager?.getUserID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,description.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.PriorityId,priorityID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.StatusID,statusID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyFilters,applyFilters.toString()))
        //requestParameters.add(BasicNameValuePair(Parameters.CompanyID,loginManager?.getCompanyID().toString()))

        /*       requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.CategoryID,categoryID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.TransactionTypeID,transactionTypeID.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
               requestParameters.add(BasicNameValuePair(Parameters.Description,description.toString()))
       */

        UrlBuilder?.setUrl(Module.tasks, RequestCode.gettasksassignedbyme,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)

                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var taskID = result.getInt(JsonKeys.variables.TaskID)
                        var title = result.getString(JsonKeys.variables.Title)
                        var statusID = result.getInt(JsonKeys.variables.StatusID)
                        var priority = result.getString(JsonKeys.variables.Priority)
                        var userID = result.getInt(JsonKeys.variables.UserID)
                        var projectID = result.getInt(JsonKeys.variables.ProjectID)
                        var startDate = result.getString(JsonKeys.variables.StartDate)
                        var endDate =  result.getString(JsonKeys.variables.KEY_ENDDATE)
                        var attachment = result.getString(JsonKeys.variables.Attachment)
                        var taskDesc = result.getString(JsonKeys.variables.TaskDesc)
                        var taskInchargeID  = result.getInt(JsonKeys.variables.TaskInchargeID)
                        var flag = result.getString(JsonKeys.variables.Flag)
                        var sendDate = result.getString(JsonKeys.variables.SendDate)
                        var moduleID = result.getInt(JsonKeys.variables.ModuleID)
                        var referenceID =  result.getInt(JsonKeys.variables.ReferenceID)
                        var pendingDays = result.getInt(JsonKeys.variables.PendingDays)
                        var pendingDate = result.getString(JsonKeys.variables.PendingDate)
                        var completedDate = result.getString(JsonKeys.variables.CompletedDate)
                        var companyID = result.getInt(JsonKeys.variables.CompanyID)
                        var customerId = result.getInt(JsonKeys.variables.CustomerId)
                        var userName = result.getString(JsonKeys.variables.UserName)
                        var status = result.getString(JsonKeys.variables.Status)
                        var taskPriority = result.getString(JsonKeys.variables.TaskPriority)
                        var taskInchargeName = result.getString(JsonKeys.variables.TaskInchargeName)
                        var name = result.getString(JsonKeys.variables.Name)
                        var projectName = result.getString(JsonKeys.variables.ProjectName)
                        var daysLeft = result.getString(JsonKeys.variables.DaysLeft)
                        var documentUrl = result.getString(JsonKeys.variables.DocumentURL)




                        TasksDataManager?.insertAssignedByMe(taskID,title,statusID,priority, userID,
                                projectID,startDate, endDate,attachment,
                                taskDesc,taskInchargeID, flag, sendDate,
                                moduleID, referenceID,pendingDays, pendingDate,
                                completedDate,companyID, customerId, userName,
                                status, taskPriority,taskInchargeName,name,projectName,
                                daysLeft, documentUrl)


                        var adapter = AssignedByMeAdapter(this, TasksDataManager!!.assignedByMe!!)
                        tasks_view?.layoutManager = LinearLayoutManager(this)
                        tasks_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                    
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                }

                finally
                {

                }

            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)
    }

    internal inner class AssignedToMeAdapter(private val context: Context, arraylist: ArrayList<TaskAssignedToMeAdapter>) : RecyclerView.Adapter<AssignedToMeAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<TaskAssignedToMeAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.task_list_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/
            var sr_number = position+1

            holder.title_view?.setText(""+list_items.Title)
            holder.inchargeName_view?.setText(""+list_items.TaskInchargeName)
            holder.status_view?.setText(""+list_items.Status)

/*
            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())*/

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var title_view: TextView? = null
            var inchargeName_view: TextView? = null
            var status_view: TextView? = null


            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                title_view = itemView.findViewById(R.id.title_view)
                inchargeName_view = itemView.findViewById(R.id.inchargeName_view)
                status_view =  itemView.findViewById(R.id.status_view)


            }

            override fun onClick(v: View) {

                val itemPosition = tasks_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                item_position =  itemPosition

                var intent = Intent(this@TasksList,ViewTask::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.assignedToMe))
                intent.putExtra("ITEM_POSITION",itemPosition)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

                //registerForContextMenu(tasks_view)
                //tasks_view?.showContextMenu()

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }


    internal inner class AssignedByMeAdapter(private val context: Context, arraylist: ArrayList<TaskAssignedByMeAdapter>) : RecyclerView.Adapter<AssignedByMeAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<TaskAssignedByMeAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.task_list_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/
            var sr_number = position+1

            holder.title_view?.setText(""+list_items.Title)
            holder.inchargeName_view?.setText(""+list_items.TaskInchargeName)
            holder.status_view?.setText(""+list_items.Status)

/*
            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())*/

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var title_view: TextView? = null
            var inchargeName_view: TextView? = null
            var status_view: TextView? = null


            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                title_view = itemView.findViewById(R.id.title_view)
                inchargeName_view = itemView.findViewById(R.id.inchargeName_view)
                status_view =  itemView.findViewById(R.id.status_view)


            }

            override fun onClick(v: View) {

                val itemPosition = tasks_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                item_position =  itemPosition

                var intent = Intent(this@TasksList,ViewTask::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.assignedByMe))
                intent.putExtra("ITEM_POSITION",itemPosition)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)


             /*   registerForContextMenu(tasks_view)
                tasks_view?.showContextMenu()*/

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }


    fun loadSpinnerValues()
    {
        projectNameArray?.add("Any Project")
        for (i in 0..UserDataManager?.allProjectsList!!.size - 1) {
            if (UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false) {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }

        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.spinner_item_style, projectNameArray)

        spinner_project?.adapter = spinnerProjectAdapter



        statusNameArray?.add("Any Status")
        for (i in 0..TasksDataManager?.tasksStatus!!.size - 1) {
            if (TasksDataManager?.tasksStatus?.get(i)?.Status?.equals(null) == false) {
                statusNameArray?.add(TasksDataManager?.tasksStatus?.get(i)?.Status.toString()!!)

            }

        }

        var spinnerStatusAdapter = ArrayAdapter(this, R.layout.spinner_item_style, statusNameArray)

        spinner_status?.adapter = spinnerStatusAdapter




        groupNameArray?.add("Any Group")
        for (i in 0..UserDataManager?.userGroupsList!!.size - 1) {
            if (UserDataManager?.userGroupsList?.get(i)?.groupName?.equals(null) == false) {
                groupNameArray?.add(UserDataManager?.userGroupsList?.get(i)?.groupName.toString()!!)

                Log.d("response","group names: "+UserDataManager?.userGroupsList?.get(i)?.groupName.toString())

            }

        }

        var spinnerGroupAdapter = ArrayAdapter(this, R.layout.spinner_item_style, groupNameArray)

        spinner_group?.adapter = spinnerGroupAdapter


        priorityNameArray?.add("Any Priority")
        for (i in 0..TasksDataManager?.tasksPriorities!!.size - 1) {
            if (TasksDataManager?.tasksPriorities?.get(i)?.priorityName?.equals(null) == false) {
                priorityNameArray?.add(TasksDataManager?.tasksPriorities?.get(i)?.priorityName.toString()!!)

                Log.d("response","priority names: "+TasksDataManager?.tasksPriorities?.get(i)?.priorityName.toString()!!)

            }

        }

        var spinnerPriorityAdapter = ArrayAdapter(this, R.layout.spinner_item_style, priorityNameArray)

        spinner_priority?.adapter = spinnerPriorityAdapter



       /* inchargeNameArray?.add("Select Incharge")
        for (i in 0..UserDataManager?.usersInfoList!!.size - 1) {
            if (UserDataManager?.usersInfoList?.get(i)?.fullName?.equals(null) == false) {
                inchargeNameArray?.add(UserDataManager?.usersInfoList?.get(i)?.fullName?.toString()!!)

                Log.d("response","priority names: "+UserDataManager?.usersInfoList?.get(i)?.fullName?.toString()!!)

            }

        }

        var spinnerInchargeAdapter = ArrayAdapter(this, R.layout.spinner_item_style, inchargeNameArray)

        spinner_incharge?.adapter = spinnerInchargeAdapter*/



        var spinnerDateAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.array_datefilter))

        spinner_date?.adapter = spinnerDateAdapter


    }



        fun setCurrentDate() {
            val c = Calendar.getInstance()
            System.out.println("Current time => " + c.time)

            val df = SimpleDateFormat("yyyy/MM/dd")
            val formattedDate = df.format(c.time)

            input_fromDate?.setText(resources.getString(R.string.fromDate))
            input_toDate?.setText(resources.getString(R.string.toDate))

            currentDate = formattedDate
        }

        fun setUpFromDate() {
            val calendar = Calendar.getInstance()
            val y = calendar.get(Calendar.YEAR)
            val m = calendar.get(Calendar.MONTH)
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                    this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                input_fromDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth )
            }, y, m, d)
            dpd.show()
        }

        fun setUpToDate() {
            val calendar = Calendar.getInstance()
            val y = calendar.get(Calendar.YEAR)
            val m = calendar.get(Calendar.MONTH)
            val d = calendar.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                    this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                input_toDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth)
            }, y, m, d)
            dpd.show()
        }


        override fun onClick(v: View?) {
            when(v?.id)
            {
                R.id.button_search->
                {

                    description = Uri.encode(input_description?.text.toString().trim(),"UTF-8")
                    if(description.equals(null))
                    {
                        description = ""
                    }
                    fromDate= Uri.encode(input_fromDate?.text.toString(),"UTF-8")

                    toDate = Uri.encode(input_toDate?.text.toString(),"UTF-8")

                    if(applyDateFilter.equals(0))
                    {
                        fromDate = "1918/01/01"
                        toDate = currentDate.toString()
                    }



                    if(title!!.equals(resources.getString(R.string.assignedToMe)))
                    {
                        getAssignedToMe(projectID,priorityID,statusID,fromDate,toDate,"true",description)

                    }
                    else if(title!!.equals(resources.getString(R.string.assignedByMe)))
                    {
                        getAssignedByMe(projectID,priorityID,statusID,fromDate,toDate,"true",description)
                    }

                    //getAllTransctions(loginManager?.getCompanyID(),projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,description)

                }
                R.id.button_reset->
                {
                    input_description?.setText("")
                    spinner_project?.setSelection(0)
                    spinner_status?.setSelection(0)
                    spinner_priority?.setSelection(0)
                    spinner_date?.setSelection(0)


                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()

                    //getAllTransctions(loginManager?.getCompanyID(),"","","",fromDate,toDate,"","")


                    if(title!!.equals(resources.getString(R.string.assignedToMe)))
                    {
                        getAssignedToMe("","","","","","false","")

                    }
                    else if(title!!.equals(resources.getString(R.string.assignedByMe)))
                    {
                        getAssignedByMe("","","","","","false","")
                    }
                }
                R.id.input_fromdate->
                {
                    setUpFromDate()
                }
                R.id.input_todate->
                {
                    setUpToDate()
                }


            }
        }
}
