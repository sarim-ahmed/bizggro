package com.averox.bizggro.Activities

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.UserDataManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import kotlinx.android.synthetic.main.activity_transaction_detail.*
import org.w3c.dom.Text

class TransactionDetail : AppCompatActivity() {


    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var textview_description: TextView? = null
    private var textview_amount: TextView? = null
    private var textview_date: TextView? = null
    private var textview_project: TextView? = null
    private var textview_category: TextView? = null
    private var textview_type: TextView? = null
    private var textview_po_invoice: TextView? = null
    private var imageview_document: ImageView? = null
    private var position: Int? = null
    private var heading_inovice_po: TextView? = null

    private var description: Any? = ""
    private var amount: Any? = ""
    private var date: Any? = ""
    private var project: Any? = ""
    private var category: Any? = ""
    private var type: Any? = ""
    private var invoice_no: Any? = ""
    private var po_no: Any? = ""
    private var document_url: Any? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_detail)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        textview_description = findViewById(R.id.textview_description)
        textview_amount = findViewById(R.id.textview_amount)
        textview_date = findViewById(R.id.textview_date)
        textview_project = findViewById(R.id.textview_project)
        textview_category = findViewById(R.id.textview_category)
        textview_type = findViewById(R.id.textview_type)
        textview_po_invoice = findViewById(R.id.textview_po_invoice)
        imageview_document = findViewById(R.id.imageview_document)
        heading_inovice_po = findViewById(R.id.heading_invoice_po)

        setData()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {

            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)



    }



    fun setData()
    {

        var intent = intent
        var extras = intent.extras
        position = extras?.getInt("POSITION")

        description = UserDataManager.allTransactionsList?.get(position!!)?.description
        amount = UserDataManager.allTransactionsList?.get(position!!)?.amount
        date = UserDataManager.allTransactionsList?.get(position!!)?.date
        project = UserDataManager.allTransactionsList?.get(position!!)?.projectName
        category = UserDataManager.allTransactionsList?.get(position!!)?.categoryName
        type = UserDataManager.allTransactionsList?.get(position!!)?.transactionTypeName
        invoice_no = UserDataManager.allTransactionsList?.get(position!!)?.invoiceNumber
        po_no = UserDataManager.allTransactionsList?.get(position!!)?.poNumber
        document_url = UserDataManager.allTransactionsList?.get(position!!)?.documentUrl

        loadImage(document_url.toString())


        textview_description?.setText(description.toString())
        textview_amount?.setText(amount.toString())
        textview_date?.setText(date.toString())
        textview_project?.setText(project.toString())
        textview_category?.setText(category.toString())
        textview_type?.setText(type.toString())

        if(type!!.equals("Income"))
        {
            heading_inovice_po?.setText(resources.getString(R.string.invoice_number))
            textview_po_invoice?.setText(invoice_no.toString())
        }
        else if(type!!.equals("Expense"))
        {
            heading_inovice_po?.setText(resources.getString(R.string.po_number))
            textview_po_invoice?.setText(po_no.toString())
        }





    }

    fun loadImage(documentURL: String?)
    {

        Glide.with(this@TransactionDetail).load(documentURL).asBitmap().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageview_document)

    }

}
