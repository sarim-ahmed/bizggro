package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.ContactDetailAdapter
import com.averox.bizggro.Adapters.LeadsDetailAdapter
import com.averox.bizggro.Adapters.LeadsStatusAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.ContactsDataManager
import com.averox.bizggro.UserManagement.LeadsDataManager
import com.averox.bizggro.UserManagement.LoginManager
import kotlinx.android.synthetic.main.activity_all_transactions.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class LeadsList : AppCompatActivity(),View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var contacts_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_status: Spinner? = null
    private var spinner_type: Spinner? = null
    private var spinner_date: Spinner? = null
    private var statusNameArray: ArrayList<String>? = null
    private var contactTypeArray: ArrayList<String>? = null
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var name_email: Any = ""
    private var categoryID: Any = ""
    private var statusID: Any = ""
    private var contactTypeID: Any? = null
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var applyDateFilter: Any = ""
    private var input_name_email: EditText? = null
    private var currentDate: String? = null
    private var input_name_or_email: EditText? = null
    private var toolbar_title: String = ""
    private var toolbar_titleView: TextView? = null
    private var leads_view: RecyclerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_leads)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        leads_view = findViewById(R.id.leads_view)

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE


        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_status = findViewById(R.id.spinner_status)
        spinner_type = findViewById(R.id.spinner_type)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)



        statusNameArray = ArrayList()
        contactTypeArray = ArrayList()



        input_fromDate = findViewById(R.id.input_fromdate)
        input_toDate = findViewById(R.id.input_todate)
        //input_fromDate?.setOnClickListener(this)
        //input_toDate?.setOnClickListener(this)
        input_name_or_email = findViewById(R.id.input_name_or_email)

        toolbar_titleView = findViewById(R.id.toolbar_titleView)


        try {
            var intent = intent
            var extras = intent.extras
            toolbar_title = extras?.getString("TOOLBAR_TITLE")!!
        } catch (e: Exception) {
        } finally {
        }

        toolbar_titleView?.setText(toolbar_title)


        setCurrentDate()

        loadSpinnerValues()

        if(toolbar_title?.equals(resources.getString(R.string.All_Leads)))
        {
            getAllLeads(loginManager?.getCompanyID(),"","1918/01/01",currentDate,0,"")

        }
        else if(toolbar_title?.equals(resources.getString(R.string.My_Leads)))
        {
            getMyLeads(loginManager?.getCompanyID(),loginManager?.getUserID(),"","1918/01/01",currentDate,0,"")

        }

        spinner_date?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position.equals(0)) {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = 0


                } else if (position.equals(1)) {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1


                }
            }
        }

        spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position.equals(0)) {
                    statusID = ""
                } else {
                    statusID = ContactsDataManager?.categoriesList?.get(position)?.categoryId!!
                    Log.d("newTransaction", "categorytId: " + categoryID)
                }


            }

        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {

            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {
            LeadsDataManager?.leadsList?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }


    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_fromdate?.setText(resources.getString(R.string.fromDate))
        input_todate?.setText(resources.getString(R.string.toDate))

        currentDate = formattedDate
    }


    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }


    fun loadSpinnerValues() {


        statusNameArray?.add("Any Status")
        var spinnerstatusAdapter = ArrayAdapter(this, R.layout.spinner_item_style, statusNameArray)
        spinner_status?.adapter = spinnerstatusAdapter

        for (i in 0..LeadsDataManager?.leadsStatusList!!.size - 1) {
            if (LeadsDataManager?.leadsStatusList?.get(i)?.status?.equals(null) == false) {
                statusNameArray?.add(LeadsDataManager?.leadsStatusList?.get(i)?.status.toString()!!)

            }

        }


        spinner_status?.adapter = spinnerstatusAdapter


/*
        contactTypeArray?.add("All Types")

        var spinnerTypeAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,contactTypeArray)

        spinner_type?.adapter = spinnerTypeAdapter

        for(i in 0..UserDataManager?.transactionTypeList!!.size - 1)
        {

            contactTypeArray?.add(UserDataManager?.transactionTypeList?.get(i)?.getTransactionType()!!)


        }

        spinnerTypeAdapter?.notifyDataSetChanged()
*/


        var spinnerDateAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.array_datefilter))

        spinner_date?.adapter = spinnerDateAdapter


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_search -> {

                //description = Uri.encode(input_description?.text.toString().trim(),"UTF-8")
                /* if(description.equals(null))
                {
                    description = ""
                }*/

                name_email = input_name_or_email?.text.toString()
                fromDate = Uri.encode(input_fromDate?.text.toString(), "UTF-8")

                toDate = Uri.encode(input_toDate?.text.toString(), "UTF-8")

                if (applyDateFilter.equals(0)) {
                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()


                }

                if(toolbar_title?.equals(resources.getString(R.string.All_Leads))) {
                    getAllLeads(loginManager?.getCompanyID(), statusID, fromDate, currentDate, applyDateFilter, name_email)
                }

                else if(toolbar_title?.equals(resources.getString(R.string.My_Leads)))
                {
                    getMyLeads(loginManager?.getCompanyID(),loginManager?.getUserID(), statusID, fromDate, currentDate, applyDateFilter, name_email)

                }

                //getAllTransctions(loginManager?.getCompanyID(),projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,description)

            }
            R.id.button_reset -> {
                input_description?.setText("")
                spinner_projects?.setSelection(0)
                spinner_categories?.setSelection(0)
                spinner_type?.setSelection(0)
                spinner_date?.setSelection(0)


                fromDate = "1918/01/01"
                toDate = currentDate.toString()

                if(toolbar_title?.equals(resources.getString(R.string.All_Leads)))
                {
                    getAllLeads(loginManager?.getCompanyID(),"","1918/01/01",currentDate,0,"")

                }
                else if(toolbar_title?.equals(resources.getString(R.string.My_Leads)))
                {
                    getMyLeads(loginManager?.getCompanyID(),loginManager?.getUserID(),"","1918/01/01",currentDate,0,"")

                }

                //getAllTransctions(loginManager?.getCompanyID(),"","","",fromDate,toDate,"","")
            }
            R.id.input_fromdate -> {
                setUpFromDate()
            }
            R.id.input_todate -> {
                setUpToDate()
            }
        }

    }

    fun getAllLeads(companyid: Int?, statusId: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,keyword: Any?)
    {

        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        LeadsDataManager?.leadsList?.clear()
        var adapter = CustomAdapter(this,LeadsDataManager?.leadsList!!)
        leads_view?.layoutManager = LinearLayoutManager(this)
        leads_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.StatusId,statusID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,name_email.toString()))


        UrlBuilder?.setUrl(Module.leads, RequestCode.getleads,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationDate = result.getString(JsonKeys.variables.CreationDate)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var State = result.getString(JsonKeys.variables.KEY_STATE)
                        var CountryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var Details = result.getString(JsonKeys.variables.DETAILS)
                        var CompanyID = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var DateUpdated = result.getString(JsonKeys.variables.DateUpdated)
                        var CreatedById = result.getInt(JsonKeys.variables.CreatedBy)
                        //var createdByName = result.getString(JsonKeys.variables.CREATEDBY)
                        var UpdatedBy = result.getString(JsonKeys.variables.UpdatedBy)
                        var ContactTypeId = result.getInt(JsonKeys.variables.ContactTypeId)
                        var CompanyName = result.getString(JsonKeys.variables.CompanyName)
                        var CategoryId = result.getInt(JsonKeys.variables.CategoryId)
                        var ZipCode = result.getString(JsonKeys.variables.ZipCode)
                        var IsHidden = result.getString(JsonKeys.variables.IsHidden)
                        var leadID = result.getInt(JsonKeys.variables.LeadID)
                        var leadTitle = result.getString(JsonKeys.variables.LeadTitle)
                        var rating = result.getString(JsonKeys.variables.Rating)
                        var leadIncharge = result.getInt(JsonKeys.variables.LeadIncharge)
                        var convertedDate = result.getString(JsonKeys.variables.ConvertedDate)
                        var statusId = result.getInt(JsonKeys.variables.StatusId)
                        var ownerId = result.getInt(JsonKeys.variables.OwnerId)
                        var convertedByID = result.getInt(JsonKeys.variables.ConvertedBy)
                        var modifiedDate = result.getString(JsonKeys.variables.ModifiedDate)
                        var annualRevenue = result.getString(JsonKeys.variables.AnnualRevenue)
                        var noOfEmployees = result.getString(JsonKeys.variables.NoOfEmployees)
                        var leadSource = result.getString(JsonKeys.variables.LeadSource)
                        var status = result.getString(JsonKeys.variables.Status)
                        var convertedByUserName = result.getString(JsonKeys.variables.ConvertedByUserName)
                        var source = result.getString(JsonKeys.variables.SourceUserName)
                        var category = result.getString(JsonKeys.variables.Category)
                        var countryName = result.getString(JsonKeys.variables.CountryName)
                        var inchargeName = result.getString(JsonKeys.variables.InchargeUserName)
                        var ownerName = result.getString(JsonKeys.variables.OwnerUserName)



                        LeadsDataManager?.insertLeads(contactId,firstname,lastname,email,
                                phone, mobile,address,creationDate, city,State, CountryID,Details,
                                CompanyID,DateUpdated, CreatedById,
                                UpdatedBy, ContactTypeId,CompanyName,CategoryId,
                                ZipCode, IsHidden,leadID,leadTitle,
                                rating,leadIncharge,convertedDate,statusId,ownerId,
                                convertedByID,modifiedDate,annualRevenue,noOfEmployees,
                                leadSource,status,convertedByUserName,
                                category,countryName,source,
                                inchargeName,ownerName)


                        var adapter = CustomAdapter(this,LeadsDataManager?.leadsList!!)
                        leads_view?.layoutManager = LinearLayoutManager(this)
                        leads_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                } finally {
                }

            }


        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }


    fun getMyLeads(companyid: Int?,currentUserId: Any?, statusId: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,keyword: Any?)
    {

        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        LeadsDataManager?.leadsList?.clear()
        var adapter = CustomAdapter(this,LeadsDataManager?.leadsList!!)
        leads_view?.layoutManager = LinearLayoutManager(this)
        leads_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CurrentUserId,currentUserId.toString()))

        requestParameters.add(BasicNameValuePair(Parameters.StatusId,statusID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,name_email.toString()))


        UrlBuilder?.setUrl(Module.leads, RequestCode.getleads,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationDate = result.getString(JsonKeys.variables.CreationDate)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var State = result.getString(JsonKeys.variables.KEY_STATE)
                        var CountryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var Details = result.getString(JsonKeys.variables.DETAILS)
                        var CompanyID = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var DateUpdated = result.getString(JsonKeys.variables.DateUpdated)
                        var CreatedById = result.getInt(JsonKeys.variables.CreatedBy)
                        //var createdByName = result.getString(JsonKeys.variables.CREATEDBY)
                        var UpdatedBy = result.getString(JsonKeys.variables.UpdatedBy)
                        var ContactTypeId = result.getInt(JsonKeys.variables.ContactTypeId)
                        var CompanyName = result.getString(JsonKeys.variables.CompanyName)
                        var CategoryId = result.getInt(JsonKeys.variables.CategoryId)
                        var ZipCode = result.getString(JsonKeys.variables.ZipCode)
                        var IsHidden = result.getString(JsonKeys.variables.IsHidden)
                        var leadID = result.getInt(JsonKeys.variables.LeadID)
                        var leadTitle = result.getString(JsonKeys.variables.LeadTitle)
                        var rating = result.getString(JsonKeys.variables.Rating)
                        var leadIncharge = result.getInt(JsonKeys.variables.LeadIncharge)
                        var convertedDate = result.getString(JsonKeys.variables.ConvertedDate)
                        var statusId = result.getInt(JsonKeys.variables.StatusId)
                        var ownerId = result.getInt(JsonKeys.variables.OwnerId)
                        var convertedByID = result.getInt(JsonKeys.variables.ConvertedBy)
                        var modifiedDate = result.getString(JsonKeys.variables.ModifiedDate)
                        var annualRevenue = result.getString(JsonKeys.variables.AnnualRevenue)
                        var noOfEmployees = result.getString(JsonKeys.variables.NoOfEmployees)
                        var leadSource = result.getString(JsonKeys.variables.LeadSource)
                        var status = result.getString(JsonKeys.variables.Status)
                        var convertedByUserName = result.getString(JsonKeys.variables.ConvertedByUserName)
                        var source = result.getString(JsonKeys.variables.SourceUserName)
                        var category = result.getString(JsonKeys.variables.Category)
                        var countryName = result.getString(JsonKeys.variables.CountryName)
                        var inchargeName = result.getString(JsonKeys.variables.InchargeUserName)
                        var ownerName = result.getString(JsonKeys.variables.OwnerUserName)




                        LeadsDataManager?.insertLeads(contactId,firstname,lastname,email,
                                phone, mobile,address,creationDate, city,State, CountryID,Details,
                                CompanyID,DateUpdated, CreatedById,
                                UpdatedBy, ContactTypeId,CompanyName,CategoryId,
                                ZipCode, IsHidden,leadID,leadTitle,
                                rating,leadIncharge,convertedDate,statusId,ownerId,
                                convertedByID,modifiedDate,annualRevenue,noOfEmployees,
                                leadSource,status,convertedByUserName,category,countryName,source,
                                inchargeName,ownerName)


                        var adapter = CustomAdapter(this,LeadsDataManager?.leadsList!!)
                        leads_view?.layoutManager = LinearLayoutManager(this)
                        leads_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                } finally {
                }

            }


        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    internal inner class CustomAdapter(private val context: Context, arraylist: ArrayList<LeadsDetailAdapter>) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<LeadsDetailAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.leads_list_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/


            holder.name_view?.setText(""+list_items.firstname+" "+list_items.lastname)
            holder.status_view?.setText(""+list_items.status)


        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<LeadsDetailAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var name_view: TextView? = null
            var status_view: TextView? = null


            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                name_view = itemView.findViewById(R.id.name_view)
                status_view = itemView.findViewById(R.id.status_view)


            }

            override fun onClick(v: View) {

                val itemPosition = leads_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                var intent = Intent(this@LeadsList,LeadsDetailActivity::class.java)
                intent.putExtra("LEAD_POSITION",itemPosition)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)



            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }


}
