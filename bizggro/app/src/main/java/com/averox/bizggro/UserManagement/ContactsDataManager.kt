package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 2/13/2018.
 */
public class ContactsDataManager {
    companion object {
        public var countriesList: ArrayList<CountriesListAdapter>? = null
        public var categoriesList: ArrayList<ContactCategoriesAdapter>? = null
        public var contactDetailList: ArrayList<ContactDetailAdapter>? = null


        init {
            countriesList = ArrayList<CountriesListAdapter>()
            categoriesList = ArrayList<ContactCategoriesAdapter>()
            contactDetailList = ArrayList<ContactDetailAdapter>()

        }

        fun insertCountries(CountryId: Any?, Name: Any?) {
            countriesList?.add(CountriesListAdapter(CountryId, Name))
        }
        fun insertCategories( CategoryId: Any?,Category: Any?) {
            categoriesList?.add(ContactCategoriesAdapter(CategoryId,Category))
        }

        fun insertContactDetails(contactId: Any?,firstname: Any?, lastname: Any?,email: Any?,
                                   phone: Any?,mobile: Any?,address: Any?,creationDate: Any?,
                                   city: Any?, state: Any?,countryId: Any?, details: Any?,
                                    companyId: Any?,dateUpdated: Any?, createdBy: Any?,updatedBy: Any?,
                                   contactTypeId: Any?, companyName: Any?,categoryId: Any?, zipCode: Any?,
                                   isHidden: Any?, countryName: Any?, Type: Any?,
                                  CreatedByName: Any?,Category: Any?)
        {

            contactDetailList?.add(ContactDetailAdapter(contactId,firstname, lastname,email,
            phone,mobile,address,creationDate,
            city, state,countryId, details,
            companyId,dateUpdated, createdBy,updatedBy,
            contactTypeId, companyName,categoryId, zipCode,
            isHidden,  countryName,Type,
                    CreatedByName,Category))

        }

    }
}

