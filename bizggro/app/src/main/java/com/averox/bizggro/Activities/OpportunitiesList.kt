package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Adapters.LeadsDetailAdapter
import com.averox.bizggro.Adapters.OpportunitiesDetailAdapter
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.ContactsDataManager
import com.averox.bizggro.UserManagement.LeadsDataManager
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.OpportunitiesDataManager
import kotlinx.android.synthetic.main.activity_all_transactions.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class OpportunitiesList : AppCompatActivity(),View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var opportunities_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_stage: Spinner? = null
    private var spinner_type: Spinner? = null
    private var spinner_date: Spinner? = null
    private var stageNameArray: ArrayList<String>? = null
    private var typeNameArray: ArrayList<String>? = null
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var name_email: Any = ""
    private var categoryID: Any = ""
    private var stageID: Any = ""
    private var typeID: Any = ""
    private var contactTypeID: Any? = null
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var applyDateFilter: Any = ""
    private var input_name_email: EditText? = null
    private var currentDate: String? = null
    private var input_name_or_email: EditText? = null
    private var toolbar_title: String = ""
    private var toolbar_titleView: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opportunities_list)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        opportunities_view = findViewById(R.id.opportunities_view)

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE


        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_stage = findViewById(R.id.spinner_stage)
        spinner_type = findViewById(R.id.spinner_type)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)



        stageNameArray = ArrayList()
        typeNameArray = ArrayList()



        input_fromDate = findViewById(R.id.input_fromdate)
        input_toDate = findViewById(R.id.input_todate)
        input_fromDate?.setOnClickListener(this)
        input_toDate?.setOnClickListener(this)
        input_name_or_email = findViewById(R.id.input_name_or_email)

        toolbar_titleView = findViewById(R.id.toolbar_titleView)


        try {
            var intent = intent
            var extras = intent.extras
            toolbar_title = extras?.getString("TOOLBAR_TITLE")!!
        } catch (e: Exception) {
        } finally {
        }

        toolbar_titleView?.setText(toolbar_title)


        setCurrentDate()
        loadSpinnerValues()

        if(toolbar_title?.equals(resources.getString(R.string.allOpportunities)))
        {
            getAllOpportunities(loginManager?.getCompanyID(),"","","1918/01/01",currentDate,0,"")

        }
        else if(toolbar_title?.equals(resources.getString(R.string.myOpportunites)))
        {
            getMyOpportunities(loginManager?.getCompanyID(),"","",loginManager?.getUserID(),"1918/01/01",currentDate,0,"")

        }


        spinner_date?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position.equals(0)) {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = 0


                } else if (position.equals(1)) {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1


                }
            }
        }

        spinner_stage?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position.equals(0)) {
                    stageID = ""
                } else {
                    stageID = OpportunitiesDataManager.stagesList?.get(position - 1)?.stageId!!
                    Log.d("newTransaction", "categorytId: " + stageID)
                }


            }

        }

        spinner_type?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position.equals(0)) {
                    typeID = ""
                } else {
                    typeID = OpportunitiesDataManager.typesList?.get(position - 1)?.typeId!!
                    Log.d("newTransaction", "categorytId: " + typeID)
                }


            }

        }


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {

            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {
            OpportunitiesDataManager.opportunitiesList?.clear()
        } catch (e: Exception) {
        } finally {
        }
    }



    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_fromdate?.setText(resources.getString(R.string.fromDate))
        input_todate?.setText(resources.getString(R.string.toDate))

        currentDate = formattedDate
    }


    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_search -> {


                name_email = input_name_or_email?.text.toString()
                fromDate = Uri.encode(input_fromDate?.text.toString(), "UTF-8")

                toDate = Uri.encode(input_toDate?.text.toString(), "UTF-8")

                Log.d("response","stageID: "+ stageID)
                Log.d("response","typeID: "+ typeID)


                if (applyDateFilter.equals(0)) {
                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()


                }

                if(toolbar_title?.equals(resources.getString(R.string.allOpportunities))) {
                    //getAllLeads(loginManager?.getCompanyID(), statusID, fromDate, currentDate, applyDateFilter, name_email)
                    getAllOpportunities(loginManager?.getCompanyID(),typeID,stageID,fromDate,toDate,true,name_email)


                }

                else if(toolbar_title?.equals(resources.getString(R.string.myOpportunites)))
                {
                    //getMyLeads(loginManager?.getCompanyID(),loginManager?.getUserID(), statusID, fromDate, currentDate, applyDateFilter, name_email)
                    getMyOpportunities(loginManager?.getCompanyID(),typeID,stageID,loginManager?.getUserID(),fromDate,toDate,true,name_email)

                }

                //getAllTransctions(loginManager?.getCompanyID(),projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,description)

            }
            R.id.button_reset -> {
                input_name_or_email?.setText("")
                spinner_stage?.setSelection(0)
                spinner_type?.setSelection(0)
                spinner_date?.setSelection(0)


                fromDate = "1918/01/01"
                toDate = currentDate.toString()



                if(toolbar_title?.equals(resources.getString(R.string.allOpportunities)))
                {
                    getAllOpportunities(loginManager?.getCompanyID(),"","","1918/01/01",currentDate,0,"")

                }
                else if(toolbar_title?.equals(resources.getString(R.string.myOpportunites)))
                {
                    getMyOpportunities(loginManager?.getCompanyID(),"","",loginManager?.getUserID(),"1918/01/01",currentDate,0,"")

                }

                //getAllTransctions(loginManager?.getCompanyID(),"","","",fromDate,toDate,"","")
            }
            R.id.input_fromdate -> {
                setUpFromDate()
            }
            R.id.input_todate -> {
                setUpToDate()
            }
        }

    }


    fun loadSpinnerValues() {


        stageNameArray?.add("Any Stage")
        var spinnerstageAdapter = ArrayAdapter(this, R.layout.spinner_item_style, stageNameArray)
        spinner_stage?.adapter = spinnerstageAdapter

        for (i in 0..OpportunitiesDataManager?.stagesList!!.size - 1) {
            if (OpportunitiesDataManager?.stagesList?.get(i)?.stage?.equals(null) == false) {
                stageNameArray?.add(OpportunitiesDataManager?.stagesList?.get(i)?.stage.toString()!!)

            }
        }

        spinner_stage?.adapter = spinnerstageAdapter


        typeNameArray?.add("Any Type")
        var spinnerTypeAdapter = ArrayAdapter(this, R.layout.spinner_item_style, typeNameArray)
        spinner_type?.adapter = spinnerTypeAdapter

        for (i in 0..OpportunitiesDataManager?.typesList!!.size - 1) {
            if (OpportunitiesDataManager?.typesList?.get(i)?.type?.equals(null) == false) {
                typeNameArray?.add(OpportunitiesDataManager?.typesList?.get(i)?.type.toString()!!)

            }
        }

        spinner_type?.adapter = spinnerTypeAdapter

        var spinnerDateAdapter = ArrayAdapter(this, R.layout.spinner_item_style, resources.getStringArray(R.array.array_datefilter))
        spinner_date?.adapter = spinnerDateAdapter


    }



    fun getAllOpportunities(companyid: Int?, typeId: Any,stageId: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,keyword: Any?)
    {

        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        OpportunitiesDataManager.opportunitiesList?.clear()
        var adapter = CustomAdapter(this,OpportunitiesDataManager?.opportunitiesList!!)
        opportunities_view?.layoutManager = LinearLayoutManager(this)
        opportunities_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.StageId,stageID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.TypeId,typeID.toString()))

        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,name_email.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.applyfilters,true.toString()))


        UrlBuilder?.setUrl(Module.opportunity, RequestCode.getopportunities,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationDate = result.getString(JsonKeys.variables.CreationDate)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var State = result.getString(JsonKeys.variables.KEY_STATE)
                        var CountryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var Details = result.getString(JsonKeys.variables.DETAILS)
                        var CompanyID = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var DateUpdated = result.getString(JsonKeys.variables.DateUpdated)
                        var CreatedById = result.getInt(JsonKeys.variables.CreatedBy)
                        //var createdByName = result.getString(JsonKeys.variables.CREATEDBY)
                        var UpdatedBy = result.getString(JsonKeys.variables.UpdatedBy)
                        var ContactTypeId = result.getInt(JsonKeys.variables.ContactTypeId)
                        var CompanyName = result.getString(JsonKeys.variables.CompanyName)
                        var CategoryId = result.getInt(JsonKeys.variables.CategoryId)
                        var ZipCode = result.getString(JsonKeys.variables.ZipCode)
                        var IsHidden = result.getString(JsonKeys.variables.IsHidden)
                        var leadID = result.getInt(JsonKeys.variables.LeadID)
                        var convertedDate = result.getString(JsonKeys.variables.DateConverted)
                        var convertedByUserName = result.getString(JsonKeys.variables.ConvertedByUser)
                        var stageID = result.getInt(JsonKeys.variables.StageID)
                        var description = result.getString(JsonKeys.variables.KEY_DESCRIPTION)
                        var opportunityID = result.getInt(JsonKeys.variables.OpportunityID)
                        var typeID = result.getInt(JsonKeys.variables.TYPEID)
                        var budget =  result.getString(JsonKeys.variables.Budget)
                        var OrderExpectedDate = result.getString(JsonKeys.variables.OrderExpectedDate)
                        var ConvertedBy = result.getInt(JsonKeys.variables.ConvertedBy)
                        var AssignedTo = result.getInt(JsonKeys.variables.AssignedTo)
                        var ProposalWriterId = result.getInt(JsonKeys.variables.ProposalWriterId)
                        var Competitor = result.getString(JsonKeys.variables.Competitor)
                        var ContactThroughId = result.getInt(JsonKeys.variables.ContactThroughId)
                        var Stage = result.getString(JsonKeys.variables.Stage)
                        var Type = result.getString(JsonKeys.variables.TYPE)
                        var ContactThroughUserName = result.getString(JsonKeys.variables.ContactThroughUserName)
                        var ProposalWriterUserName = result.getString(JsonKeys.variables.ProposalWriterUserName)
                        var AssignedToUserName = result.getString(JsonKeys.variables.AssignedToUserName)
                        var ContactCategory = result.getString(JsonKeys.variables.ContactCategory)
                        var CountryName = result.getString(JsonKeys.variables.CountryName)

                       OpportunitiesDataManager?.insertOpportunites(contactId,firstname,lastname,email,
                                phone, mobile,address,creationDate, city,State, CountryID,Details,
                                CompanyID,DateUpdated, CreatedById,
                                UpdatedBy, ContactTypeId,CompanyName,CategoryId,
                                ZipCode, IsHidden,opportunityID,leadID,
                               stageID, description, typeID, budget,
                               OrderExpectedDate, ConvertedBy,
                               convertedDate,AssignedTo, ProposalWriterId,
                               Competitor,ContactThroughId,Stage,
                               Type,convertedByUserName,ContactThroughUserName,
                               ProposalWriterUserName,AssignedToUserName,
                               ContactCategory,CountryName)


                        var adapter = CustomAdapter(this,OpportunitiesDataManager?.opportunitiesList!!)
                        opportunities_view?.layoutManager = LinearLayoutManager(this)
                        opportunities_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                } finally {
                }

            }
        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    fun getMyOpportunities(companyid: Int?, typeId: Any,stageId: Any?,CurrentUserId: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,keyword: Any?)
    {

        // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        OpportunitiesDataManager.opportunitiesList?.clear()
        var adapter = CustomAdapter(this,OpportunitiesDataManager?.opportunitiesList!!)
        opportunities_view?.layoutManager = LinearLayoutManager(this)
        opportunities_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.StageId,stageID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.TypeId,typeID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Keyword,name_email.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.applyfilters,true.toString()))


        UrlBuilder?.setUrl(Module.opportunity, RequestCode.getmyopportunities,requestParameters)

        Log.d("response", UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getUrl(),null, Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var contactId = result.getInt(JsonKeys.variables.CONTACTID)
                        var firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                        var lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)
                        var email = result.getString(JsonKeys.variables.KEY_EMAIL)
                        var phone = result.getString(JsonKeys.variables.KEY_PHONE)
                        var mobile = result.getString(JsonKeys.variables.KEY_MOBILE)
                        var address = result.getString(JsonKeys.variables.KEY_ADDRESS)
                        var creationDate = result.getString(JsonKeys.variables.CreationDate)
                        var city = result.getString(JsonKeys.variables.KEY_CITY)
                        var State = result.getString(JsonKeys.variables.KEY_STATE)
                        var CountryID = result.getInt(JsonKeys.variables.COUNTRY_ID)
                        var Details = result.getString(JsonKeys.variables.DETAILS)
                        var CompanyID = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                        var DateUpdated = result.getString(JsonKeys.variables.DateUpdated)
                        var CreatedById = result.getInt(JsonKeys.variables.CreatedBy)
                        //var createdByName = result.getString(JsonKeys.variables.CREATEDBY)
                        var UpdatedBy = result.getString(JsonKeys.variables.UpdatedBy)
                        var ContactTypeId = result.getInt(JsonKeys.variables.ContactTypeId)
                        var CompanyName = result.getString(JsonKeys.variables.CompanyName)
                        var CategoryId = result.getInt(JsonKeys.variables.CategoryId)
                        var ZipCode = result.getString(JsonKeys.variables.ZipCode)
                        var IsHidden = result.getString(JsonKeys.variables.IsHidden)
                        var leadID = result.getInt(JsonKeys.variables.LeadID)
                        var convertedDate = result.getString(JsonKeys.variables.DateConverted)
                        var convertedByUserName = result.getString(JsonKeys.variables.ConvertedByUser)
                        var stageID = result.getInt(JsonKeys.variables.StageID)
                        var description = result.getString(JsonKeys.variables.KEY_DESCRIPTION)
                        var opportunityID = result.getInt(JsonKeys.variables.OpportunityID)
                        var typeID = result.getInt(JsonKeys.variables.TYPEID)
                        var budget =  result.getString(JsonKeys.variables.Budget)
                        var OrderExpectedDate = result.getString(JsonKeys.variables.OrderExpectedDate)
                        var ConvertedBy = result.getInt(JsonKeys.variables.ConvertedBy)
                        var AssignedTo = result.getInt(JsonKeys.variables.AssignedTo)
                        var ProposalWriterId = result.getInt(JsonKeys.variables.ProposalWriterId)
                        var Competitor = result.getString(JsonKeys.variables.Competitor)
                        var ContactThroughId = result.getInt(JsonKeys.variables.ContactThroughId)
                        var Stage = result.getString(JsonKeys.variables.Stage)
                        var Type = result.getString(JsonKeys.variables.TYPE)
                        var ContactThroughUserName = result.getString(JsonKeys.variables.ContactThroughUserName)
                        var ProposalWriterUserName = result.getString(JsonKeys.variables.ProposalWriterUserName)
                        var AssignedToUserName = result.getString(JsonKeys.variables.AssignedToUserName)
                        var ContactCategory = result.getString(JsonKeys.variables.ContactCategory)
                        var CountryName = result.getString(JsonKeys.variables.CountryName)

                        OpportunitiesDataManager?.insertOpportunites(contactId,firstname,lastname,email,
                                phone, mobile,address,creationDate, city,State, CountryID,Details,
                                CompanyID,DateUpdated, CreatedById,
                                UpdatedBy, ContactTypeId,CompanyName,CategoryId,
                                ZipCode, IsHidden,opportunityID,leadID,
                                stageID, description, typeID, budget,
                                OrderExpectedDate, ConvertedBy,
                                convertedDate,AssignedTo, ProposalWriterId,
                                Competitor,ContactThroughId,Stage,
                                Type,convertedByUserName,ContactThroughUserName,
                                ProposalWriterUserName,AssignedToUserName,
                                ContactCategory,CountryName)


                        var adapter = CustomAdapter(this,OpportunitiesDataManager?.opportunitiesList!!)
                        opportunities_view?.layoutManager = LinearLayoutManager(this)
                        opportunities_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE

                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                } finally {
                }

            }
        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    internal inner class CustomAdapter(private val context: Context, arraylist: ArrayList<OpportunitiesDetailAdapter>) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()

        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null

        var arraylist: ArrayList<OpportunitiesDetailAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.opportunities_list_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

            /*    holder.user_name.setText(list_items.getName())
                holder.content.setText(list_items.getContent())
                holder.time.setText(list_items.getTime())*/


            holder.name_view?.setText(""+list_items.firstname+" "+list_items.lastname)
            holder.company_view?.setText(""+list_items.companyName)


        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<LeadsDetailAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var name_view: TextView? = null
            var company_view: TextView? = null


            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                name_view = itemView.findViewById(R.id.name_view)
                company_view = itemView.findViewById(R.id.company_view)


            }

            override fun onClick(v: View) {

                val itemPosition = opportunities_view?.getChildLayoutPosition(v)

                /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                         UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
                 toast(""+itemPosition)*/

                var intent = Intent(this@OpportunitiesList,OpportunityDetailActivity::class.java)
                intent.putExtra("OPPORTUNITY_POSITION",itemPosition)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }




}
