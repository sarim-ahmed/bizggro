package com.averox.bizggro.Activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.util.Log
import android.view.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.R
import org.json.JSONObject
import android.widget.*
import com.android.volley.toolbox.BasicNetwork
import com.averox.bizggro.Adapters.FinancialsAdapter
import com.averox.bizggro.Adapters.TransactionsAdapter
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.UserManagement.*
import kotlinx.android.synthetic.main.activity_all_transactions.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AllTransactions : AppCompatActivity(),View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var transactions_view: RecyclerView? = null
    private var loginManager: LoginManager? = null
    private var loader: ProgressBar? = null
    private var layout_filters: LinearLayout? = null
    private var button_search: ImageButton? = null
    private var button_reset: ImageButton? = null
    private var spinner_projects: Spinner?= null
    private var spinner_categories: Spinner? = null
    private var spinner_type: Spinner? = null
    private var spinner_date: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var categoryNameArray: ArrayList<String>? = null
    private var transactionTypeArray: ArrayList<String>? = null
    private var input_fromDate: Button? = null
    private var input_toDate: Button? = null
    private var layout_date: LinearLayout? = null
    private var description: Any = ""
    private var projectID: Any = ""
    private var categoryID: Any = ""
    private var transactionTypeID: Any? = null
    private var fromDate: Any = ""
    private var toDate: Any = ""
    private var applyDateFilter: Any = ""
    private var input_description: EditText? = null
    private var currentDate: String? = null
    private var item_position: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_transactions)
        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        transactions_view = findViewById(R.id.transactions_view)

        loginManager = LoginManager(Constants.context!!)

        loader = findViewById(R.id.loader)
        loader?.visibility = View.VISIBLE


        layout_filters = findViewById(R.id.layout_filters)
        button_reset = findViewById(R.id.button_reset)
        button_reset?.setOnClickListener(this)
        button_search = findViewById(R.id.button_search)
        button_search?.setOnClickListener(this)
        spinner_categories = findViewById(R.id.spinner_categories)
        spinner_projects = findViewById(R.id.spinner_projects)
        spinner_type = findViewById(R.id.spinner_type)
        spinner_date = findViewById(R.id.spinner_date)
        layout_date = findViewById(R.id.layout_date)

        input_description = findViewById(R.id.input_description)


        projectNameArray = ArrayList()
        categoryNameArray = ArrayList()
        transactionTypeArray = ArrayList()

        input_fromDate = findViewById(R.id.input_fromdate)
        input_toDate = findViewById(R.id.input_todate)

        input_fromDate?.setOnClickListener(this)
        input_toDate?.setOnClickListener(this)

        setCurrentDate()

        loadSpinnerValues()

        getAllTransctions(loginManager?.getCompanyID(),"","","","1918/01/01",currentDate,"","")


        spinner_date?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position.equals(0))
                {
                    layout_date?.visibility = View.GONE
                    applyDateFilter = 0

                }
                else if(position.equals(1))
                {
                    layout_date?.visibility = View.VISIBLE
                    applyDateFilter = 1

                }
            }
        }
        spinner_categories?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    categoryID = ""
                }
                else {
                    categoryID = UserDataManager.companyCategoriesList?.get(position - 1)?.getCategoryID()!!
                    Log.d("newTransaction", "categorytId: " + categoryID)
                }

            }

        }
        spinner_projects?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    projectID = ""
                }
                else {
                    projectID = UserDataManager.allProjectsList?.get(position - 1)?.getProjectID()!!

                    Log.d("newTransaction", "projectId: " + projectID)
                }
            }

        }

        spinner_type?.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position.equals(0))
                {
                    transactionTypeID = ""
                }
                else
                {
                    transactionTypeID = UserDataManager.transactionTypeList?.get(position-1)?.getTransactionID()

                }

            }

            }
    }


    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left)

        try {
            UserDataManager?.allTransactionsList?.clear()
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)

    }

    fun getAllTransctions(companyid: Int?, projectID: Any?, categoryID: Any?,transactionTypeID: Any?,fromDate: Any?,toDate: Any?,applyDateFilter: Any?,description: Any?)
    {

       // UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,
        UserDataManager.allTransactionsList?.clear()
        var adapter = CustomAdapter(this,UserDataManager.allTransactionsList!!)
        transactions_view?.layoutManager = LinearLayoutManager(this)
        transactions_view?.adapter = adapter
        adapter.notifyDataSetChanged()
        loader?.visibility = View.VISIBLE


        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,companyid.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ProjectId,projectID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.CategoryID,categoryID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.TransactionTypeID,transactionTypeID.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.FromDate,fromDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ToDate,toDate.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.ApplyDateFilter,applyDateFilter.toString()))
        requestParameters.add(BasicNameValuePair(Parameters.Description,description.toString()))


        UrlBuilder?.setUrl(Module.bookkeeping,RequestCode.getfilteredtransactions,requestParameters)

        Log.d("response",UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET,UrlBuilder.getUrl(),null,Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))


            }
            else
            {
                val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)



                try {
                    for(i in 0..data.length() - 1)
                    {
                        var result = data.getJSONObject(i)

                        var transactionID = result.getInt(JsonKeys.variables.TransactionID)
                        var description = result.getString(JsonKeys.variables.KEY_DESCRIPTION)
                        var amount = result.getInt(JsonKeys.variables.KEY_AMOUNT)
                        var date = result.getString(JsonKeys.variables.KEY_DATE)
                        var categoryID = result.getInt(JsonKeys.variables.CategoryID)
                        var projectID = result.getInt(JsonKeys.variables.ProjectID)
                        var transactionTypeID = result.getInt(JsonKeys.variables.TransactionTypeID)
                        var companyID =  result.getInt(JsonKeys.variables.CompanyID)
                        var invoiceNumber = result.getString(JsonKeys.variables.InvoiceNumber)
                        var attachment = result.getString(JsonKeys.variables.KEY_ATTACHMENT)
                        var poNumber  = result.getString(JsonKeys.variables.PONumber)
                        var categoryName = result.getString(JsonKeys.variables.CategoryName)
                        var documentName = result.getString(JsonKeys.variables.DocumentName)
                        var invoiceID = result.getString(JsonKeys.variables.InvoiceID)
                        var poID =  result.getString(JsonKeys.variables.PO_ID)
                        var documentUrl = result.getString(JsonKeys.variables.DocumentURL)
                        var transactionTypeName = result.getString(JsonKeys.variables.TransactionTypeName)
                        var projectName = result.getString(JsonKeys.variables.ProjectName)




                        UserDataManager.insertAllTransactions(transactionID,description,amount,date,categoryID,projectID,
                                transactionTypeID,companyID,invoiceNumber,attachment,poNumber,
                                categoryName,documentName,invoiceID,poID,projectName,transactionTypeName,documentUrl)


                        var adapter = CustomAdapter(this,UserDataManager.allTransactionsList!!)
                        transactions_view?.layoutManager = LinearLayoutManager(this)
                        transactions_view?.adapter = adapter
                        adapter.notifyDataSetChanged()
                        loader?.visibility = View.GONE


                    }
                } catch (e: Exception) {

                    Log.d("response","exception: " +e.message.toString())
                }

                finally
                {

                }

            }

        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
    ){}

        ApplicationController.instance?.addToRequestQueue(request)

    }

    internal inner class CustomAdapter(private val context: Context, arraylist: ArrayList<TransactionsAdapter>) : RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {

        //Creating an arraylist of POJO objects
        //private var list_members: ArrayList<FinancialsAdapter> = ArrayList()
        private val inflater: LayoutInflater
        internal var view: View? = null
        internal var holder: MyViewHolder? = null
        var arraylist: ArrayList<TransactionsAdapter> = ArrayList()

        init {
            inflater = LayoutInflater.from(context)


            this.arraylist = arraylist
        }

        //This method inflates view present in the RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            view = inflater.inflate(R.layout.transaction_item, parent, false)
            holder = MyViewHolder(view!!)
            return holder!!
        }

        //Binding the data using get() method of POJO object
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val list_items = arraylist[position]

        /*    holder.user_name.setText(list_items.getName())
            holder.content.setText(list_items.getContent())
            holder.time.setText(list_items.getTime())*/
            var sr_number = position+1

            holder.sr_view?.setText(""+sr_number)
            holder.date_view?.setText(""+list_items.date)
            holder.description_view?.setText(""+list_items.description)
            holder.category_view?.setText(""+list_items.categoryName)
            holder.amount_view?.setText(""+list_items.amount)
/*
            Log.d("holder",""+list_items.getDate())
            Log.d("holder",""+list_items.getDescription())

            Log.d("holder",""+list_items.getCategory())

            Log.d("holder",""+list_items.getAmount())*/

        }

        //Setting the arraylist
        fun setListContent(list_members: ArrayList<FinancialsAdapter>) {
            notifyItemRangeChanged(0, arraylist.size)

        }

        override fun getItemCount(): Int {

            Log.d("holder",""+arraylist.size)

            return arraylist.size


        }

        //View holder class, where all view components are defined
        internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

            var date_view: TextView? = null
            var description_view: TextView? = null
            var category_view: TextView? = null
            var amount_view: TextView? = null
            var sr_view: TextView? = null

            init {

                itemView.setOnClickListener(this)
                /*user_name = itemView.findViewById(R.id.user_name)
                content = itemView.findViewById(R.id.content)*/
                date_view = itemView.findViewById(R.id.date_view)
                description_view = itemView.findViewById(R.id.description_view)
                category_view =  itemView.findViewById(R.id.category_view)
                amount_view = itemView.findViewById(R.id.amount_view)
                sr_view = itemView.findViewById(R.id.sr_view)

            }

           override fun onClick(v: View) {

               val itemPosition = transactions_view?.getChildLayoutPosition(v)

              /* alert(""+UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDate()+
                       UserDataManager.allFinancialsList?.get(itemPosition!!)?.getCategory()+
                       UserDataManager.allFinancialsList?.get(itemPosition!!)?.getDescription()+
                       UserDataManager.allFinancialsList?.get(itemPosition!!)?.getAmount()).show()
               toast(""+itemPosition)*/

               item_position =  itemPosition

               registerForContextMenu(transactions_view)
               transactions_view?.showContextMenu()

            }
        }

        fun removeAt(position: Int) {
            arraylist.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(0, arraylist.size)
        }

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

            getMenuInflater().inflate(R.menu.transactions_option_menu, menu)

    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        // val info = item?.getMenuInfo() as AdapterContextMenuInfo

        when (item?.itemId) {
            R.id.context_detail -> {
                //openCamera()

                var intent = Intent(this@AllTransactions,TransactionDetail::class.java)

                intent.putExtra("POSITION",item_position)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)


                return true
            }

            R.id.context_edit -> {
                //openCamera()

                var intent = Intent(this@AllTransactions,NewTransaction::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.string_title_edittransaction))
                intent.putExtra("ITEM_POSITION",item_position)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)


                return true
            }

            R.id.context_delete -> {
                //openCamera()

               /* var intent = Intent(this@AllTransactions,NewTransaction::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.string_title_edittransaction))
                intent.putExtra("ITEM_POSITION",item_position)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)*/

                try {

                    val alertDialog = AlertDialog.Builder(this@AllTransactions).create()



                    alertDialog?.setMessage("Are you sure you want to Delete this Transaction?")

                    alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "Yes") { dialog, which ->


                        deleteTransaction()

                    }

                    alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "No") { dialog, which ->

                        alertDialog?.cancel()

                    }

                    alertDialog.show()




                } catch (e: Exception) {
                } finally {
                }


                return true
            }




            R.id.context_cancel -> {
                try {
                    //attachment_view?.setImageDrawable(null)
                   // attachment_view?.setImageBitmap(null)
                    //base64String = ""

                    //attachment_view?.setImageDrawable(resources.getDrawable(R.drawable.attach_more))
                } catch (e: Exception) {
                } finally {
                }
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }


    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_search->
            {

                description = Uri.encode(input_description?.text.toString().trim(),"UTF-8")
                if(description.equals(null))
                {
                    description = ""
                }
                fromDate= Uri.encode(input_fromDate?.text.toString(),"UTF-8")

                toDate = Uri.encode(input_toDate?.text.toString(),"UTF-8")

                if(applyDateFilter.equals(0))
                {
                    fromDate = "1918/01/01"
                    toDate = currentDate.toString()
                }




                getAllTransctions(loginManager?.getCompanyID(),projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,description)

            }
            R.id.button_reset->
            {
                input_description?.setText("")
                spinner_projects?.setSelection(0)
                spinner_categories?.setSelection(0)
                spinner_type?.setSelection(0)
                spinner_date?.setSelection(0)


                fromDate = "1918/01/01"
                toDate = currentDate.toString()

                getAllTransctions(loginManager?.getCompanyID(),"","","",fromDate,toDate,"","")
            }
            R.id.input_fromdate->
            {
                setUpFromDate()
            }
            R.id.input_todate->
            {
                setUpToDate()
            }


        }
    }

    fun loadSpinnerValues()
    {

        projectNameArray?.add("All Projects")
        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, projectNameArray)

        spinner_projects?.adapter = spinnerProjectAdapter
        for(i in 0..UserDataManager?.allProjectsList!!.size - 1)
        {
            if(UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false)
            {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }


        spinner_projects?.adapter = spinnerProjectAdapter


        categoryNameArray?.add("All Categories")
        var spinnerCategoryAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,categoryNameArray)
        spinner_categories?.adapter = spinnerCategoryAdapter

        for(i in 0..UserDataManager?.companyCategoriesList!!.size - 1)
        {
            if(UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()?.equals(null) == false)
            {
                categoryNameArray?.add(UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()!!)

            }

        }


        spinner_categories?.adapter = spinnerCategoryAdapter




        transactionTypeArray?.add("All Types")

        var spinnerTypeAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,transactionTypeArray)

        spinner_type?.adapter = spinnerTypeAdapter

        for(i in 0..UserDataManager?.transactionTypeList!!.size - 1)
        {

            transactionTypeArray?.add(UserDataManager?.transactionTypeList?.get(i)?.getTransactionType()!!)


        }

        spinnerTypeAdapter?.notifyDataSetChanged()


        var spinnerDateAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, resources.getStringArray(R.array.array_datefilter))

        spinner_date?.adapter = spinnerDateAdapter


    }

    fun setCurrentDate() {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_fromdate?.setText(resources.getString(R.string.fromDate))
        input_todate?.setText(resources.getString(R.string.toDate))

        currentDate = formattedDate
    }

    fun setUpFromDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_fromDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth )
        }, y, m, d)
        dpd.show()
    }

    fun setUpToDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_toDate?.setText(year.toString() + "/" +  (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)
        dpd.show()
    }

    fun deleteTransaction()
    {
// UrlBuilder.setAllTransactionsUrl(companyid,projectID,categoryID,transactionTypeID,fromDate,toDate,applyDateFilter,

        var transactionID = UserDataManager?.allTransactionsList?.get(item_position!!)?.transactionID

        var requestParameters: LinkedList<NameValuePair> = LinkedList()

        requestParameters.add(BasicNameValuePair(Parameters.CompanyId,loginManager?.getCompanyID().toString()))
        requestParameters.add(BasicNameValuePair(Parameters.TransactionId,transactionID.toString()))



        UrlBuilder?.setUrl(Module.bookkeeping,RequestCode.deletetransaction,requestParameters)

        Log.d("response",UrlBuilder.getUrl())
        var request = object : JsonObjectRequest(Method.GET,UrlBuilder.getUrl(),null,Response.Listener<JSONObject> {

            response ->

            Log.d("response",response.toString())
            if(response.getBoolean(JsonKeys.objects.KEY_STATUS).equals(false))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
                loader?.visibility = View.GONE

                AlertManager(resources.getString(R.string.errorDeletingTransaction),2000,this@AllTransactions)

            }
            else if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.success)))
            {
                //progressDialog?.dismiss()
                //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                //alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)
               /* loader?.visibility = View.GONE
                alert(resources.getString(R.string.NoDataFound))*/



                fromDate = "1918/01/01"
                toDate = currentDate.toString()

                getAllTransctions(loginManager?.getCompanyID(),"","","",fromDate,toDate,"","")


            }


        },
                Response.ErrorListener {
                    error ->
                    loader?.visibility = View.GONE


                    Log.d("response","error: " +error.message.toString())


                    alert(resources.getString(R.string.networkProblem))
                }
        ){}

        ApplicationController.instance?.addToRequestQueue(request)
    }

}
