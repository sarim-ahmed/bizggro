package com.averox.bizggro.Activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.util.Base64
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import com.averox.bizggro.UserManagement.UserDataManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.util.*
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList


class NewTransaction : AppCompatActivity(),View.OnClickListener {


    private var toolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var loginManager: LoginManager? = null
    private var input_date: TextView? = null
    private var layout_date: LinearLayout? = null
    private var input_description: EditText? = null
    private var input_amount: EditText? = null
    private var spinner_project: Spinner? = null
    private var spinner_category: Spinner? = null
    private var spinner_type: Spinner? = null
    private var spinner_invoice_po: Spinner? = null
    private var projectNameArray: ArrayList<String>? = null
    private var categoryNameArray: ArrayList<String>? = null
    private var invoiceNameArray: ArrayList<String>? = null
    private var transactionTypeArray: ArrayList<String>? = null
    private var PONameArray: ArrayList<String>? = null
    private var heading_invoice_po: TextView? = null
    private var button_submit: Button? = null
    private var description: String = ""
    private var amount: String = ""
    private var date: String = ""
    private var projectID: Any? = ""
    private var categoryID: Any? = ""
    private var transactionTypeID: Int? = null
    private var type: String = ""
    private var invoice: String = ""
    private var po: String = ""
    private var progressDialog: ProgressDialog? = null
    private var layout_spinner_invoice_po: LinearLayout? = null
    private var title: String? = null
    private var attachment: Boolean = false
    private lateinit var marshMallowPermission: MarshMallowPermission
    private var imageFileuri: Uri? = null
    private var bitmap_object: Bitmap? = null
    private var imageFile: File? = null
    private var RESULT_LOAD_CAMERA = 0
    private var RESULT_LOAD_IMAGE = 1
    private var filename: String? = null
    private var base64String: String = ""
    private var bitmap: Bitmap? = null
    private var attachment_view: ImageView? = null
    private var fileSize: Long = 0
    private var fileName: String = ""
    private var position: Int? = null
    private var screenTitle: String = ""


    private var project: Any? = ""
    private var category: Any? = ""
    private var invoice_no: Any? = ""
    private var po_no: Any? = ""
    private var document_url: String? = null
    private var transactionID: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_transaction)

        super.setTitle("")

        toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(toolbar)

        toolbar_title = findViewById(R.id.toolbar_title)


        loginManager = LoginManager(this)



        marshMallowPermission = MarshMallowPermission(this)


        input_date = findViewById(R.id.input_date)
        input_date?.setOnClickListener(this)

        projectNameArray = ArrayList()
        categoryNameArray = ArrayList()
        invoiceNameArray = ArrayList()
        PONameArray = ArrayList()
        transactionTypeArray = ArrayList()

        input_description = findViewById(R.id.input_description)
        input_amount = findViewById(R.id.input_amount)
        spinner_project = findViewById(R.id.spinner_project)
        spinner_category = findViewById(R.id.spinner_category)
        spinner_invoice_po = findViewById(R.id.spinner_invoice_po)

        spinner_invoice_po?.visibility = View.GONE
        heading_invoice_po?.visibility = View.GONE
        layout_spinner_invoice_po?.visibility = View.GONE

        spinner_type = findViewById(R.id.spinner_type)

        button_submit = findViewById(R.id.button_submit)
        button_submit?.setOnClickListener(this)

        layout_date = findViewById(R.id.layout_date)
        layout_date?.setOnClickListener(this)

        attachment_view = findViewById(R.id.attachment_view)
        attachment_view?.setOnClickListener(this)







        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy/MM/dd")
        val formattedDate = df.format(c.time)

        input_date?.setText(formattedDate)

        loadSpinnerValues()

        spinner_project?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position.equals(0)) {
                    projectID = ""
                } else {
                    projectID = UserDataManager.allProjectsList?.get(position - 1)?.getProjectID()

                    Log.d("newTransaction", "projectId: " + projectID)
                }
            }

        }


        spinner_category?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position.equals(0)) {
                    categoryID = ""
                } else {
                    categoryID = UserDataManager.companyCategoriesList?.get(position - 1)?.getCategoryID()
                    Log.d("newTransaction", "categorytId: " + categoryID)
                }

            }

        }

        spinner_invoice_po?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d("", "")
            }
        }


        spinner_type?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {


                /*spinner_invoice_po?.visibility = View.VISIBLE
                    heading_invoice_po?.visibility = View.VISIBLE
                    layout_spinner_invoice_po?.visibility = View.VISIBLE*/
                transactionTypeID = UserDataManager.transactionTypeList?.get(position)?.getTransactionID()
                if (spinner_type?.getItemAtPosition(position)!!.equals("Income")) {
                    heading_invoice_po?.setText(resources.getText(R.string.attach_invoice))

                    for (i in 0..UserDataManager?.allInvoicesList!!.size - 1) {
                        if (UserDataManager?.allInvoicesList?.get(i)?.getProjectName()?.equals(null) == false) {
                            invoiceNameArray?.add(UserDataManager?.allInvoicesList?.get(i)?.getInvoiceNumber()!!)

                        }

                    }

                    var invoiceNameAdapter = ArrayAdapter(this@NewTransaction, R.layout.support_simple_spinner_dropdown_item, invoiceNameArray)

                    spinner_invoice_po?.adapter = invoiceNameAdapter

                } else if (spinner_type?.getItemAtPosition(position)!!.equals("Expense")) {
                    heading_invoice_po?.setText(resources.getText(R.string.attach_po))

                    for (i in 0..UserDataManager?.allPOsList!!.size - 1) {
                        if (UserDataManager?.allPOsList?.get(i)?.getProjectName()?.equals(null) == false) {
                            PONameArray?.add(UserDataManager?.allPOsList?.get(i)?.getPoNumber()!!)

                        }


                        var invoiceNameAdapter = ArrayAdapter(this@NewTransaction, R.layout.support_simple_spinner_dropdown_item, PONameArray)

                        spinner_invoice_po?.adapter = invoiceNameAdapter

                    }
                }
            }

        }

        try {
            var intent = intent
            var extras = intent.extras

            screenTitle = extras!!.getString("TITLE")

            Log.d("response",""+screenTitle)

            toolbar_title?.setText(screenTitle)

            if (screenTitle!!.equals(resources.getString(R.string.string_title_edittransaction))) {

                getTransactionData()
                position = extras?.getInt("POSITION")

                transactionID = UserDataManager?.allTransactionsList?.get(position!!)?.transactionID as Int


            }


        } catch (e: Exception) {

            Log.d("response","intent exception: "+e.message)
        } finally {
        }
    }

    override fun onResume() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera()

                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage()
                }
            }
        } catch (e: Exception) {
        } finally {
        }

        super.onResume()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.layout_date -> {
                setUpDate()
            }
            R.id.button_submit -> {

                description = input_description?.text.toString()
                amount = input_amount?.text.toString()
                date = input_date?.text.toString()

                if (description?.isEmpty() || description?.equals("") || description?.equals(" ") || description?.equals(null)) {
                    input_description?.setError(resources.getString(R.string.error_description_empty))
                    input_description?.isFocusable
                } else if (amount?.isEmpty() || amount?.equals(null) || amount?.substring(0)?.equals(0)) {
                    input_amount?.setError(resources.getString(R.string.error_amount_empty))
                    input_amount?.isFocusable

                } else if (spinner_category!!.selectedItemPosition == 0) {
                    AlertManager("Select Category", 4000, this@NewTransaction)
                } else if (spinner_project!!.selectedItemPosition == 0) {
                    AlertManager("Select Project", 4000, this@NewTransaction)
                } else {

                    try {
                    } catch (e: Exception) {
                    } finally {
                    }

                    if (screenTitle?.equals(resources.getString(R.string.string_title_newtransaction))) {
                        addTransaction(description, amount, date, categoryID, projectID, transactionTypeID, loginManager?.getCompanyID(), attachment, base64String.toString())

                    } else if (screenTitle?.equals(resources.getString(R.string.string_title_edittransaction))) {

                        updateTransaction(description, amount, date, categoryID, projectID, transactionTypeID, loginManager?.getCompanyID(), attachment, base64String.toString())


                    }

                }
            }

            R.id.attachment_view -> {
                registerForContextMenu(attachment_view)
                attachment_view?.showContextMenu()
            }
        }

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if (attachment.equals(false)) {

            getMenuInflater().inflate(R.menu.new_attachment_menu, menu)
        } else if (attachment.equals(true)) {
            getMenuInflater().inflate(R.menu.attachment_menu, menu)

        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        // val info = item?.getMenuInfo() as AdapterContextMenuInfo

        when (item?.itemId) {
            R.id.context_camera -> {
                openCamera()
                return true
            }

            R.id.context_gallery -> {
                openGallery()
                return true
            }

            R.id.context_remove_attachment -> {
                try {
                    attachment_view?.setImageDrawable(null)
                    attachment_view?.setImageBitmap(null)
                    base64String = ""

                    attachment_view?.setImageDrawable(resources.getDrawable(R.drawable.attach_more))
                } catch (e: Exception) {
                } finally {
                }
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }


    fun openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!marshMallowPermission.checkPermissionForCamera()) {
                marshMallowPermission.requestPermissionForCamera()

            } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage()
            } else {
                /* if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                     marshMallowPermission.requestPermissionForExternalStorage()
                 }*/
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val mediaStorageDir = File(
                        Environment.getExternalStorageDirectory().toString()
                                + File.separator
                                + "abc"
                                + File.separator
                                + "abc"
                )



                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs()
                }

                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(Date())
                try {
                    imageFile = File.createTempFile(
                            "IMG_" + timeStamp, /* prefix */
                            ".jpg", /* suffix */
                            mediaStorageDir      /* directory */
                    )



                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
                    startActivityForResult(takePictureIntent, RESULT_LOAD_CAMERA)
                } catch (e: IOException) {
                    Log.d("camera", e.message)
                    Log.d("camera", e.printStackTrace().toString())
                } finally {
                    Log.d("camera", "final")
                }
            }
        } else {
            val camera_intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            imageFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "test.jpg")
            imageFileuri = Uri.fromFile(imageFile)
            camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileuri)
            camera_intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            try {
                if (camera_intent.resolveActivity(getPackageManager()) != null) {

                    startActivityForResult(camera_intent, RESULT_LOAD_CAMERA)
                } else {
                    Log.d("Imagelog", "package manager null")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("Camera Activity", e.message)

            } finally {
                Log.d("Camera Activity", "final")

            }
        }
    }


    fun openGallery() {

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage()
        } else {
            val gallery_intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(gallery_intent, RESULT_LOAD_IMAGE)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0) {
            when (resultCode) {
                Activity.RESULT_OK -> if (imageFile?.exists() == true) {

                    if (bitmap_object != null) {
                        bitmap_object = null
                    } else if (bitmap != null) {
                        bitmap = null
                    }
                    // bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(imageFile?.getAbsolutePath()), 1200)

                    // bitmap_object = BitmapFactory.decodeFile(imageFile?.absolutePath)

                    bitmap_object = getRotatedBitmap(imageFile!!.absolutePath)

                    //bitmap_object = scaleBitmapDown(bitmap_object!!, 1200)


                    fileName = imageFile!!.name

                    // base64String = getBase64(bitmap_object!!)

                    var a = GetBase64String(scaleBitmapDown(bitmap_object!!, 1200)!!)
                    a.execute()


                    saveImage(bitmap_object!!)

                    attachment_view?.setImageBitmap(bitmap_object)


                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    /*Glide.with(this@GeneralPost)
                            .load(Uri.fromFile(imageFile))
                            .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                            .into(imageview_add_image)*/
                    //filename = "bitmap.png"


                    //imageview_add_image?.setImageBitmap(bitmap_object)
                    //picture_string = getBase64(bitmap_object!!)!!
                    // var stream = this.openFileOutput(filename, Context.MODE_PRIVATE)
                    // bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream)


                    //Cleanup
                    // stream.close()
                    // bitmap_object?.recycle();

                    // var load = SetImageFromBitmap(getRotatedBitmap(imageFile?.absolutePath!!), imageview_image!!)
                    // load.execute()
                    //Log.d("Imagelog", "base 64: "+getBase64(bitmap_object!!))
                    //Toast.makeText(this,"Picture was saved at "+imageFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
                    //Log.d("Imagelog", "Picture was saved at " + imageFile?.getAbsolutePath())
                } else {
                    Toast.makeText(this, "Error loading Picture", Toast.LENGTH_LONG).show()
                    Log.d("Imagelog", "Error loading Picture")
                }
                Activity.RESULT_CANCELED ->
                    //Toast.makeText(this,"Result canceled",Toast.LENGTH_LONG).show();
                    Log.d("Imagelog", "Result canceled")
            }
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor = contentResolver.query(selectedImage,
                            filePathColumn, null, null, null)
                    cursor!!.moveToFirst()


                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    var picturePath = cursor.getString(columnIndex)
                    cursor.close()


                    // imageByteArray = convertToByteArray(bitmap_object!!)
                    if (bitmap_object != null) {
                        bitmap_object = null

                    } else if (bitmap != null) {
                        bitmap = null
                    }


                    // bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(picturePath),1200)
                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    bitmap_object = BitmapFactory.decodeFile(picturePath)

                    bitmap_object = scaleBitmapDown(bitmap_object!!, 1200)


                    var a = GetBase64String(bitmap_object!!)
                    a.execute()


                    fileName = picturePath



                    attachment_view?.setImageBitmap(bitmap_object)

                    //filename = "bitmap.png"


                    //Write file
                    //var stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
                    //bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    //stream.close();

                    // var load = SetImageFromBitmap(getRotatedBitmap(picturePath), imageview_image!!)
                    //load.execute()
                    //bitmap_object?.recycle();


                    //picture_string = getBase64(compressBitmap(bitmap_object!!))


                    //Log.d("Imagelog", "base 64 image load: "+getBase64(bitmap_object!!))


                    // Log.d("Imagelog",imageview_add_image?.toString())

                    //imageview_add_image?.setImageBitmap(compressBitmap(bitmap_object!!))
                    //Toast.makeText(getApplicationContext(),picturePath,Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    fun saveImage(image: Bitmap): String {
        var savedImagePath: String? = null

        var imageFileName = "IMG" + getTimeStamp() + ".jpg"
        var storageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "/Bizggro")
        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            var imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.getAbsolutePath()
            try {
                var fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            // Add the image to the system gallery
            galleryAddPic(savedImagePath)
            toast("Image saved into folder: FileDownloadTask in Gallery")
        } else {
            toast("ERROR SAVING IMAGE")
        }
        return savedImagePath!!
    }

    fun galleryAddPic(imagePath: String) {
        var mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        var f = File(imagePath)
        var contentUri = Uri.fromFile(f)
        mediaScanIntent.setData(contentUri)
        sendBroadcast(mediaScanIntent)
    }

    fun getTimeStamp(): String? {
        val tsLong = System.currentTimeMillis() / 1000
        val ts = tsLong.toString()

        return ts
    }

    fun getBase64(bitmap: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    private inner class GetBase64String(bitmap: Bitmap) : AsyncTask<Void, Void, Void>() {
        var bitmap: Bitmap? = null
        val outputStream = ByteArrayOutputStream()
        var base64: String = ""


        init {

            this.bitmap = bitmap
            Log.d("response", "in constructor ")


        }

        override fun onPreExecute() {

            progressDialog = ProgressDialog.show(this@NewTransaction, "Loading attachment..", resources.getString(R.string.pleaseWait), true)
            progressDialog?.setCancelable(false)

            Log.d("response", "pre execute ")

        }

        override fun doInBackground(vararg params: Void?): Void? {

            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
            Log.d("response", "do in background: " + base64.toString())
            Log.d("response", "do in background: ")


            return null
        }

        override fun onPostExecute(result: Void?) {

            base64String = base64

            attachment = true
            fileSize = bitmap?.byteCount?.toLong()!!
            Log.d("response", "post execute: ")

            Log.d("response", "post execute: " + base64String.toString())
            progressDialog?.dismiss()

        }

    }

    fun getRotatedBitmap(imagePath: String): Bitmap {

        val f = File(imagePath)
        val exif = ExifInterface(f.getPath())
        val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        var angle = 0

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            angle = 90
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            angle = 180
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            angle = 270
        }

        val mat = Matrix()
        mat.postRotate(angle.toFloat())

        val bmp = BitmapFactory.decodeStream(FileInputStream(f), null, null)
        val correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, mat, true)

        return correctBmp

    }

    private fun setUpDate() {
        val calendar = Calendar.getInstance()
        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(
                this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            input_date?.setText(year.toString() + "/" + (monthOfYear + 1).toString() + "/" + dayOfMonth)
        }, y, m, d)

        dpd.show()


    }

    fun loadSpinnerValues() {

        projectNameArray?.add("Select Project")
        for (i in 0..UserDataManager?.allProjectsList!!.size - 1) {
            if (UserDataManager?.allProjectsList?.get(i)?.getProjectName()?.equals(null) == false) {
                projectNameArray?.add(UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!)

            }

        }

        var spinnerProjectAdapter = ArrayAdapter(this, R.layout.spinner_item_style, projectNameArray)

        spinner_project?.adapter = spinnerProjectAdapter



        categoryNameArray?.add("Select Category")

        for (i in 0..UserDataManager?.companyCategoriesList!!.size - 1) {
            if (UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()?.equals(null) == false) {
                categoryNameArray?.add(UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()!!)

            }

        }

        var spinnerCategoryAdapter = ArrayAdapter(this, R.layout.spinner_item_style, categoryNameArray)

        spinner_category?.adapter = spinnerCategoryAdapter


        /*   if(title?.equals(resources.getString(R.string.string_title_newincome))!!)
        {
            for(i in 0..UserDataManager?.allInvoicesList!!.size - 1)
            {
                if(UserDataManager?.allInvoicesList?.get(i)?.getProjectName()?.equals(null) == false)
                {
                    invoiceNameArray?.add(UserDataManager?.allInvoicesList?.get(i)?.getInvoiceNumber()!!)

                }

            }

            var invoiceNameAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,invoiceNameArray)

            spinner_invoice_po?.adapter = invoiceNameAdapter



        }
        else if(title?.equals(resources.getString(R.string.string_title_newexpense))!!)
        {
            for(i in 0..UserDataManager?.allPOsList!!.size - 1)
            {
                if(UserDataManager?.allPOsList?.get(i)?.getProjectName()?.equals(null) == false)
                {
                    PONameArray?.add(UserDataManager?.allPOsList?.get(i)?.getPoNumber()!!)

                }

            }

            var invoiceNameAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,PONameArray)

            spinner_invoice_po?.adapter = invoiceNameAdapter
        }*/




        for (i in 0..UserDataManager?.transactionTypeList!!.size - 1) {

            transactionTypeArray?.add(UserDataManager?.transactionTypeList?.get(i)?.getTransactionType()!!)


        }


        var spinnerTypeAdapter = ArrayAdapter(this, R.layout.spinner_item_style, transactionTypeArray)

        spinner_type?.adapter = spinnerTypeAdapter

    }


    fun addTransaction(description: String?, amount: String?, date: String?, categoryID: Any?, projectID: Any?, transactionTypeID: Int?, companyID: Int?, attachment: Boolean?, base64String: String?) {

        Log.d("response","in add transaction")

        progressDialog = ProgressDialog.show(this@NewTransaction, resources.getString(R.string.addingTransaction), resources.getString(R.string.pleaseWait), true)
        progressDialog?.setCancelable(false)
        //UrlBuilder?.setAddTransactionUrl(description,amount,date,categoryID,projectID,transactionTypeID,loginManager?.getCompanyID())

//        Log.d("response",UrlBuilder?.getAddTransactionUrl())

        var request = object : JsonObjectRequest(Method.POST, Constants.URL_ADDTRANSACTION, null, Response.Listener<JSONObject>
        { response ->

            Log.d("response", "response: " + response.toString())

            progressDialog?.dismiss()


            var description = response.getString(JsonKeys.variables.KEY_DESCRIPTION)


            if (description!!.equals("Success")) {

                alert(resources.getString(R.string.transactionAddedSuccessfully)).show()

                Handler().postDelayed(object : Runnable {

                    // Using handler with postDelayed called runnable run method


                    override fun run() {

                        var intent = Intent(this@NewTransaction, AllTransactions::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                        finish()
                    }
                }, 2 * 1000) // wait for 5 seconds

            } else {
                alert(resources.getString(R.string.errorAddingTransaction))
            }


        },
                Response.ErrorListener { error ->

                    Log.d("response", "error: " + error.message.toString())

                    progressDialog?.dismiss()

                }
        ) {

            override fun getBody(): ByteArray {

                var jsonObject = JSONObject()


                jsonObject.put(Parameters.Description, description)
                jsonObject.put(Parameters.Amount, amount)
                jsonObject.put(Parameters.Date, date)
                jsonObject.put(Parameters.CategoryID, categoryID)
                jsonObject.put(Parameters.ProjectID, projectID)
                jsonObject.put(Parameters.TransactionTypeID, transactionTypeID)
                jsonObject.put(Parameters.Attachment, fileName)
                jsonObject.put(Parameters.CompanyID, loginManager?.getCompanyID())
                jsonObject.put(Parameters.FileSize, fileSize)

                jsonObject.put(Parameters.AttachmentString, base64String)


                Log.d("response", jsonObject.toString())


                return jsonObject.toString().toByteArray()
            }
        }
        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController?.instance?.addToRequestQueue(request)
    }

    fun scaleBitmapDown(bitmap: Bitmap, maxDimension: Int): Bitmap {
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var resizedWidth = maxDimension
        var resizedHeight = maxDimension

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = (resizedHeight * originalWidth.toFloat() / originalHeight.toFloat()).toInt()
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension
            resizedHeight = (resizedWidth * originalHeight.toFloat() / originalWidth.toFloat()).toInt()
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = maxDimension
        }
        val a = "Width: $resizedWidth\nHeight: $resizedHeight"
        Log.d("imagedimen", a)
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false)

    }

    fun getTransactionData() {
        var intent = intent
        var extras = intent.extras
        position = extras?.getInt("ITEM_POSITION")


        transactionID = UserDataManager.allTransactionsList?.get(position!!)?.transactionID as Int
        description = UserDataManager.allTransactionsList?.get(position!!)?.description.toString()
        amount = UserDataManager.allTransactionsList?.get(position!!)?.amount.toString()
        date = UserDataManager.allTransactionsList?.get(position!!)?.date.toString()
        project = UserDataManager.allTransactionsList?.get(position!!)?.projectName
        category = UserDataManager.allTransactionsList?.get(position!!)?.categoryName
        type = UserDataManager.allTransactionsList?.get(position!!)?.transactionTypeName.toString()
        invoice_no = UserDataManager.allTransactionsList?.get(position!!)?.invoiceNumber
        po_no = UserDataManager.allTransactionsList?.get(position!!)?.poNumber
        document_url = UserDataManager.allTransactionsList?.get(position!!)?.documentUrl.toString()




        if ((document_url as String)?.isNullOrBlank()) {
            Log.d("response", "document url null ")
        } else {
            Log.d("response", "document url is not null ")

        }

        Log.d("response", "document url: " + document_url)
        fileName = UserDataManager.allTransactionsList?.get(position!!)?.documentName.toString()
        Log.d("response", "fileName: " + fileName)

        input_description?.setText(description)
        input_amount?.setText(amount)
        input_date?.setText(date)

        for (i in 0..UserDataManager?.allProjectsList!!.size - 1) {
            if (UserDataManager?.allProjectsList?.get(i)?.getProjectName()!!.equals(project)) {

                var index = i + 1
                spinner_project?.setSelection(index)

            }

        }

        for (i in 0..UserDataManager?.companyCategoriesList!!.size - 1) {
            if (UserDataManager?.companyCategoriesList?.get(i)?.getCategoryName()!!.equals(category)) {

                var index = i + 1
                spinner_category?.setSelection(index)

            }

        }

        for (i in 0..UserDataManager?.transactionTypeList!!.size - 1) {
            if (UserDataManager?.transactionTypeList?.get(i)?.getTransactionType()!!.equals(type)) {


                spinner_type?.setSelection(i)

            }

        }

        try {

            /*     if(document_url!!.toString()!!.equals(null) || document_url!!.toString()!!.equals(""))
            {
                Log.d("response","in if ")



            }
            else
            {

                Log.d("response","in else ")

                loadImage(document_url.toString())
            }
*/
            loadImage(document_url.toString())

        } catch (e: Exception) {

            Log.d("response", "image exception: " + e.message)

        } finally {
        }
    }

    fun loadImage(documentURL: String?) {

        var a = SetImageFromUrl(documentURL!!, attachment_view!!)
        a.execute()


    }

    private inner class SetImageFromUrl constructor(documentUrl: String, view: ImageView) : AsyncTask<Object, Object, Object>() {

        private var documentUrl: String? = null
        private var view: ImageView? = null


        init {
            this.documentUrl = documentUrl
            this.view = view

            Log.d("response", "document url in async task: " + documentUrl)


        }


        override fun onPreExecute() {

            Log.d("load", "in pre execute")
            Log.d("resumecalled", "in pre  execute ")

            try {


            } catch (e: Exception) {
                e.printStackTrace();
            }


            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Object?): Object? {

            try {

                Log.d("load", "in do in background")

                Log.d("resumecalled", "in do in background execute ")



                bitmap = Glide.with(this@NewTransaction)
                        .load(documentUrl).asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(Int.MAX_VALUE, Int.MAX_VALUE).get()


            } catch (e: Exception) {
                e.printStackTrace();
            }
            return null;
        }

        override fun onPostExecute(result: Object?) {

            Log.d("load", "on post execute")

            Log.d("resumecalled", "in post execute ")


            try {


                attachment_view?.setImageBitmap(bitmap)

                var a = GetBase64String(bitmap!!)
                a.execute()

                fileSize = bitmap?.byteCount!!.toLong()

                Log.d("response", "filesize: " + fileSize)


                saveImage(bitmap!!)


            } catch (e: Exception) {
                Log.d("img", "" + e.message)
                Log.d("img", "" + e.printStackTrace().toString())

            } finally {
            }


        }
    }

        fun updateTransaction(description: String?, amount: String?, date: String?, categoryID: Any?, projectID: Any?, transactionTypeID: Int?, companyID: Int?, attachment: Boolean?, base64String: String?) {



            Log.d("response","in update transaction")
            progressDialog = ProgressDialog.show(this@NewTransaction, resources.getString(R.string.updatingTransaction), resources.getString(R.string.pleaseWait), true)
            progressDialog?.setCancelable(false)
            //UrlBuilder?.setAddTransactionUrl(description,amount,date,categoryID,projectID,transactionTypeID,loginManager?.getCompanyID())

//        Log.d("response",UrlBuilder?.getAddTransactionUrl())

            var request = object : JsonObjectRequest(Method.POST, Constants.URL_UPDATETRANSACTION, null, Response.Listener<JSONObject>
            { response ->

                Log.d("response", "response: " + response.toString())

                progressDialog?.dismiss()


                var description = response.getString(JsonKeys.variables.KEY_DESCRIPTION)


                if (description!!.equals("Success")) {

                    alert(resources.getString(R.string.transactionUpdatedSuccessfully)).show()

                    Handler().postDelayed(object : Runnable {

                        // Using handler with postDelayed called runnable run method


                        override fun run() {

                            var intent = Intent(this@NewTransaction, AllTransactions::class.java)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                            finish()
                        }
                    }, 2 * 1000) // wait for 5 seconds

                } else {
                    alert(resources.getString(R.string.errorUpdatingTransaction))
                }


            },
                    Response.ErrorListener { error ->

                        Log.d("response", "error: " + error.message.toString())

                        progressDialog?.dismiss()

                    }
            ) {

                override fun getBody(): ByteArray {

                    var jsonObject = JSONObject()

                    jsonObject.put(Parameters.TransactionId, transactionID)
                    jsonObject.put(Parameters.Description, description)
                    jsonObject.put(Parameters.Amount, amount)
                    jsonObject.put(Parameters.Date, date)
                    jsonObject.put(Parameters.CategoryID, categoryID)
                    jsonObject.put(Parameters.ProjectID, projectID)
                    jsonObject.put(Parameters.TransactionTypeID, transactionTypeID)
                    jsonObject.put(Parameters.Attachment, fileName)
                    jsonObject.put(Parameters.CompanyID, loginManager?.getCompanyID())
                    jsonObject.put(Parameters.FileSize, fileSize)
                    jsonObject.put(Parameters.AttachmentString, base64String)

                    Log.d("response", jsonObject.toString())

                    return jsonObject.toString().toByteArray()
                }
            }
            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController?.instance?.addToRequestQueue(request)


        }



}
