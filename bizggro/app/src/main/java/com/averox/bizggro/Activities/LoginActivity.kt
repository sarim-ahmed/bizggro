package com.averox.bizggro.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.averox.bizggro.BroadcastReceivers.ConnectivityReceiver
import com.averox.bizggro.Controller.ApplicationController
import com.averox.bizggro.HelperClasses.*
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.LoginManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.*

class LoginActivity : AppCompatActivity(),View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {


    private var email_view: AutoCompleteTextView? = null
    private var password_view: EditText? = null
    private var loginButton: Button? = null
    private var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private var alert: AlertManager? = null
    private var progressDialog: ProgressDialog? = null
    private var loginManager: LoginManager? = null
    private lateinit var marshMallowPermission: MarshMallowPermission

    private var input_email: String? = null
    private var input_password: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        email_view = findViewById(R.id.input_email)
        password_view = findViewById(R.id.input_password)
        loginButton = findViewById(R.id.loginButton)
        loginButton?.setOnClickListener(this)
        marshMallowPermission = MarshMallowPermission(this)
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }

        loginManager = LoginManager(Constants.context!!)


        if(loginManager!!.isLogedIn())
        {
            val MainActivityIntent = Intent(this, MainActivity::class.java)

            startActivity(MainActivityIntent)

            finish()
            unregisterReceiver(ConnectionReceiver)

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Log.d("response","device greater than marshmallow")
            if (!marshMallowPermission.checkPermissionForInternet()) {
                Log.d("response","no internet permission")

                marshMallowPermission.requestPermissionForInternet()

            }
        }


    }

    override fun onResume() {

     /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!marshMallowPermission.checkPermissionForInternetPermission()) {
                marshMallowPermission.requestPermissionForInternet()

            }
        }*/
        super.onResume()

        ApplicationController.instance?.setConnectivityListener(this)

    }

    override fun onPause() {
        super.onPause()
        try {
            unregisterReceiver(ConnectionReceiver)
        } catch (e: Exception) {
            Log.d("loginexception",""+e.message.toString())
        } finally {

            Log.d("loginexception",""+"final block")

        }

    }

    override fun onClick(v: View?) {

    when(v?.id)
    {
        R.id.loginButton->
        {
            if(!isInternetConnected)
            {
                toast(resources.getString(R.string.please_check_internet_connection))
            }
            else
            {

                input_email = Uri.encode(email_view!!.text.toString().trim(), "UTF-8")
                input_password = Uri.encode(password_view!!.text.toString(), "UTF-8")

                processUserLogin(input_email!!,input_password!!)
            }
        }
    }
    }

    fun processUserLogin(input_email: String,input_password: String)
    {

     /*   var input_email = Uri.encode(email_view!!.text.toString().trim(), "UTF-8")
        var input_password = Uri.encode(password_view!!.text.toString(), "UTF-8")*/

        var email: String? = null
        var password: String? = null
        var userid: Int? = null
        var usertype: Int? = null
        var companyname: String? = null
        var companyid: Int? = null
        var firstname: String? = null
        var lastname: String? = null

         if(input_email!!.isEmpty())
        {
            email_view?.setError(resources.getString(R.string.this_field_is_required))
        }
        else if(input_password!!.isEmpty())
        {
            password_view?.setError(resources.getString(R.string.this_field_is_required))
        }
            else
        {

            try
            {
                progressDialog = ProgressDialog.show(this@LoginActivity, "Signing in", "Please wait....", true)
                progressDialog?.setCancelable(false)
            }
            catch (e: Exception)
            {

            }
            finally
            {

            }

            UrlBuilder.setLoginUrl(input_email!!, input_password!!)
            Log.d("url", UrlBuilder.getLoginUrl())


            var request = object : JsonObjectRequest(Method.GET, UrlBuilder.getLoginUrl(), null, Response.Listener<JSONObject> {

                response ->

                try {
                    //Log.d("response", response.getJSONObject("data").toString())

                    //alert(response.getString("ID")).show()

                    if(response.getString(JsonKeys.objects.KEY_DESCRIPTION).equals(resources.getString(R.string.NoDataFound)))
                    {
                        progressDialog?.dismiss()

                        //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                        alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)

                    }
                    else {
                        val data = response.optJSONArray(JsonKeys.arrays.KEY_DATA)


                            var result = data.getJSONObject(0)



                            email = result.getString(JsonKeys.variables.KEY_EMAIL)
                            password = result.getString(JsonKeys.variables.KEY_PASSWORD)
                            userid = result.getInt(JsonKeys.variables.KEY_USERID)
                            usertype = result.getInt(JsonKeys.variables.KEY_USERTYPE)
                            companyname = result.getString(JsonKeys.variables.KEY_COMPANYNAME)
                            companyid = result.getInt(JsonKeys.variables.KEY_COMPANYID)
                            firstname = result.getString(JsonKeys.variables.KEY_FIRSTNAME)
                            lastname = result.getString(JsonKeys.variables.KEY_LASTNAME)



                        //alert(" "+id).show()

                        try {



                                loginManager?.createLoginSession(email, password, userid, firstname, lastname,usertype, companyname,companyid)

                                loginManager?.savePass(password)

                                if (loginManager!!.isLoggedIn) {
                                    //progressDialog?.dismiss()

                                    val intent = Intent(this, MainActivity::class.java)
                                    startActivity(intent)
                                    overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)

                                    finish()


                                } else {

                                    Log.d("response","in login else ")

                                    progressDialog?.dismiss()

                                    var alert = AlertManager("Network Problem!", 3000, this)

                                }


                        }

                        catch(e: Exception)
                        {

                            Log.d("response","in login request exception ")

                            Log.d("response","error: "+e.message)
                            var alert = AlertManager("Network Problem!",3000,this)
                            Log.d("response", "error "+e.printStackTrace().toString())

                        }




                    }

                    // alert(result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME)).show()
                    // Toast.makeText(this,result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME), Toast.LENGTH_LONG).show()

                } catch(e: Exception) {
                    progressDialog?.dismiss()

                    var alert = AlertManager("Network Problem!",3000,this)
                    Log.d("response","in volley exception ")


                    Log.d("response",""+e.message)
                    Log.d("response", "error "+e.printStackTrace().toString())

                }


            },
                    Response.ErrorListener {

                        error ->

                        //progressDialog?.dismiss()
                        try {
                            progressDialog?.dismiss()
                        } catch (e: Exception) {
                        } finally {
                        }

                        Log.d("response","in volley error ")

                        var alert = AlertManager("Network Problem!",3000,this)

                        Log.d("response", "error "+error.message)
                        //Toast.makeText(this, "error " +error.message, Toast.LENGTH_LONG).show()


                    }
            ) {}

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController.instance?.addToRequestQueue(request)

            // progressDialog?.dismiss()



        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services

        } else if (!isConnected) {
            //implement here for no network services

        }
    }


}
