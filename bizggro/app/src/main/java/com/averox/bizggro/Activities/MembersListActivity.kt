package com.averox.bizggro.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.UserDataManager
import kotlinx.android.synthetic.main.activity_members_list.*

class MembersListActivity : AppCompatActivity() {

    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null

    private var listView_members: ListView? = null

    private var title: String? = null

    private var membersNameArray: ArrayList<String>? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_members_list)

        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")



        toolbar_title = findViewById(R.id.toolbar_title)


        var intent = intent
        var extras = intent.extras

        title = extras?.getString("TITLE")

        UserDataManager?.type = title


        super.setTitle(title)




        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        listView_members = findViewById(R.id.members_listview)
        listView_members?.choiceMode = ListView.CHOICE_MODE_MULTIPLE


        membersNameArray = ArrayList()

        loadData()


        listView_members?.setOnItemClickListener { parent, view, position, id ->


            if(title!!.equals("TO")) {

                if (listView_members?.isItemChecked(position) == true) {

                    UserDataManager.arraylist_selected_userIDs_To?.add(UserDataManager?.groupMembersList?.get(position)?.userID as Int)


                } else if (listView_members?.isItemChecked(position) == false) {
                    if (UserDataManager.arraylist_selected_userIDs_To?.contains(UserDataManager?.groupMembersList?.get(position)?.userID as Int) == true) {
                        UserDataManager.arraylist_selected_userIDs_To?.remove(UserDataManager?.groupMembersList?.get(position)?.userID as Int)
                    }
                }
            }
            else if(title!!.equals("CC"))
            {
                if (listView_members?.isItemChecked(position) == true) {

                    UserDataManager.arraylist_selected_userIDs_Cc?.add(UserDataManager?.groupMembersList?.get(position)?.userID as Int)


                } else if (listView_members?.isItemChecked(position) == false) {
                    if (UserDataManager.arraylist_selected_userIDs_Cc?.contains(UserDataManager?.groupMembersList?.get(position)?.userID as Int) == true) {
                        UserDataManager.arraylist_selected_userIDs_Cc?.remove(UserDataManager?.groupMembersList?.get(position)?.userID as Int)
                    }
                }
            }

        }


        setSelected()

       /* if(title!!.equals("TO")) {
            for (i in 0..UserDataManager?.arraylist_selected_userIDs_To!!.size - 1) {

                for (j in 0..UserDataManager?.groupMembersList!!.size - 1) {

                    if (UserDataManager?.groupMembersList?.get(j)?.userID!!.equals(UserDataManager?.arraylist_selected_userIDs_To?.get(i))) {

                        listView_members?.setSelection(j)

                    }
                }
            }
        }
        else if(title!!.equals("CC")) {
            for (i in 0..UserDataManager?.arraylist_selected_userIDs_Cc!!.size -1) {

                for(j in 0..UserDataManager?.groupMembersList!!.size - 1) {

                    if (UserDataManager?.groupMembersList?.get(j)?.userID!!.equals(UserDataManager?.arraylist_selected_userIDs_Cc?.get(i))) {

                        listView_members?.setSelection(j)

                    }

                }
            }
        }*/

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()

        }

        return super.onOptionsItemSelected(item)


    }


    override fun onBackPressed() {

        super.onBackPressed()

        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

        try {

            //UserDataManager?.allTransactionsList?.clear()

        } catch (e: Exception) {
        } finally {
        }

    }


    fun loadData()
    {

        for (i in 0..UserDataManager?.groupMembersList!!.size - 1) {
            if (UserDataManager?.groupMembersList?.get(i)?.fullName?.equals(null) == false) {
                membersNameArray?.add(UserDataManager?.groupMembersList?.get(i)?.fullName.toString()!!)

            }

        }

        val itemsAdapter = ArrayAdapter<String>(this, R.layout.members_list_item, membersNameArray)
        listView_members?.adapter = itemsAdapter

    }


    fun setSelected()
    {
        if(title!!.equals("TO")) {
            for (i in 0..UserDataManager?.arraylist_selected_userIDs_To!!.size - 1) {

                for (j in 0..UserDataManager?.groupMembersList!!.size - 1) {

                    if (UserDataManager?.groupMembersList?.get(j)?.userID!!.equals(UserDataManager?.arraylist_selected_userIDs_To?.get(i))) {

                        Log.d("response","in to select loop"+UserDataManager?.groupMembersList?.get(j)?.userID)
                        Log.d("response","in cc select loop"+j)
                        listView_members?.setItemChecked(j,true)
                    }
                }
            }
        }
        else if(title!!.equals("CC")) {
            for (i in 0..UserDataManager?.arraylist_selected_userIDs_Cc!!.size -1) {

                for(j in 0..UserDataManager?.groupMembersList!!.size - 1) {

                    if (UserDataManager?.groupMembersList?.get(j)?.userID!!.equals(UserDataManager?.arraylist_selected_userIDs_Cc?.get(i))) {

                        Log.d("response","in cc select loop"+UserDataManager?.groupMembersList?.get(j)?.userID)
                        Log.d("response","in cc select loop"+j)

                        listView_members?.setItemChecked(j,true)
                    }

                }
            }
        }
        }

}
