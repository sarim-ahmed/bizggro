package com.averox.bizggro.Controller

import android.annotation.SuppressLint
import android.app.Application
import android.os.StrictMode
import android.text.TextUtils
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.averox.bizggro.BroadcastReceivers.ConnectivityReceiver
import com.averox.bizggro.HelperClasses.Constants
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

/**
 * Created by Sarim on 11/29/2017.
 */
class ApplicationController : Application() {



    override fun onCreate() {
        super.onCreate()

        Constants.context = applicationContext
        instance = this
        handleSSLHandshake()

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())







    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(Constants.context)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }


    companion object {
        private val TAG = ApplicationController::class.java.simpleName
        @get:Synchronized var instance: ApplicationController? = null
            private set
    }



    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }

    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    fun handleSSLHandshake() {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }

                override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) {}

                override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) {}
            })

            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
            HttpsURLConnection.setDefaultHostnameVerifier(object : HostnameVerifier {
                override fun verify(arg0: String, arg1: SSLSession): Boolean {
                    if(arg0.equals("app.bizggro.com",true) || arg0.equals("api.bizggro.com",true)
                            || arg0.equals("dev.bizggro.com",true) )
                    {



                        return true
                    }
                    else
                    {
                        return false
                    }
                  //  return true

                }


            })
        } catch (ignored: Exception) {
        }

    }


}
