package com.averox.bizggro.Fragments

import android.app.Dialog
import android.app.Fragment
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.provider.SyncStateContract
import android.support.v4.app.ActivityCompat.finishAffinity
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.averox.bizggro.Activities.*
import com.averox.bizggro.Adapters.MainDrawerAdapter
import com.averox.bizggro.HelperClasses.Constants
import com.averox.bizggro.R
import com.averox.bizggro.UserManagement.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

/**
 * Created by Sarim on 12/5/2017.
 */

public class MainDrawerMenu: Fragment(),ExpandableListView.OnChildClickListener,View.OnClickListener


{



    var listAdapter: ExpandableListAdapter? = null
    var menuListView: ExpandableListView? = null
    var listDataHeader: List<String>? = null
    var listDataChild: HashMap<String, List<String>>? = null

    private var button_signout: LinearLayout? = null
    private var loginManager: LoginManager? = null


    private var main_menu_list: ExpandableListView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater?.inflate(R.layout.fragment_main_drawer, container, false)

        main_menu_list = view?.findViewById(R.id.main_menu_listview)
        button_signout = view?.findViewById(R.id.button_signout)
        button_signout?.setOnClickListener(this)

        return view!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loginManager = LoginManager(Constants.context!!)

        // get the listview
        menuListView = view.findViewById<ExpandableListView>(R.id.main_menu_listview)

        // preparing list data
        prepareListData()

        listAdapter = MainDrawerAdapter(Constants.context!!, listDataHeader!!, listDataChild!!)

        // setting list adapter
        menuListView?.setAdapter(listAdapter)

        menuListView?.setOnChildClickListener(this)

    }

    private fun prepareListData() {
        listDataHeader = ArrayList<String>()
        listDataChild = HashMap<String, List<String>>()

        // Adding child data

        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_customers))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_financials))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_contacts))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_leads))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_opportunities))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_task))
        (listDataHeader as ArrayList<String>).add(resources.getString(R.string.list_header_eSigning))




        // Adding child data
        val customers = ArrayList<String>()
        customers.add(resources.getString(R.string.list_item_allcustomers))
     /*   customers.add(resources.getString(R.string.list_item_managetypes))
        customers.add(resources.getString(R.string.list_item_manager_credit_worthiness))
        customers.add(resources.getString(R.string.list_item_managementpaymentterms))*/


        val financials = ArrayList<String>()
        financials.add(resources.getString(R.string.list_item_newtransaction))
        financials.add(resources.getString(R.string.list_item_alltransactions))
        financials.add(resources.getString(R.string.list_item_invoices))
        financials.add(resources.getString(R.string.list_item_subscriptions))
        financials.add(resources.getString(R.string.list_item_pos))
        financials.add(resources.getString(R.string.list_item_managecategories))
        financials.add(resources.getString(R.string.list_item_tax))

        val contacts = ArrayList<String>()

        contacts.add(resources.getString(R.string.list_item_allContacts))
        contacts.add(resources.getString(R.string.list_item_newContact))

        val leads = ArrayList<String>()

        leads.add(resources.getString(R.string.list_item_allLeads))
        leads.add(resources.getString(R.string.list_item_myLeads))

        val opportunities = ArrayList<String>()
        opportunities.add(resources.getString(R.string.allOpportunities))
        opportunities.add(resources.getString(R.string.myOpportunites))
        /*opportunities.add(resources.getString(R.string.stages))
        opportunities.add(resources.getString(R.string.types))
        opportunities.add(resources.getString(R.string.proposalStatus))
        opportunities.add(resources.getString(R.string.proposals))*/

        val task = ArrayList<String>()
        task.add(resources.getString(R.string.list_item_newTask))
        task.add(resources.getString(R.string.list_item_assignedByMe))
        task.add(resources.getString(R.string.list_item_assignedToMe))

        var eSigning = ArrayList<String>()
        eSigning?.add(resources.getString(R.string.list_item_signatureRequests))
        //eSigning?.add(resources.getString(R.string.list_item_manageSigature))


        listDataChild!!.put((listDataHeader as ArrayList<String>).get(0), customers) // Header, Child data
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(1), financials)
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(2), contacts)
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(3), leads)
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(4), opportunities)
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(5), task)
        listDataChild!!.put((listDataHeader as ArrayList<String>).get(6), eSigning)

    }

    override fun onChildClick(parent: ExpandableListView?, v: View?, groupPosition: Int, childPosition: Int, id: Long): Boolean {

        when(parent?.id) {
            R.id.main_menu_listview -> {
                if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_customers))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_allcustomers))) {
                        var intent = Intent(activity, AllCustomers::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                } else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_financials))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_alltransactions))) {
                        var intent = Intent(activity, AllTransactions::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_newtransaction))) {
                        var intent = Intent(activity, NewTransaction::class.java)
                        intent.putExtra("TITLE", resources.getString(R.string.string_title_newtransaction))
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_invoices))) {
                        var intent = Intent(activity, Invoices::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_managecategories))) {
                        var intent = Intent(activity, Categories::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_tax))) {
                        var intent = Intent(activity, ManageTax::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_pos))) {
                        var intent = Intent(activity, POs::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_subscriptions))) {
                        var intent = Intent(activity, Subscriptions::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                } else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_contacts))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_newContact))) {
                        var intent = Intent(activity, NewContact::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_allContacts))) {
                        var intent = Intent(activity, AllContacts::class.java)
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                } else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_leads))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_allLeads))) {
                        var intent = Intent(activity, LeadsList::class.java)
                        intent.putExtra("TOOLBAR_TITLE", resources.getString(R.string.All_Leads))
                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_myLeads))) {
                        var intent = Intent(activity, LeadsList::class.java)
                        intent.putExtra("TOOLBAR_TITLE", resources.getString(R.string.My_Leads))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                } else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_opportunities))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.allOpportunities))) {
                        var intent = Intent(activity, OpportunitiesList::class.java)
                        intent.putExtra("TOOLBAR_TITLE", resources.getString(R.string.allOpportunities))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    } else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.myOpportunites))) {
                        var intent = Intent(activity, OpportunitiesList::class.java)
                        intent.putExtra("TOOLBAR_TITLE", resources.getString(R.string.myOpportunites))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                } else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_task))) {
                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_newTask))) {
                        var intent = Intent(activity, NewTask::class.java)
                        //intent.putExtra("TOOLBAR_TITLE",resources.getString(R.string.allOpportunities))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_assignedByMe))) {
                        var intent = Intent(activity, TasksList::class.java)
                        intent.putExtra("TOOLBAR_TITLE",resources.getString(R.string.assignedByMe))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_assignedToMe))) {
                        var intent = Intent(activity, TasksList::class.java)
                        intent.putExtra("TOOLBAR_TITLE",resources.getString(R.string.assignedToMe))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    /*else if(listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.myOpportunites)))
                    {
                        var intent =  Intent(activity,OpportunitiesList::class.java)
                        intent.putExtra("TOOLBAR_TITLE",resources.getString(R.string.myOpportunites))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)
                    }*//*
                }*/
                }

                else if (listDataHeader?.get(groupPosition)!!.equals(resources.getString(R.string.list_header_eSigning))) {


                    if (listDataChild?.get(
                            listDataHeader!!.get(groupPosition))?.get(
                            childPosition)!!.equals(resources.getString(R.string.list_item_signatureRequests))) {
                        var intent = Intent(activity, SignatureRequest::class.java)
                        //intent.putExtra("TOOLBAR_TITLE",resources.getString(R.string.allOpportunities))

                        startActivity(intent)
                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if (listDataChild?.get(

                            listDataHeader!!.get(groupPosition))?.get(

                            childPosition)!!.equals(resources.getString(R.string.list_item_manageSigature)))
                    {

                        var intent = Intent(activity, SignatureRequest::class.java)

                        startActivity(intent)

                        activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

                    }


                }

            }
        }
            return true

    }

    override fun onClick(v: View?) {
when(v?.id)
{
    R.id.button_signout ->
    {
        try {

            val alertDialog = AlertDialog.Builder(activity).create()



            alertDialog?.setMessage("Are you sure you want to logout?")

            alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "Yes") { dialog, which ->

                try {
                    UserDataManager?.companyCategoriesList?.clear()
                    UserDataManager?.allInvoicesList?.clear()
                    UserDataManager?.allPOsList?.clear()
                    UserDataManager?.transactionTypeList?.clear()
                    UserDataManager?.incomeExpenseDataList?.clear()
                    UserDataManager?.transactionTypeList?.clear()
                    UserDataManager?.customerTypeList ?.clear()
                    ContactsDataManager?.countriesList?.clear()
                    ContactsDataManager?.categoriesList?.clear()
                    LeadsDataManager?.leadsStatusList?.clear()
                    OpportunitiesDataManager?.typesList?.clear()
                    OpportunitiesDataManager?.stagesList?.clear()
                    CustomersDataManager?.campaingsList?.clear()
                    CustomersDataManager?.contractsList?.clear()
                    TasksDataManager?.tasksStatus?.clear()

                } catch (e: Exception) {
                } finally {
                }

                loginManager?.logoutUser()
                activity.overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)
                finishAffinity(activity)

            }

            alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "No") { dialog, which ->

                alertDialog?.cancel()

            }

            alertDialog.show()




        } catch (e: Exception) {
        } finally {
        }
    }
}
    }
}