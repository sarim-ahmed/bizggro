package com.averox.bizggro.UserManagement

import com.averox.bizggro.Adapters.*

/**
 * Created by Sarim on 1/16/2018.
 */
public class CustomersDataManager
{
    companion object {

        public var basicInfoList: ArrayList<CustomerBasicDataAdapter>? = null
        public var socialInfoList: ArrayList<CustomerSocialProfilesAdapter>? = null
        public var productsList: ArrayList<CustomerProductsAdapter>? = null
        public var campaingsList: ArrayList<CustomerCampaignsAdapter>? = null
        public var transactionsList: ArrayList<CustomerTransactionandInvoicesAdpater>? = null
        public var posList: ArrayList<CustomerPOsAdapter>? = null
        public var newslettersList: ArrayList<CustomerNewslettersAdapter>? = null
        public var projectsList: ArrayList<CustomerProjectsAdapter>? = null
        public var contractsList: ArrayList<CustomerContractsAdapter>? = null
        public var tasksList: ArrayList<CustomerTasksAdapter>? = null
        public var eventsList: ArrayList<CustomerEventsAdapter>? = null

    init {

            basicInfoList = ArrayList<CustomerBasicDataAdapter>()
            socialInfoList = ArrayList<CustomerSocialProfilesAdapter>()
            productsList = ArrayList<CustomerProductsAdapter>()
            campaingsList = ArrayList<CustomerCampaignsAdapter>()
            transactionsList = ArrayList<CustomerTransactionandInvoicesAdpater>()
            posList = ArrayList<CustomerPOsAdapter>()
            newslettersList = ArrayList<CustomerNewslettersAdapter>()
            projectsList = ArrayList<CustomerProjectsAdapter>()
            contractsList = ArrayList<CustomerContractsAdapter>()
            tasksList = ArrayList<CustomerTasksAdapter>()
            eventsList = ArrayList<CustomerEventsAdapter>()

    }

        fun insertBasicInfo(contactID: Any?, firstname: Any?,lastname: Any?, email: Any?,phone: Any?,mobile: Any?,
                            address: Any?, creationdate: Any?,  city: Any?, state: Any?, countryid: Any?,countryName: Any?,
                            details: Any?,companyid: Any?, dateupdated: Any?, createdby: Any?, updatedby: Any?,
                            contacttypeId: Any?,companyname: Any?, categoryid: Any?,contactCategory: Any?, zipcode: Any?, ishidden: Any?
                            , customerid: Any?, convertedby: Any?,dateconverted: Any?,typeid: Any?,creditworthinessid: Any?,creditWorthness: Any?,contactpersonid: Any?,ContactPersonUserName: Any?,
                            paymenttermsid: Any?, paymentTerms: Any?,  billingstreet: Any?, billingcity: Any?, billingstate: Any?, billingzipcode: Any?, billingcountryid: Any?
                            ,shippingstreet: Any?, shippingcity: Any?, shippingstate: Any?, shippingzipcode: Any?,
                            shippingcountryid: Any?, type: Any?,convertedbyuser: Any?,billingCountry: Any,shippingCountry: Any?)
        {
            basicInfoList?.add(CustomerBasicDataAdapter(contactID, firstname,lastname, email,phone,mobile, address, creationdate,  city, state, countryid,countryName, details,companyid,
                    dateupdated, createdby, updatedby, contacttypeId,companyname, categoryid,contactCategory, zipcode, ishidden
                    , customerid, convertedby,dateconverted,typeid,creditworthinessid,creditWorthness,contactpersonid,ContactPersonUserName
                    ,paymenttermsid,paymentTerms,  billingstreet, billingcity, billingstate, billingzipcode, billingcountryid
                    ,shippingstreet, shippingcity, shippingstate, shippingzipcode,
                    shippingcountryid, type,convertedbyuser,billingCountry,shippingCountry))
        }


        fun insertSocialInfo(id: Any?,
                              facebook: Any?, twitter: Any?, googlePlus: Any?, instagram: Any?,
                             linkedin: Any?,pinterest: Any?,skype: Any?, whatsapp: Any?)
        {
            socialInfoList?.add(CustomerSocialProfilesAdapter(id,
           facebook, twitter,googlePlus, instagram,
             linkedin, pinterest, skype,whatsapp))
        }
        fun insertProducts(id: Any?, productId: Any?, customerId: Any?,
                            prdoctName: Any?, productDesc: Any?, unitPrice: Any?)
        {
            productsList?.add(CustomerProductsAdapter( id,productId,customerId,
            prdoctName, productDesc, unitPrice))
        }
        fun insertCampaigns(campaignId: Any?, sourceCode: Any?, campaignName: Any?,
                            campaignStatus: Any?, campaignType: Any?,  startDate: Any?,
                             revenue: Any?)
        {
            campaingsList?.add(CustomerCampaignsAdapter(campaignId,sourceCode,
            campaignName, campaignStatus, campaignType,startDate, revenue))
        }
        fun insertTransactions( description: Any?,amount: Any?,
                                invoiceNumber: Any?,
                               transactionTypeName: Any?, categoryName: Any?, date: Any?)
        {
transactionsList?.add(CustomerTransactionandInvoicesAdpater(description,amount,
             invoiceNumber,
            transactionTypeName,categoryName,date))
        }
        fun insertPos( poId: Any?, projectId: Any?, invoiceNumber: Any?,
                       dueDate: Any?, poNumber: Any?,discountAmount: Any?,
                       projectName: Any?,transactionStatus: Any?, notes: Any?)
        {
        posList?.add(CustomerPOsAdapter(poId, projectId, invoiceNumber,
            dueDate,poNumber,discountAmount,
             projectName,transactionStatus,notes))
        }
        fun insertNewsLetters(newsLetterId: Any?, newLetterName: Any?,description: Any?, status: Any?,
                               type: Any?, sendDate: Any?, sentBy: Any?)
        {
            newslettersList?.add(CustomerNewslettersAdapter(newsLetterId,newLetterName,description,status,
            type, sendDate,sentBy))
        }
        fun insertProjects(projectId: Any?, projectName: Any?, contactPerson: Any?,
                           status: Any?, dateStarted: Any?,endDate: Any?)
        {
            projectsList?.add(CustomerProjectsAdapter(projectId,projectName,contactPerson,
            status, dateStarted,endDate))
        }
        fun insertContracts(docTitle: Any?,  docId: Any?, dateSigned: Any?,latestVersion: Any?,
                            addedBy: Any?)
        {
            contractsList?.add(CustomerContractsAdapter(docTitle,  docId,dateSigned,latestVersion,
            addedBy))
        }
        fun insertTasks( title: Any?,status: Any?, startDate: Any?,endDate: Any?,username: Any?)
        {
            tasksList?.add(CustomerTasksAdapter(title,status,startDate, endDate,username))
        }
        fun insertEvents( eventId: Any?,eventName: Any?,startDate: Any?,endTime: Any?,
                         status: Any?,guest: Any?,contactPerson: Any?,email: Any?,phone: Any?,
                          mobile: Any?,conductBy: Any?)
        {
            eventsList?.add(CustomerEventsAdapter(eventId,eventName, startDate,endTime,
             status,guest,contactPerson,email,phone, mobile,conductBy))
        }

    }

}