package com.averox.bizggro.BroadcastReceivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.averox.bizggro.Controller.ApplicationController

/**
 * Created by Sarim on 11/29/2017.
 */
class ConnectivityReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val cm = context
                ?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.getActiveNetworkInfo()
        val isConnected = activeNetwork != null && activeNetwork!!.isConnectedOrConnecting()

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }


    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null

        val isConnected: Boolean
            get() {
                val cm = ApplicationController.instance?.getApplicationContext()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = cm.getActiveNetworkInfo()
                return activeNetwork != null && activeNetwork!!.isConnectedOrConnecting()
            }
    }

}